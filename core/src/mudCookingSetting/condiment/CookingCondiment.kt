package mudCookingSetting.condiment


import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonWriter
import com.inos.mudgdx.DimCon
import com.inos.mudgdx.FindUUID
import com.inos.mudgdx.NPFileHandle
import java.io.Serializable


class CookingCondiment :  Serializable {

    var UUID: String = "UUID"
    var category: String = "cookingCondiment"
    var name: String = "salt"
    var rarity: String = "Common"
    var type: String = "Primary"
    var price: Int = 1
    var gainAttributeMap : MutableMap<String, Int> = mutableMapOf()

    companion object {

        val cookingCondimentMap = mutableMapOf<String, CookingCondiment>()

        private val condimentGainAttribute = CondimentCategory.condimentGainAttribute()
        private val primaryList = CondimentCategory.defaultPrimaryCondiment()
        private val primaryGainMax = DimCon.primaryGainMax

        private val intermediateList = CondimentCategory.defaultIntermediateCondiment()
        private val intermediateGainMax = DimCon.intermediateGainMax

        private val advanceList = CondimentCategory.defaultAdvanceCondiment()
        private val advanceGainMax = DimCon.advanceGainMax



        fun cookingCondimentMap(){

            if (!NPFileHandle.condimentMapFH().exists()){
                generateCondimentMap()
            }
            extractCondimentMapFile()
        }


        private fun generateCondimentMap() {

            for (element in primaryList) {
                val thisCondiment = CookingCondiment()
                thisCondiment.name = element
                thisCondiment.UUID = FindUUID.findUUID(element)
                thisCondiment.category = "cookingCondiment"
                thisCondiment.rarity = "Common"
                thisCondiment.type = "Primary"
                thisCondiment.price = 1

                for (attributeName in condimentGainAttribute) {
                    val attributeUUID = FindUUID.findUUID(attributeName)
                    thisCondiment.gainAttributeMap[attributeUUID] = ((0..primaryGainMax).random()).toInt()
                }

                cookingCondimentMap[thisCondiment.UUID] = thisCondiment

            }


            for (element in intermediateList) {
                val thisCondiment = CookingCondiment()
                thisCondiment.name = element
                thisCondiment.UUID = FindUUID.findUUID(element)
                thisCondiment.category = "cookingCondiment"
                thisCondiment.rarity = "Common"
                thisCondiment.type = "Intermediate"
                thisCondiment.price = 3

                for (attributeName in condimentGainAttribute) {
                    val attributeUUID = FindUUID.findUUID(attributeName)
                    thisCondiment.gainAttributeMap[attributeUUID] = ((0..intermediateGainMax).random()).toInt()
                }
                cookingCondimentMap[thisCondiment.UUID] = thisCondiment

            }


            for (element in advanceList) {
                val thisCondiment = CookingCondiment()
                thisCondiment.name = element
                thisCondiment.UUID = FindUUID.findUUID(element)
                thisCondiment.category = "cookingCondiment"
                thisCondiment.rarity = "Common"
                thisCondiment.type = "Advance"
                thisCondiment.price = 10

                for (attributeName in condimentGainAttribute) {
                    val attributeUUID = FindUUID.findUUID(attributeName)
                    thisCondiment.gainAttributeMap[attributeUUID] = ((0..advanceGainMax).random()).toInt()
                }
                cookingCondimentMap[thisCondiment.UUID] = thisCondiment
            }

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.condimentMapFH()
            fileHandle.writeString(json.toJson(cookingCondimentMap), false)
        }




        private fun extractCondimentMapFile() {

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.condimentMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            for ( element in jsonValueMap) {
                val thisCondiment = CookingCondiment()
                thisCondiment.UUID = element.getString("UUID")
                thisCondiment.category = element.getString("category")
                thisCondiment.name = element.getString("name")
                thisCondiment.rarity = element.getString("rarity")
                thisCondiment.type = element.getString("type")
                thisCondiment.price = element.getInt("price")

                val attributeMap = element.get("gainAttributeMap")

                for (attribute in attributeMap) {

                    if (attribute.name != "class") {
                        val attributeUUID = attribute.name
                        val attributeValue = attributeMap.getInt(attribute.name)
                        thisCondiment.gainAttributeMap[attributeUUID] = attributeValue
                    }
                }

                cookingCondimentMap[thisCondiment.UUID] = thisCondiment

            }

        }


        private fun ClosedRange<Int>.random() = Math.random() * ((endInclusive + 1) - start) + start


    }


}


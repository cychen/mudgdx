package mudCookingSetting.condiment

class InnCookingCondiment {

    companion object {

        val innCookingCondimentMap = mutableMapOf<String, CookingCondiment>()
        private val cookingCondimentMap = CookingCondiment.cookingCondimentMap


        fun generateInnCookingCondimentMap(){

            innCookingCondimentMap.clear()

            val innCookingCondimentList = CondimentCategory.innCookingCondiment()

            for (innCookingCondiment in innCookingCondimentList) {

                for (condiment in cookingCondimentMap) {
                    if (condiment.value.name == innCookingCondiment) {
                        innCookingCondimentMap[condiment.key] = condiment.value
                    }
                }
            }
            if (innCookingCondimentMap.isEmpty()) {
                throw NullPointerException("innCookingCondimentMap is empty")
            }
        }



    }
}
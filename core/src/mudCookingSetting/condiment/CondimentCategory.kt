package mudCookingSetting.condiment

class CondimentCategory {

    companion object {

        val condimentRarity = mutableListOf("Common", "Special", "Unique")
        val condimentType = mutableListOf("Primary", "Intermediate", "Advance")

        fun condimentGainAttribute(): MutableList<String> {
            val gainAttributeNameList = mutableListOf("mainGFactor", "specialGFactor")
            return gainAttributeNameList
        }


        fun defaultPrimaryCondiment(): MutableList<String> {
            val condimentNameList = mutableListOf("NPSalt", "NPSugar", "NPCookingOil")
            return condimentNameList
        }


        fun defaultIntermediateCondiment(): MutableList<String> {
            val condimentNameList = mutableListOf("NPVinegar", "NPLiquor", "NPSoySource", "NPPepperPowder", "NPCinnamon", "NPCuminPowder", "NPPepperCorns")
            return condimentNameList
        }


        fun defaultAdvanceCondiment(): MutableList<String> {
            val condimentNameList = mutableListOf("NPPaste", "NPCurry", "NPMixedSpices")
            return condimentNameList
        }

        fun innCookingCondiment():MutableList<String> {
            val condimentNameList = mutableListOf("NPLiquor", "NPMixedSpices")
            return condimentNameList
        }






    }

}
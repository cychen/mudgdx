package mudCookingSetting.food

import mudCookingSetting.recipe.CookingRecipe
import mudObjectUUIDSetting.ObjectUUIDToName
import com.badlogic.gdx.utils.JsonReader

import com.inos.mudgdx.NPFileHandle
import com.inos.mudgdx.SaveMapFiles
import java.util.*

class RecipeFood {


    companion object {

        val recipeFoodMap = mutableMapOf<String, Food>()
        private val cookingRecipeMap = CookingRecipe.cookingRecipeMap
        private val objectUUIDToNameMap = ObjectUUIDToName.objectUUIDToNameMap

        fun recipeFoodMap() {

            if (!NPFileHandle.recipeFoodMapFH().exists()) {
                generateRecipeFoodMap()
            }
            extractRecipeFoodMap()
        }


        private fun generateRecipeFoodMap(){

            for (recipe in cookingRecipeMap) {
                val recipeFood = Food()
                recipeFood.UUID = UUID.randomUUID().toString()
                recipeFood.name = "Food${recipe.value.name}"

                for (mainEffect in recipe.value.mainEffectMap) {
                    recipeFood.mainEffectMap[mainEffect.key] = mainEffect.value
                }
                for (specialEffect in recipe.value.specialEffectMap) {
                    recipeFood.specialEffectMap[specialEffect.key] = specialEffect.value
                }

                recipeFood.qty = 1
                recipeFood.price = 1

                recipeFoodMap[recipeFood.UUID] = recipeFood

                objectUUIDToNameMap[recipeFood.UUID] = recipeFood.name
            }

            SaveMapFiles.saveRecipeFoodMap()
            SaveMapFiles.saveObjectUUIDToNameMap()

            //*** update objectUUIDToNameMap ***
            mudObjectUUIDSetting.ObjectUUIDToName.UUIDToNameMap()

        }


        private fun extractRecipeFoodMap() {

            val fileHandle = NPFileHandle.recipeFoodMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            for ( recipeFoodJVMap in jsonValueMap){

                val thisRecipeFood = Food()
                thisRecipeFood.UUID = recipeFoodJVMap.getString("UUID")
                thisRecipeFood.category = recipeFoodJVMap.getString("category")
                thisRecipeFood.name = recipeFoodJVMap.getString("name")
                thisRecipeFood.rarity = recipeFoodJVMap.getString("rarity")
                thisRecipeFood.type = recipeFoodJVMap.getString("type")
                thisRecipeFood.price = recipeFoodJVMap.getInt("price")
                thisRecipeFood.inventor = recipeFoodJVMap.getString("inventor")
                thisRecipeFood.description = recipeFoodJVMap.getString("description")

                val condimentJVMap = recipeFoodJVMap.get("condimentMap")
                for (condiment in condimentJVMap) {
                    if (condiment.name != "class") {
                        val condimentUUID = condiment.name
                        val condimentValue = condimentJVMap.getInt(condiment.name)
                        thisRecipeFood.condimentMap[condimentUUID] = condimentValue
                    }
                }

                val ingredientJVMap = recipeFoodJVMap.get("ingredientMap")
                for (ingredient in ingredientJVMap) {
                    if (ingredient.name != "class") {
                        val ingredientUUID = ingredient.name
                        val ingredientValue = ingredientJVMap.getInt(ingredient.name)
                        thisRecipeFood.ingredientMap[ingredientUUID] = ingredientValue
                    }
                }

                val mainEffectJVMap = recipeFoodJVMap.get("mainEffectMap")
                for (mainEffect in mainEffectJVMap) {
                    if (mainEffect.name != "class") {
                        val mainEffectUUID = mainEffect.name
                        val mainEffectValue = mainEffectJVMap.getInt(mainEffect.name)
                        thisRecipeFood.mainEffectMap[mainEffectUUID] = mainEffectValue
                    }
                }

                val specialEffectJVMap = recipeFoodJVMap.get("specialEffectMap")
                for (specialEffect in specialEffectJVMap){
                    if (specialEffect.name != "class"){
                        val specialEffectUUID = specialEffect.name
                        val specialEffectValue = specialEffectJVMap.getInt(specialEffect.name)
                        thisRecipeFood.specialEffectMap[specialEffectUUID] = specialEffectValue
                    }
                }
                recipeFoodMap[thisRecipeFood.UUID] = thisRecipeFood
            }

        }



        fun updateRecipeFoodMap(newRecipe: CookingRecipe) {

            val newRecipeFood = Food()

            do {
                newRecipeFood.UUID = UUID.randomUUID().toString()
            } while (objectUUIDToNameMap.contains(newRecipeFood.UUID))

            newRecipeFood.name = "Food${newRecipe.name}"

            for (mainEffect in newRecipe.mainEffectMap) {
                newRecipeFood.mainEffectMap[mainEffect.key] = mainEffect.value
            }
            for (specialEffect in newRecipe.specialEffectMap) {
                newRecipeFood.specialEffectMap[specialEffect.key] = specialEffect.value
            }

            newRecipeFood.qty = 1
            newRecipeFood.price = 1

            recipeFoodMap[newRecipeFood.UUID] = newRecipeFood
            objectUUIDToNameMap[newRecipeFood.UUID] = newRecipeFood.name
        }

        /*
            SaveMapFiles.saveRecipeFoodMap()

            //update recipeFoodMap
            extractRecipeFoodMap()

            SaveMapFiles.saveObjectUUIDToNameMap()

            //update objectUUIDToNameMap
            MudObjectUUID.ObjectUUIDToName.UUIDToNameMap()
        }
*/


    }

}
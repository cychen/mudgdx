package mudCookingSetting.food


class Food {

    var UUID: String = "UUID"
    var name:String = "FoodRecipeName"
    var category:String = "System"
    var rarity: String = "Unique"
    var type: String = "Simple"
    var description: String = ""
    var inventor: String = ""
    var qty:Int = 1
    var price: Int = 1
    var mainEffectMap:MutableMap<String, Int> = mutableMapOf()
    var specialEffectMap:MutableMap<String, Int> = mutableMapOf()
    var ingredientMap:MutableMap<String, Int> = mutableMapOf()
    var condimentMap:MutableMap<String, Int> = mutableMapOf()


    companion object {


        fun generateRecipeFoodMap() {
            //*** extract data from file
            RecipeFood.recipeFoodMap()
        }


        fun generateNoNameFoodMap() {
            //*** extract data from file
            NoNameFood.noNameFoodMap()
        }


        fun generateFailedFoodMap() {
            //*** extract data from file
            FailedFood.failedFoodMap()
        }

        fun recipeFoodMap(): MutableMap<String, Food>{
            //*** point to temporary storage address
            return RecipeFood.recipeFoodMap
        }

        fun noNameFoodMap(): MutableMap<String, Food>{
            //*** point to temporary storage address
            return NoNameFood.noNameFoodMap
        }

        fun failedFoodMap(): MutableMap<String, Food>{
            //*** point to temporary storage address
            return FailedFood.failedFoodMap
        }







    }


}
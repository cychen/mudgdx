package mudCookingSetting.food

import mudObjectUUIDSetting.NoNameObjectUUID
import com.badlogic.gdx.utils.JsonReader


import com.inos.mudgdx.NPFileHandle
import com.inos.mudgdx.SaveMapFiles

class NoNameFood {


    companion object {

        val noNameFoodMap = mutableMapOf<String, Food>()
        private val noNameObjectUUIDMap = NoNameObjectUUID.noNameObjectUUIDMap


        fun noNameFoodMap() {
            if (!NPFileHandle.noNameFoodMapFH().exists()) {
                generateNoNameFoodMap()
            }
            extractNoNameFoodMap()
        }


        private fun generateNoNameFoodMap(){
            noNameFoodMap.clear()
            SaveMapFiles.saveNoNameFoodMap()
        }


        private fun extractNoNameFoodMap() {

            val fileHandle = NPFileHandle.noNameFoodMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            for ( noNameFoodJVMap in jsonValueMap){

                val thisNoNameFood = Food()
                thisNoNameFood.UUID = noNameFoodJVMap.getString("UUID")
                thisNoNameFood.category = noNameFoodJVMap.getString("category")
                thisNoNameFood.name = noNameFoodJVMap.getString("name")
                thisNoNameFood.rarity = noNameFoodJVMap.getString("rarity")
                thisNoNameFood.type = noNameFoodJVMap.getString("type")
                thisNoNameFood.price = noNameFoodJVMap.getInt("price")
                thisNoNameFood.inventor = noNameFoodJVMap.getString("inventor")
                thisNoNameFood.description = noNameFoodJVMap.getString("description")

                val condimentJVMap = noNameFoodJVMap.get("condimentMap")
                for (condiment in condimentJVMap) {
                    if (condiment.name != "class") {
                        val condimentUUID = condiment.name
                        val condimentValue = condimentJVMap.getInt(condiment.name)
                        thisNoNameFood.condimentMap[condimentUUID] = condimentValue
                    }
                }


                val ingredientJVMap = noNameFoodJVMap.get("ingredientMap")
                for (ingredient in ingredientJVMap) {
                    if (ingredient.name != "class") {
                        val ingredientUUID = ingredient.name
                        val ingredientValue = ingredientJVMap.getInt(ingredient.name)
                        thisNoNameFood.ingredientMap[ingredientUUID] = ingredientValue

                    }
                }

                val mainEffectJVMap = noNameFoodJVMap.get("mainEffectMap")
                for (mainEffect in mainEffectJVMap) {
                    if (mainEffect.name != "class") {
                        val mainEffectUUID = mainEffect.name
                        val mainEffectValue = mainEffectJVMap.getInt(mainEffect.name)
                        thisNoNameFood.mainEffectMap[mainEffectUUID] = mainEffectValue
                    }
                }

                val specialEffectJVMap = noNameFoodJVMap.get("specialEffectMap")
                for (specialEffect in specialEffectJVMap){
                    if (specialEffect.name != "class"){
                        val specialEffectUUID = specialEffect.name
                        val specialEffectValue = specialEffectJVMap.getInt(specialEffect.name)
                        thisNoNameFood.specialEffectMap[specialEffectUUID] = specialEffectValue
                    }
                }

                noNameFoodMap[thisNoNameFood.UUID] = thisNoNameFood
            }
        }


        fun updateNoNameFoodMap(noNameFood: Food) {
            checkForExistence(noNameFood)
        }

        private fun checkForExistence(noNameFood: Food) {

            if (noNameFoodMap.containsKey(noNameFood.UUID)) throw NullPointerException(
                    "noNameFood already exists in noNameFoodMap")
            else {
                doUpdateNoNameFoodMap(noNameFood)
            }
        }


        private fun doUpdateNoNameFoodMap(noNameFood: Food) {

            noNameFoodMap[noNameFood.UUID] = noNameFood
            noNameObjectUUIDMap[noNameFood.UUID] = noNameFood.name
        }
        /*
            SaveMapFiles.saveNoNameFoodMap()

            //update noNameFoodMap
            extractNoNameFoodMap()

            SaveMapFiles.saveNoNameObjectUUIDMap()

            //update noNameObjectUUIDMap
            NoNameObjectUUID.noNameObjectUUIDMap()
        }

*/

    }

}
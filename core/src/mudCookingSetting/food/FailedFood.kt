package mudCookingSetting.food

import mudObjectUUIDSetting.FailedObjectUUID
import com.badlogic.gdx.utils.JsonReader
import com.inos.mudgdx.NPFileHandle
import com.inos.mudgdx.SaveMapFiles

class FailedFood {

    companion object {

        val failedFoodMap = mutableMapOf<String, Food>()
        private val failedObjectUUIDMap = FailedObjectUUID.failedObjectUUIDMap


        fun failedFoodMap() {
            if (!NPFileHandle.failedFoodMapFH().exists()) {
                generateFailedFoodMap()
            }
            extractFailedFoodMap()
        }


        private fun generateFailedFoodMap(){
            failedFoodMap.clear()
            SaveMapFiles.saveFailedFoodMap()
        }


        private fun extractFailedFoodMap() {

            val fileHandle = NPFileHandle.failedFoodMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            for ( failedFoodJVMap in jsonValueMap){

                val thisFailedFood = Food()
                thisFailedFood.UUID = failedFoodJVMap.getString("UUID")
                thisFailedFood.category = failedFoodJVMap.getString("category")
                thisFailedFood.name = failedFoodJVMap.getString("name")
                thisFailedFood.rarity = failedFoodJVMap.getString("rarity")
                thisFailedFood.type = failedFoodJVMap.getString("type")
                thisFailedFood.price = failedFoodJVMap.getInt("price")
                thisFailedFood.inventor = failedFoodJVMap.getString("inventor")
                thisFailedFood.description = failedFoodJVMap.getString("description")

                val condimentJVMap = failedFoodJVMap.get("condimentMap")

                for (condiment in condimentJVMap) {
                    if (condiment.name != "class") {
                        val condimentUUID = condiment.name
                        val condimentValue = condimentJVMap.getInt(condiment.name)
                        thisFailedFood.condimentMap[condimentUUID] = condimentValue
                    }
                }


                val ingredientJVMap = failedFoodJVMap.get("ingredientMap")
                for (ingredient in ingredientJVMap) {
                    if (ingredient.name != "class") {
                        val ingredientUUID = ingredient.name
                        val ingredientValue = ingredientJVMap.getInt(ingredient.name)
                        thisFailedFood.ingredientMap[ingredientUUID] = ingredientValue

                    }
                }

                val mainEffectJVMap = failedFoodJVMap.get("mainEffectMap")
                for (mainEffect in mainEffectJVMap) {
                    if (mainEffect.name != "class") {
                        val mainEffectUUID = mainEffect.name
                        val mainEffectValue = mainEffectJVMap.getInt(mainEffect.name)
                        thisFailedFood.mainEffectMap[mainEffectUUID] = mainEffectValue
                    }
                }

                val specialEffectJVMap = failedFoodJVMap.get("specialEffectMap")
                for (specialEffect in specialEffectJVMap){
                    if (specialEffect.name != "class"){
                        val specialEffectUUID = specialEffect.name
                        val specialEffectValue = specialEffectJVMap.getInt(specialEffect.name)
                        thisFailedFood.specialEffectMap[specialEffectUUID] = specialEffectValue
                    }
                }

                failedFoodMap[thisFailedFood.UUID] = thisFailedFood
            }
        }


        fun updateFailedFoodMap(newFailedFood: Food) {
            checkForExistence(newFailedFood)
        }

        private fun checkForExistence(newFailedFood: Food) {

            if (failedFoodMap.containsKey(newFailedFood.UUID)) throw NullPointerException(
                    "failedFood already exists in failedFoodMap")
            else {
                doUpdateFailedFoodMap(newFailedFood)
            }
        }


        private fun doUpdateFailedFoodMap(newFailedFood: Food) {

            failedFoodMap[newFailedFood.UUID] = newFailedFood
            failedObjectUUIDMap[newFailedFood.UUID] = newFailedFood.name
        }
        /*
            SaveMapFiles.saveFailedFoodMap()

            //update failedFoodMap
            extractFailedFoodMap()

            SaveMapFiles.saveFailedObjectUUIDMap()

            //update failedObjectUUIDMap
            FailedObjectUUID.failedObjectUUIDMap()
        }

*/




    }
}
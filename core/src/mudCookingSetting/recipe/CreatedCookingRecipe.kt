package mudCookingSetting.recipe

import com.badlogic.gdx.utils.JsonReader
import com.inos.mudgdx.NPFileHandle
import com.inos.mudgdx.SaveMapFiles

//category: System And Create
class CreatedCookingRecipe  {

    companion object {

        val createdCookingRecipeMap = mutableMapOf<String, CookingRecipe>()

        private val objectUUIDToNameMap = mudObjectUUIDSetting.ObjectUUIDToName.objectUUIDToNameMap

        fun createdCookingRecipeMap(){

            if (!NPFileHandle.createdCookingRecipeMapFH().exists()){
                generateCreatedCookingRecipeMap()
            }
            extractCreatedCookingRecipeMap()
        }


        private fun generateCreatedCookingRecipeMap() {

            val recipeMap = mutableMapOf<String, CookingRecipe>()
            val json = SaveMapFiles.prepareJson()

            val fileHandle = NPFileHandle.createdCookingRecipeMapFH()
            fileHandle.writeString(json.toJson(recipeMap), false)

        }


        private fun extractCreatedCookingRecipeMap(){
            /*
            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)
*/
            val fileHandle = NPFileHandle.createdCookingRecipeMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            for ( recipeJVMap in jsonValueMap){

                val thisRecipe = CookingRecipe()
                thisRecipe.UUID = recipeJVMap.getString("UUID")
                thisRecipe.category = recipeJVMap.getString("category")
                thisRecipe.name = recipeJVMap.getString("name")
                thisRecipe.rarity = recipeJVMap.getString("rarity")
                thisRecipe.type = recipeJVMap.getString("type")
                thisRecipe.price = recipeJVMap.getInt("price")
                thisRecipe.inventor = recipeJVMap.getString("inventor")
                thisRecipe.description = recipeJVMap.getString("description")

                val condimentJVMap = recipeJVMap.get("condimentMap")
                for (condiment in condimentJVMap) {
                    if (condiment.name != "class") {

                        val condimentUUID = condiment.name
                        val condimentValue = condimentJVMap.getInt(condiment.name)

                        thisRecipe.condimentMap[condimentUUID] = condimentValue
                    }
                }


                val ingredientJVMap = recipeJVMap.get("ingredientMap")
                for (ingredient in ingredientJVMap) {
                    if (ingredient.name != "class") {
                        val ingredientUUID = ingredient.name
                        val ingredientValue = ingredientJVMap.getInt(ingredient.name)
                        thisRecipe.ingredientMap[ingredientUUID] = ingredientValue
                    }
                }

                val mainEffectJVMap = recipeJVMap.get("mainEffectMap")
                for (mainEffect in mainEffectJVMap) {
                    if (mainEffect.name != "class") {
                        val mainEffectUUID = mainEffect.name
                        val mainEffectValue = mainEffectJVMap.getInt(mainEffect.name)
                        thisRecipe.mainEffectMap[mainEffectUUID] = mainEffectValue
                    }
                }

                val specialEffectJVMap = recipeJVMap.get("specialEffectMap")
                for (specialEffect in specialEffectJVMap){
                    if (specialEffect.name != "class"){
                        val specialEffectUUID = specialEffect.name
                        val specialEffectValue = specialEffectJVMap.getInt(specialEffect.name)
                        thisRecipe.specialEffectMap[specialEffectUUID] = specialEffectValue
                    }
                }

                createdCookingRecipeMap[thisRecipe.UUID] = thisRecipe
            }
        }




        fun updateCreatedCookingRecipeMap(newCookingRecipe: CookingRecipe) {

            createdCookingRecipeMap[newCookingRecipe.UUID] = newCookingRecipe

            objectUUIDToNameMap[newCookingRecipe.UUID] = newCookingRecipe.name

            //*** refresh cookingRecipeMap ***
            CookingRecipe.refreshCookingRecipeMap()

            //*** also updateRecipeFoodMap data in temporary storage
            mudCookingSetting.food.RecipeFood.updateRecipeFoodMap(newCookingRecipe)
        }

        /*
            SaveMapFiles.saveCreatedCookingRecipeMap()

            """update createdCookingRecipeMap"""
           // extractCreatedCookingRecipeMap(), this is included in CookingRecipe.generateCookingRecipeMap()

            """update recipeFoodMap"""
            RecipeFood.updateRecipeFoodMap(newCookingRecipe)

            SaveMapFiles.saveObjectUUIDToNameMap()

            """update objectUUIDToNameMap"""
            MudObjectUUID.ObjectUUIDToName.UUIDToNameMap()

        }
*/





    }


}
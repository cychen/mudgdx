package mudCookingSetting.recipe


//category: System And Create

class CookingRecipe {

    var UUID: String = "UUID"
    var category: String = "System"
    var name: String = "NPPastry"
    var rarity: String = "Common"
    var type: String = "Simple"
    var price: Int = 1
    var inventor: String = "NPResident"
    var description: String = "...."
    var ingredientMap: MutableMap<String, Int> = mutableMapOf()
    var condimentMap: MutableMap<String, Int> = mutableMapOf()
    var mainEffectMap: MutableMap<String, Int> = mutableMapOf()
    var specialEffectMap: MutableMap<String, Int> = mutableMapOf()

    companion object {

        val cookingRecipeMap = mutableMapOf<String, CookingRecipe>()
        private val defaultCookingRecipeMap = DefaultCookingRecipe.defaultCookingRecipeMap
        private val createdCookingRecipeMap = CreatedCookingRecipe.createdCookingRecipeMap

        fun generateCookingRecipeMap() {
            //*** will extract data from files ***
            DefaultCookingRecipe.defaultCookingRecipeMap()
            CreatedCookingRecipe.createdCookingRecipeMap()

            for (recipe in defaultCookingRecipeMap){
                cookingRecipeMap[recipe.key]= recipe.value
            }

            for (recipe in createdCookingRecipeMap){
                cookingRecipeMap[recipe.key]= recipe.value
            }
        }

        fun refreshCookingRecipeMap(){
            //*** just refresh map from temporary data storage
            for (recipe in defaultCookingRecipeMap){
                cookingRecipeMap[recipe.key]= recipe.value
            }

            for (recipe in createdCookingRecipeMap){
                cookingRecipeMap[recipe.key]= recipe.value
            }
        }



    }


}

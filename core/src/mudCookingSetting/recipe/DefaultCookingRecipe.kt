package mudCookingSetting.recipe


import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonWriter
import com.inos.mudgdx.FindUUID
import com.inos.mudgdx.NPFileHandle

//category: System And Create
class DefaultCookingRecipe  {

    companion object {

        val defaultCookingRecipeMap = mutableMapOf<String, CookingRecipe>()

        private fun recipeNPPastry(): CookingRecipe {

            val NPPastryRecipe = CookingRecipe()
            NPPastryRecipe.name = "NPPastry"
            NPPastryRecipe.UUID = FindUUID.findUUID("NPPastry")
            NPPastryRecipe.category = "System"
            NPPastryRecipe.rarity = "Common"
            NPPastryRecipe.type = "Simple"
            NPPastryRecipe.price = 1
            NPPastryRecipe.inventor = "NPResident"
            NPPastryRecipe.description = "NPPastry is popular food in NP World. This recipe is" +
                    " so common to find and easy to learn in NP World."

            NPPastryRecipe.ingredientMap[FindUUID.findUUID("NPFlour")] = 1
            NPPastryRecipe.condimentMap[FindUUID.findUUID("NPSalt")] = 1
            NPPastryRecipe.condimentMap[FindUUID.findUUID("NPCookingOil")] = 1

            NPPastryRecipe.mainEffectMap[FindUUID.findUUID("Str")] = 2
            NPPastryRecipe.mainEffectMap[FindUUID.findUUID("Agi")] = 4
            NPPastryRecipe.mainEffectMap[FindUUID.findUUID("Foc")] = 6

           // NPPastryRecipe.specialEffectMap[FindUUID.findUUID("effectLasting")] = 10
            NPPastryRecipe.specialEffectMap[FindUUID.findUUID("effectLasting")] = 2
            NPPastryRecipe.specialEffectMap[FindUUID.findUUID("lifeRecovery")] = 3

            return NPPastryRecipe

        }


        private fun recipeNPBaozi(): CookingRecipe {

            val NPBaoziRecipe = CookingRecipe()
            NPBaoziRecipe.name = "NPBaozi"
            NPBaoziRecipe.UUID = FindUUID.findUUID("NPBaozi")
            NPBaoziRecipe.category = "System"
            NPBaoziRecipe.rarity = "Special"
            NPBaoziRecipe.type = "Advance"
            NPBaoziRecipe.price = 5
            NPBaoziRecipe.inventor = "NPResident"
            NPBaoziRecipe.description = "A sign posted in front of the inner entrance door saying :" +
                    "Do not leave your NPBaozi unattended! It is not our responsibilities to keep your" +
                    "NPBaozi safe from anyone or any creature that would love to help you enjoy them."

            NPBaoziRecipe.ingredientMap[FindUUID.findUUID("NPFlour")] = 1
            NPBaoziRecipe.ingredientMap[FindUUID.findUUID("NPPork")] = 1
            NPBaoziRecipe.ingredientMap[FindUUID.findUUID("NPGreenOnion")] = 1

            NPBaoziRecipe.condimentMap[FindUUID.findUUID("NPPepperPowder")] = 1
            NPBaoziRecipe.condimentMap[FindUUID.findUUID("NPSoySource")] = 1
            NPBaoziRecipe.condimentMap[FindUUID.findUUID("NPVinegar")] = 1
            NPBaoziRecipe.condimentMap[FindUUID.findUUID("NPLiquor")] = 1

            NPBaoziRecipe.mainEffectMap[FindUUID.findUUID("Str")] = 10
            NPBaoziRecipe.mainEffectMap[FindUUID.findUUID("Agi")] = 10
            NPBaoziRecipe.mainEffectMap[FindUUID.findUUID("Foc")] = 10

            NPBaoziRecipe.specialEffectMap[FindUUID.findUUID("effectLasting")] = 10
            NPBaoziRecipe.specialEffectMap[FindUUID.findUUID("lifeRecovery")] = 10

            return NPBaoziRecipe

        }


        private fun recipeNPMeatStew(): CookingRecipe {

            val NPMeatStewRecipe = CookingRecipe()
            NPMeatStewRecipe.name = "NPMeatStew"
            NPMeatStewRecipe.UUID = FindUUID.findUUID("NPMeatStew")
            NPMeatStewRecipe.category = "System"
            NPMeatStewRecipe.rarity = "Unique"
            NPMeatStewRecipe.type = "House"
            NPMeatStewRecipe.price = 25
            NPMeatStewRecipe.inventor = "NPInnChef"
            NPMeatStewRecipe.description = "The taste of NPMeatStew served in the NPInn is so special, so good! " +
                    "It is created by the NPInn famous chef --Oldy using special ingredients and spices found in the" +
                    "NPWorld! "

            NPMeatStewRecipe.ingredientMap[FindUUID.findUUID("NPPork")] = 1
            NPMeatStewRecipe.ingredientMap[FindUUID.findUUID("NPChestNut")] = 1
            NPMeatStewRecipe.ingredientMap[FindUUID.findUUID("NPWoodEar")] = 1
            NPMeatStewRecipe.ingredientMap[FindUUID.findUUID("NPMushroom")] = 1
            NPMeatStewRecipe.ingredientMap[FindUUID.findUUID("NPGreens")] = 1

            NPMeatStewRecipe.condimentMap[FindUUID.findUUID("NPLiquor")] = 1
            NPMeatStewRecipe.condimentMap[FindUUID.findUUID("NPVinegar")] = 1
            NPMeatStewRecipe.condimentMap[FindUUID.findUUID("NPSoySource")] = 1
            NPMeatStewRecipe.condimentMap[FindUUID.findUUID("NPMixedSpices")] = 1

            NPMeatStewRecipe.mainEffectMap[FindUUID.findUUID("Str")] = 20
            NPMeatStewRecipe.mainEffectMap[FindUUID.findUUID("Agi")] = 20
            NPMeatStewRecipe.mainEffectMap[FindUUID.findUUID("Foc")] = 20

            NPMeatStewRecipe.specialEffectMap[FindUUID.findUUID("effectLasting")] = 20
            NPMeatStewRecipe.specialEffectMap[FindUUID.findUUID("lifeRecovery")] = 30

            return NPMeatStewRecipe

        }


        fun defaultCookingRecipeMap(){

            if (!NPFileHandle.defaultCookingRecipeMapFH().exists()){
                generateDefaultCookingRecipeMap()
            }
            extractDefaultCookingRecipeMap()
        }


        private fun generateDefaultCookingRecipeMap() {

            val defaultRecipeList = mutableListOf(recipeNPPastry(), recipeNPBaozi(), recipeNPMeatStew())

            for (recipe in defaultRecipeList) {
                defaultCookingRecipeMap[recipe.UUID] = recipe
            }

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.defaultCookingRecipeMapFH()
            fileHandle.writeString(json.toJson(defaultCookingRecipeMap), false)
        }


        private fun extractDefaultCookingRecipeMap(){
            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.defaultCookingRecipeMapFH()
            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            for ( recipeJVMap in jsonValueMap){

                val thisRecipe = CookingRecipe()
                thisRecipe.UUID = recipeJVMap.getString("UUID")
                thisRecipe.category = recipeJVMap.getString("category")
                thisRecipe.name = recipeJVMap.getString("name")
                thisRecipe.rarity = recipeJVMap.getString("rarity")
                thisRecipe.type = recipeJVMap.getString("type")
                thisRecipe.price = recipeJVMap.getInt("price")
                thisRecipe.inventor = recipeJVMap.getString("inventor")
                thisRecipe.description = recipeJVMap.getString("description")

                val condimentJVMap = recipeJVMap.get("condimentMap")
                for (condiment in condimentJVMap) {
                    if (condiment.name != "class") {
                        val condimentUUID = condiment.name
                        val condimentValue = condimentJVMap.getInt(condiment.name)
                        thisRecipe.condimentMap[condimentUUID] = condimentValue
                    }
                }

                val ingredientJVMap = recipeJVMap.get("ingredientMap")
                for (ingredient in ingredientJVMap) {
                    if (ingredient.name != "class") {
                        val ingredientUUID = ingredient.name
                        val ingredientValue = ingredientJVMap.getInt(ingredient.name)
                        thisRecipe.ingredientMap[ingredientUUID] = ingredientValue

                    }
                }

                val mainEffectJVMap = recipeJVMap.get("mainEffectMap")
                for (mainEffect in mainEffectJVMap) {
                    if (mainEffect.name != "class") {
                        val mainEffectUUID = mainEffect.name
                        val mainEffectValue = mainEffectJVMap.getInt(mainEffect.name)
                        thisRecipe.mainEffectMap[mainEffectUUID] = mainEffectValue
                    }
                }

                val specialEffectJVMap = recipeJVMap.get("specialEffectMap")
                for (specialEffect in specialEffectJVMap){
                    if (specialEffect.name != "class"){
                        val specialEffectUUID = specialEffect.name
                        val specialEffectValue = specialEffectJVMap.getInt(specialEffect.name)
                        thisRecipe.specialEffectMap[specialEffectUUID] = specialEffectValue
                    }
                }

                defaultCookingRecipeMap[thisRecipe.UUID] = thisRecipe
            }

        }




    }


}
package mudCookingSetting.recipe

class RecipeCategory {

    companion object {

        private val rarity = mutableListOf("Common", "Special", "Unique")
        private val type = mutableListOf("Simple", "Advance", "House")

        fun simpleRecipe(): MutableList<String> {

            return mutableListOf("NPPastry")

        }

        fun advanceRecipe(): MutableList<String> {

            return mutableListOf("NPBaozi", "NPRiceRoll")
        }

        fun houseRecipe(): MutableList<String> {

            return mutableListOf("NPMeatStew")
        }


    }
}


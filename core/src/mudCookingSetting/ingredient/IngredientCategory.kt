package mudCookingSetting.ingredient

class IngredientCategory {

    companion object {

        private val rarity = mutableListOf("Common", "Special", "Unique")
        private val ingredientType = mutableListOf("Primary", "Intermediate", "Advance")


        fun ingredientMainAttribute(): MutableList<String> {

            return mutableListOf("Str", "Agi", "Foc")
        }

        fun ingredientSpecialAttribute(): MutableList<String> {

            return mutableListOf("effectLasting", "lifeRecovery")
        }


        fun defaultPrimaryIngredient(): MutableList<String> {
            val nameList = mutableListOf("NPFlour", "NPRice", "NPYam", "NPPotato", "NPCorn", "NPGreenOnion", "NPGarlic", "NPGinger", "NPHotPepper" )

            return nameList
        }


        fun defaultIntermediateIngredient(): MutableList<String> {
            val nameList = mutableListOf( "NPEgg","NPCabbage", "NPCarrot", "NPTomato", "NPGreens", "NPWoodEar", "NPMushroom")

            return nameList
        }


        fun defaultAdvanceIngredient(): MutableList<String> {
            val nameList = mutableListOf( "NPChestNut", "NPChickenMeat", "NPPork", "NPBeef", "NPFish", "NPBacon")

            return nameList
        }



    }

}
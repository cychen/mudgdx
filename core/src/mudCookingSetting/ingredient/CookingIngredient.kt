package mudCookingSetting.ingredient


import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonWriter
import com.inos.mudgdx.DimCon
import com.inos.mudgdx.FindUUID
import com.inos.mudgdx.NPFileHandle
import java.io.Serializable

class CookingIngredient: Serializable {

    var UUID: String = "UUID"
    var category: String = "cookingIngredient"
    var name: String = "flour"
    var rarity: String = "Common"
    var type: String = "Primary"
    var price: Int = 1
    var mainAttributeMap: MutableMap<String, Int> = mutableMapOf()
    var specialAttributeMap: MutableMap<String, Int> = mutableMapOf()

    companion object {

        val cookingIngredientMap = mutableMapOf<String, CookingIngredient>()

        private val mainAttributeList = IngredientCategory.ingredientMainAttribute()
        private val specialAttributeList = IngredientCategory.ingredientSpecialAttribute()

        private val primaryList = IngredientCategory.defaultPrimaryIngredient()
        private val primaryMainMax = DimCon.primaryMainMax
        private val primarySpecialMax = DimCon.primarySpecialMax

        private val intermediateList = IngredientCategory.defaultIntermediateIngredient()
        private val intermediateMainMax = DimCon.intermediateMainMax
        private val intermediateSpecialMax = DimCon.intermediateSpecialMax

        private val advanceList = IngredientCategory.defaultAdvanceIngredient()
        private val advanceMainMax = DimCon.advanceMainMax
        private val advanceSpecialMax = DimCon.advanceSpecialMax


        fun cookingIngredientMap() {

            if (!NPFileHandle.cookingIngredientMapFH().exists()) {
                generateCookingIngredientMap()
            }
            extractCookingIngredientMap()
        }


        private fun generateCookingIngredientMap() {

            for (element in primaryList) {
                val thisIngredient = CookingIngredient()
                thisIngredient.name = element
                thisIngredient.UUID = FindUUID.findUUID(element)
                thisIngredient.category = "cookingIngredient"
                thisIngredient.rarity = "Common"
                thisIngredient.type = "Primary"
                thisIngredient.price = 1

                for ( mainAttribute in mainAttributeList) {
                    val mainAttributeUUID = FindUUID.findUUID(mainAttribute)
                    thisIngredient.mainAttributeMap[mainAttributeUUID] = ((0..primaryMainMax).random()).toInt()
                }

                for (specialAttribute in specialAttributeList){
                    val specialAttributeUUID = FindUUID.findUUID(specialAttribute)
                    thisIngredient.specialAttributeMap[specialAttributeUUID] = ((0..primarySpecialMax).random()).toInt()
                }

                cookingIngredientMap[thisIngredient.UUID] = thisIngredient
            }

            for (element in intermediateList) {
                val thisIngredient = CookingIngredient()
                thisIngredient.name = element
                thisIngredient.UUID = FindUUID.findUUID(element)
                thisIngredient.category = "cookingIngredient"
                thisIngredient.rarity = "Common"
                thisIngredient.type = "Intermediate"
                thisIngredient.price = 3

                for ( mainAttribute in mainAttributeList) {
                    val mainAttributeUUID = FindUUID.findUUID(mainAttribute)
                    thisIngredient.mainAttributeMap[mainAttributeUUID] = ((0..intermediateMainMax).random()).toInt()
                }

                for (specialAttribute in specialAttributeList){
                    val specialAttributeUUID = FindUUID.findUUID(specialAttribute)
                    thisIngredient.specialAttributeMap[specialAttributeUUID] = ((0..intermediateSpecialMax).random()).toInt()
                }

                cookingIngredientMap[thisIngredient.UUID] = thisIngredient
            }


            for (element in advanceList) {
                val thisIngredient = CookingIngredient()
                thisIngredient.name = element
                thisIngredient.UUID = FindUUID.findUUID(element)
                thisIngredient.category = "cookingIngredient"
                thisIngredient.rarity = "Common"
                thisIngredient.type = "Advance"
                thisIngredient.price = 10

                for ( mainAttribute in mainAttributeList) {
                    val mainAttributeUUID = FindUUID.findUUID(mainAttribute)
                    thisIngredient.mainAttributeMap[mainAttributeUUID] = ((0..advanceMainMax).random()).toInt()
                }

                for (specialAttribute in specialAttributeList){
                    val specialAttributeUUID = FindUUID.findUUID(specialAttribute)
                    thisIngredient.specialAttributeMap[specialAttributeUUID] = ((0..advanceSpecialMax).random()).toInt()
                }

                cookingIngredientMap[thisIngredient.UUID] = thisIngredient
            }

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.cookingIngredientMapFH()
            fileHandle.writeString(json.toJson(cookingIngredientMap), false)

        }



        private fun extractCookingIngredientMap() {

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.cookingIngredientMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            for ( element in jsonValueMap) {

                val thisIngredient = CookingIngredient()
                thisIngredient.UUID = element.getString("UUID")
                thisIngredient.category = element.getString("category")
                thisIngredient.name = element.getString("name")
                thisIngredient.rarity = element.getString("rarity")
                thisIngredient.type = element.getString("type")
                thisIngredient.price = element.getInt("price")
                val mainAttributeMapJV = element.get("mainAttributeMap")
                val specialAttributeMapJV = element.get("specialAttributeMap")

                for (attribute in mainAttributeMapJV) {

                    if (attribute.name != "class") {
                        val attributeUUID = attribute.name
                        val attributeValue = mainAttributeMapJV.getInt(attribute.name)
                        thisIngredient.mainAttributeMap[attributeUUID] = attributeValue
                    }
                }

                for (attribute in specialAttributeMapJV) {

                    if (attribute.name != "class") {
                        val attributeUUID = attribute.name
                        val attributeValue = specialAttributeMapJV.getInt(attribute.name)
                        thisIngredient.specialAttributeMap[attributeUUID] = attributeValue
                    }
                }

                cookingIngredientMap[thisIngredient.UUID] = thisIngredient
            }
        }


        private fun ClosedRange<Int>.random() = Math.random() * ((endInclusive + 1) - start) + start


    }



}
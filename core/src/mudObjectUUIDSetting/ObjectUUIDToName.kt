package mudObjectUUIDSetting

import mudCookingSetting.condiment.CondimentCategory
import mudCookingSetting.ingredient.IngredientCategory
import mudCookingSetting.recipe.RecipeCategory
import mudMedicineSetting.MedicinalCategory
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonWriter
import com.inos.mudgdx.NPFileHandle
import java.util.*

class ObjectUUIDToName {


    companion object {

        val objectUUIDToNameMap = mutableMapOf<String, String>()

        fun UUIDToNameMap() {
            if (!NPFileHandle.objectUUIDToNameMapFH().exists()) {
                generateMap()
            }
            extractMap()
        }


        private fun generateMap() {

            //val listMap = mutableMapOf<String, String>()
            //  listMap["tree"] = mutableMapOf("1")
            //  listMap["flower"] = mutableMapOf("a")
            generatePlayerAttributeUUIDToNameMap()
            generateGainAttributeUUIDToNameMap()
            generateCookingIngredientUUIDToNameMap()
            generateCookingCondimentUUIDToNameMap()
            generateCookingRecipeUUIDToNameMap()
            generateMedicinalUUIDToNameMap()

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.objectUUIDToNameMapFH()
            fileHandle.writeString(json.toJson(objectUUIDToNameMap), false)
        }



        private fun extractMap() {

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.objectUUIDToNameMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            objectUUIDToNameMap.clear()

            for ( element in jsonValueMap) {

                // val objectName = element.name

                for (component in element){

                    if (component.name != "class") {
                        val nameString = element.getString(component.name)
                        objectUUIDToNameMap[element.name] = nameString
                    }
                }
            }
        }

        private fun generatePlayerAttributeUUIDToNameMap(){

            val baseLifeAttributeNameList = playerMaps.PlayerAttributeMap.baseLifeAttributeNameList()
            val baseMainAttributeNameList = playerMaps.PlayerAttributeMap.baseMainAttributeNameList()
            val baseSpecialAttributeNameList = playerMaps.PlayerAttributeMap.baseSpecialAttributeNameList()
            val nameList = mutableListOf(baseLifeAttributeNameList, baseMainAttributeNameList, baseSpecialAttributeNameList)

            for (eachNameList in nameList){
                for (elementName in eachNameList) {
                    val elementUUID = UUID.randomUUID().toString()
                    objectUUIDToNameMap[elementUUID] = elementName
                }
            }
        }


        private fun generateGainAttributeUUIDToNameMap(){

            val gainAttributeNameList = mutableListOf("mainGFactor", "specialGFactor")
            val nameList = mutableListOf(gainAttributeNameList)

            for (eachNameList in nameList){
                for (elementName in eachNameList) {
                    val elementUUID = UUID.randomUUID().toString()
                    objectUUIDToNameMap[elementUUID] = elementName
                }
            }
        }


        private fun generateCookingIngredientUUIDToNameMap(){
            val primaryNameList = IngredientCategory.defaultPrimaryIngredient()
            val intermediateNameList = IngredientCategory.defaultIntermediateIngredient()
            val advanceNameList = IngredientCategory.defaultAdvanceIngredient()
            val nameList = mutableListOf(primaryNameList, intermediateNameList, advanceNameList)

            for (eachNameList in nameList){
                for (elementName in eachNameList){
                    val elementUUID = UUID.randomUUID().toString()
                    objectUUIDToNameMap[elementUUID] = elementName
                }
            }
        }


        private fun generateCookingCondimentUUIDToNameMap(){
            val primaryNameList = CondimentCategory.defaultPrimaryCondiment()
            val intermediateNameList = CondimentCategory.defaultIntermediateCondiment()
            val advanceNameList = CondimentCategory.defaultAdvanceCondiment()
            val nameList = mutableListOf(primaryNameList, intermediateNameList, advanceNameList)

            for (eachNameList in nameList){
                for (elementName in eachNameList){
                    val elementUUID = UUID.randomUUID().toString()
                    objectUUIDToNameMap[elementUUID] = elementName
                }
            }
        }


        private fun generateCookingRecipeUUIDToNameMap(){
            val simpleNameList = RecipeCategory.simpleRecipe()
            val advanceNameList = RecipeCategory.advanceRecipe()
            val houseNameList = RecipeCategory.houseRecipe()
            val nameList = mutableListOf(simpleNameList, advanceNameList, houseNameList)

            for (eachNameList in nameList){
                for (elementName in eachNameList){
                    val elementUUID = UUID.randomUUID().toString()
                    objectUUIDToNameMap[elementUUID] = elementName
                }
            }
        }


        private fun generateMedicinalUUIDToNameMap(){
            val primaryNameList = MedicinalCategory.defaultPrimaryMedicinal()
            val nameList = mutableListOf(primaryNameList)
            for (eachNameList in nameList){
                for (elementName in eachNameList){
                    val elementUUID = UUID.randomUUID().toString()
                    objectUUIDToNameMap[elementUUID] = elementName
                }
            }
        }


    }


}
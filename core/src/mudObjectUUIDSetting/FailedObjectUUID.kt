package mudObjectUUIDSetting

import com.badlogic.gdx.utils.JsonReader
import com.inos.mudgdx.NPFileHandle
import com.inos.mudgdx.SaveMapFiles

class FailedObjectUUID {


    companion object {

       val failedObjectUUIDMap = mutableMapOf<String, String>()


        fun failedObjectUUIDMap() {
            if (!NPFileHandle.failedObjectUUIDMapFH().exists()) {
                generateMap()
            }
            extractMap()
        }


        private fun generateMap() {
            val listMap = mutableMapOf<String, String>()
          //  listMap["tree"] = mutableMapOf("1")
          //  listMap["flower"] = mutableMapOf("a")
            SaveMapFiles.saveFailedObjectUUIDMap()
        }


        private fun extractMap() {

            val fileHandle = NPFileHandle.failedObjectUUIDMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            failedObjectUUIDMap.clear()

            for ( element in jsonValueMap) {

                // val objectName = element.name

                for (component in element){

                    if (component.name != "class") {
                        val nameString = element.getString(component.name)
                        failedObjectUUIDMap[element.name] = nameString
                    }
                }
            }
        }




    }

}


package mudObjectUUIDSetting

import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonWriter
import com.inos.mudgdx.NPFileHandle

class NoNameObjectUUID {


    companion object {

       val noNameObjectUUIDMap = mutableMapOf<String, String>()

        fun noNameObjectUUIDMap() {

            if (!NPFileHandle.noNameObjectUUIDMapFH().exists()) {
                generateMap()
            }
            extractMap()
        }


        private fun generateMap() {
            val listMap = mutableMapOf<String, String>()
          //  listMap["tree"] = mutableMapOf("1")
          //  listMap["flower"] = mutableMapOf("a")

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.noNameObjectUUIDMapFH()
            fileHandle.writeString(json.toJson(listMap), false)

        }



        private fun extractMap() {

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.noNameObjectUUIDMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            noNameObjectUUIDMap.clear()

            for ( element in jsonValueMap) {

                // val objectName = element.name

                for (component in element){

                    if (component.name != "class") {
                        val nameString = element.getString(component.name)
                        noNameObjectUUIDMap[element.name] = nameString
                    }
                }
            }
        }



    }

}


package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

class Skill {

    companion object {

        private val nPCookingRecipeMap = NPCookingMaps.NPCookingRecipeMap()
        private val ppCookingIngredientMap = PlayerProfile.playerProfile.cookingIngredientMap
        private val ppCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap

        fun skillUi(uiStage: Stage): TextButton {

            val skillUiBtn = TableModel.myfuncButton("Skill")
            skillUiBtn.setPosition(DimCon.vpWidth * 0.85f + DimCon.tileWidthInPx * 0.55f, 0f)
            skillUiBtn.setSize(DimCon.tileWidthInPx, DimCon.tileWidthInPx)

            skillUiBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    listSkill(skillUiBtn,uiStage)
                    skillUiBtn.touchable = Touchable.disabled
                }
            })

            skillUiBtn.name = "SkillUiBtn"
            uiStage.addActor(skillUiBtn)

            return skillUiBtn
        }


        fun listSkill(skillUiBtn: TextButton, uiStage: Stage) {

            val skillSectionListWindow = TableModel.myItemSectionWindow("Skills")

            val actorsList = mutableListOf<Actor>()
            val nPSkillMap = PlayerProfile.playerProfile.nPSkillMap

            for ( (skillName, nPSkill) in nPSkillMap) {

                var skillBtn = TableModel.myTextButton(skillName)
                skillBtn.touchable = Touchable.disabled

                if (nPSkill.activated) {

                    skillBtn = TableModel.myfuncButton(skillName)
                    skillBtn.touchable = Touchable.enabled
                    skillBtn.addListener(object : ClickListener() {
                        override fun clicked(event: InputEvent?, x: Float, y: Float) {
                            skillSectionListWindow.addAction(Actions.removeActor(skillSectionListWindow))
                            doingSkillByCreateOrRecipe(skillUiBtn, nPSkill, uiStage)
                        }
                    })
                }
                actorsList.add(skillBtn)
            }

            val itemTable = TableBtnAndWindowAssembly.sectionListTable(actorsList,100f)

            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(skillSectionListWindow, itemTable, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    skillUiBtn.touchable = Touchable.enabled
                    skillSectionListWindow.addAction(Actions.removeActor(skillSectionListWindow))
                }
            })
        }



        private fun doingSkillByCreateOrRecipe(skillUiBtn: TextButton, nPSkill: NPSkill, uiStage: Stage) {

           // skillBtn.touchable = Touchable.disabled
            when (nPSkill.name) {"Cooking" -> displayCookingOption(skillUiBtn, uiStage)
                    // "material" -> DisplayRecipeInfo.displayMaterialContent(nPItemContentMap, textBtn, uiStage)
                    //  "foodEffect" -> DisplayRecipeInfo.displayFoodEffectContent(nPItemContentMap, textBtn, uiStage)
                    }
        }


        private fun displayCookingOption(skillUiBtn: TextButton, uiStage: Stage){

            val createBtn = TableModel.myfuncButton("Create")
            val recipeBtn = TableModel.myfuncButton("Recipe")
            val optionsList = mutableListOf<Actor>(createBtn, recipeBtn)
            val closeBtn = TableModel.myCloseTextButton("X")

            val optionWindow = TableModel.mySkillOptionWindow("Cooking")

            val optionListTable = TableBtnAndWindowAssembly.sectionListTable(optionsList,100f)

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(optionWindow, optionListTable, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    optionWindow.addAction(Actions.removeActor(optionWindow))
                    skillUiBtn.touchable = Touchable.enabled
                }
            })

            recipeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    optionWindow.addAction(Actions.removeActor(optionWindow))
                    CookingWithRecipe.displayLearntRecipe(skillUiBtn, uiStage)
                }
            })


            createBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    optionWindow.addAction(Actions.removeActor(optionWindow))
                    CookingWithCreate.displayCookingType(skillUiBtn, uiStage)
                   // mudCookingSkill.createCooking.CreateCooking.displayCookingType(skillUiBtn, uiStage)
                }
            })

        }





    }
}
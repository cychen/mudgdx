package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Json

class GameExit {

    companion object {

        fun exit(uiStage: Stage, player: Image) {

            val btn = TableModel.myTextButton("Exit")
            btn.setSize(30f, 30f)
            btn.setPosition(DimCon.vpWidth * 0.995f - btn.width, DimCon.vpHeight - 30f)

            uiStage.addActor(btn)

            btn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    btn.touchable = Touchable.disabled
                    confirmToExit(btn, uiStage, player)
                }

            })
        }

        fun confirmToExit(btn: TextButton, uiStage: Stage, player: Image){
            val exitGame = TableModel.myWindow("Exit the game?")

            val yesBtn = TableModel.myTextButton("Yes")
            val noBtn = TableModel.myTextButton("No")

            uiStage.addActor(exitGame)

            exitGame.add(yesBtn).padRight(20f)
            exitGame.add(noBtn)

            exitGame.addAction(Actions.sequence(
                    Actions.delay(5f),
                    Actions.run{btn.touchable = Touchable.enabled},
                    Actions.removeActor(exitGame)
            ))

            yesBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {

                    ShowInfo.infoLabel("Exit The Game", uiStage)

                    val playerProfile = PlayerProfile.playerProfile
                    playerProfile.positionXY[0] = player.x
                    playerProfile.positionXY[1] = player.y

                    val json = Json()
                    val fileHandle = NPFileHandle.playerFileHandle(playerProfile.playerName)
                    json.toJson((playerProfile), fileHandle)

                    SaveMapFiles.saveMapFiles()

                    yesBtn.clearListeners()

                    uiStage.addAction(Actions.removeActor(exitGame))

                    Gdx.app.exit()
                }

            })

            noBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    exitGame.clearListeners()
                    uiStage.addAction(Actions.removeActor(exitGame))
                    btn.touchable = Touchable.enabled
                }
            })


        }





    }
}
package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

class TradeAct {

    companion object {


        fun startTradeAct( npcObj: ObjImage, uiStage: Stage){

            npcObj.available = false

            val tradeWindow = TableModel.myWindow(" Need help ?")
            val sellBtn = TableModel.myfuncButton("sell")
            val buyBtn = TableModel.myfuncButton("buy")
            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addSellBuyAndCloseBtnToWindow(tradeWindow, sellBtn, buyBtn, closeBtn, uiStage)

            tradeWindow.addAction(Actions.sequence(
                    Actions.delay(3f),
                    Actions.run{npcObj.available = true},
                    Actions.removeActor(tradeWindow)
            ))

            closeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    closeBtn.clearListeners()
                    tradeWindow.addAction(Actions.removeActor(tradeWindow))
                    npcObj.available = true
                }
            })

            sellBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    sellBtn.clearListeners()
                    tradeWindow.addAction(Actions.removeActor(tradeWindow))
                    SellBagItem.sell(npcObj, uiStage)
                }
            })

            buyBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    buyBtn.clearListeners()
                    tradeWindow.addAction(Actions.removeActor(tradeWindow))
                    BuyStoreItem.buy(npcObj, uiStage)
                }
            })

        }

    }
}
package com.inos.mudgdx

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align
import com.badlogic.gdx.utils.viewport.FitViewport


class LogInScreen : Screen {

    lateinit var stage: Stage
    lateinit var loginTexture: Texture
    lateinit var loginImage : Image
    lateinit var uiSkin: Skin
    private var charName = "New Player"
    lateinit var messageLabel: Label

    override fun show() {
        uiSkin = Skin(Gdx.files.internal("UISkin/uiskin.json"))
        messageLabel = Label("Name Not Found", uiSkin)
        messageLabel.isVisible = false
        loginTexture = Texture(Gdx.files.internal("loginCover.png"))
        loginImage = Image(loginTexture)

        val viewport = FitViewport(
                loginImage.width,
                loginImage.height
        )

        stage = Stage(viewport)

        stage.addActor(loginImage)
        stage.addActor(logInTextField())
        stage.addActor(enterButton())
        stage.addActor(createNewCharButton())
        stage.addActor(messageLabel)

        Gdx.input.inputProcessor = stage
    }


    fun logInTextField() : TextField {

        val textField = TextField("charName", uiSkin)

        textField.setAlignment(Align.center)
        textField.setSize(150f, 50f)
        textField.setPosition(
                -textField.width/2f + loginImage.width/2f,
                -textField.height/2f + loginImage.height * 0.4f
        )

        textField.setTextFieldListener( object : TextField.TextFieldListener {
            override fun keyTyped(textField: TextField?, c: Char) {
                charName = textField!!.text

            }
        })
        return textField
    }



    fun enterButton() : TextButton {
        val button = TextButton("ENTER", uiSkin)
        button.align(Align.center)
        button.setSize(150f,50f)
        button.setPosition(
                -button.width/2f + loginImage.width/2f,
                -button.height/2f + loginImage.height * 0.25f
        )

        button.addListener( object : ClickListener(){
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                if (NPFileHandle.playerFileHandle("${charName.toLowerCase()}").exists()){
                    val applistener = Gdx.app.applicationListener as Game
                    applistener.screen = CurtainUp(charName)

                } else {

                    messageLabel.setAlignment(Align.center)
                    messageLabel.setSize(200f,50f)
                    messageLabel.color = Color.RED
                    messageLabel.setPosition(
                            -messageLabel.width/2f + loginImage.width * 0.5f,
                            -messageLabel.height/2f + loginImage.height * 0.5f
                    )
                    messageLabel.addAction(Actions.sequence(
                            Actions.visible(true),
                            Actions.delay(2f),
                            Actions.visible(false)
                    ))
                }

            }
        })
        return button
    }

    fun createNewCharButton(): TextButton {
        val button = TextButton("New Character", uiSkin)
        button.align(Align.center)
        button.setSize(110f,50f)
        button.setPosition(
                -button.width/2f + loginImage.width * 0.85f,
                -button.height/2f + loginImage.height * 0.72f
        )

        button.addListener( object : ClickListener(){
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                val listener = Gdx.app.applicationListener as Game
                listener.screen = CreateNewCharScreen()
            }
        })
        return button
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0.2f,0.4f,0.6f,1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        stage.act()
        stage.draw()
    }

    override fun dispose() {
        stage.dispose()
        loginTexture.dispose()
        uiSkin.dispose()
    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width,height)
    }

    override fun pause() {}

    override fun resume() {}

    override fun hide() { dispose() }


}
package com.inos.mudgdx

import mudCookingSetting.condiment.CookingCondiment
import mudCookingSetting.condiment.InnCookingCondiment
import mudCookingSetting.ingredient.CookingIngredient
import mudCookingSetting.recipe.CookingRecipe
import mudCookingSetting.food.Food


class NPCookingMaps {


    companion object {

        fun setUp(){

            setUpCookingCondimentMap()
            setUpCookingIngredientMap()
            setUpCookingRecipeMap()
            setUpInnCookingCondimentMap()
            setUpRecipeFoodMap()
            setUpNoNameFoodMap()
            setUpFailedFoodMap()
        }


        private fun setUpCookingCondimentMap() {
            mudCookingSetting.condiment.CookingCondiment.cookingCondimentMap()
        }

        private fun setUpCookingIngredientMap() {
            mudCookingSetting.ingredient.CookingIngredient.cookingIngredientMap()
        }

        private fun setUpCookingRecipeMap() {
            CookingRecipe.generateCookingRecipeMap()
        }

        private fun setUpInnCookingCondimentMap(){
            InnCookingCondiment.generateInnCookingCondimentMap()
        }


        fun NPCookingCondimentMap(): MutableMap<String, CookingCondiment>{
            return CookingCondiment.cookingCondimentMap
        }

        fun NPCookingIngredientMap(): MutableMap<String, CookingIngredient>{
            return CookingIngredient.cookingIngredientMap
        }

        fun NPCookingRecipeMap(): MutableMap<String, CookingRecipe>{
            return CookingRecipe.cookingRecipeMap
        }

        fun NPInnCookingCondimentMap():MutableMap<String, CookingCondiment> {
            return InnCookingCondiment.innCookingCondimentMap
        }

        fun NPCookingTypeList():MutableList<String> {
            return mutableListOf("Simple", "Advance", "House")
        }

        private fun setUpRecipeFoodMap(){
            //extract data
            mudCookingSetting.food.Food.generateRecipeFoodMap()
        }


        fun NPRecipeFoodMap(): MutableMap<String, Food> {
            //reference
            return mudCookingSetting.food.Food.recipeFoodMap()
        }

        private fun setUpNoNameFoodMap(){
            mudCookingSetting.food.Food.generateNoNameFoodMap()
        }

        private fun setUpFailedFoodMap(){
            mudCookingSetting.food.Food.generateFailedFoodMap()
        }

        fun NPNoNameFoodMap(): MutableMap<String, Food> {
            //*** reference to temporary address
            return mudCookingSetting.food.Food.noNameFoodMap()
        }

        fun NPFailedFoodMap(): MutableMap<String, Food> {
            //*** reference
            return mudCookingSetting.food.Food.failedFoodMap()
        }

        fun updateNPNoNameFoodMap(newNoNameFood: Food){
            //*** reference
            mudCookingSetting.food.NoNameFood.updateNoNameFoodMap(newNoNameFood)
        }

        fun updateNPCreatedCookingRecipeMap(newRecipe: CookingRecipe){
            //*** reference
            mudCookingSetting.recipe.CreatedCookingRecipe.updateCreatedCookingRecipeMap(newRecipe)
        }

        fun updateNPFailedFoodMap(newFailedFood: Food) {
            //*** reference to FailedFoodMap and FailedObjectUUIDMap
            mudCookingSetting.food.FailedFood.updateFailedFoodMap(newFailedFood)
        }







    }
}
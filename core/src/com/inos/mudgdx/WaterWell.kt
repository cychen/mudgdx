package com.inos.mudgdx


import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Json


class WaterWell {

    companion object {

        fun useWell(player: Image, objImage: ObjImage, uiStage: Stage) {

            if (objImage.available) {

                if (objImage.clickFlag) {

                    if (objImage.collideWithPlayerFlag) {

                        wellWindow(player, objImage, uiStage)

                    } else {

                        ShowInfo.farAwayInfo(objImage,uiStage)
                    }
                }
            }
        }


        fun wellWindow(player: Image, objImage: ObjImage, uiStage: Stage){

            objImage.available = false

            val playerProfile = PlayerProfile.playerProfile

            val wellWindow = TableModel.myWindow("Well")
            val useBtn = TableModel.myfuncButton("use")
            val saveGameBtn = TableModel.myfuncButton("save")
            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addSellBuyAndCloseBtnToWindow(wellWindow, useBtn, saveGameBtn, closeBtn, uiStage)


            wellWindow.addAction(Actions.sequence(
                    Actions.delay(3f),
                    Actions.run{objImage.available = true},
                    Actions.removeActor(wellWindow)
            ))


            closeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    wellWindow.addAction(Actions.removeActor(wellWindow))
                    objImage.available = true
                }
            })


            useBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    wellWindow.addAction(Actions.removeActor(wellWindow))
                    ShowInfo.infoLabel("collect water", uiStage)
                    objImage.available = true
                }
            })


            saveGameBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    playerProfile.positionXY[0] = player.x
                    playerProfile.positionXY[1] = player.y

                    val json = Json()
                    val fileHandle = NPFileHandle.playerFileHandle(playerProfile.playerName)
                        json.toJson((playerProfile),fileHandle)

                    SaveMapFiles.saveMapFiles()

                    wellWindow.addAction(Actions.removeActor(wellWindow))
                    ShowInfo.infoLabel("save Game", uiStage)
                    objImage.available = true
                }
            })


        }


    }
}
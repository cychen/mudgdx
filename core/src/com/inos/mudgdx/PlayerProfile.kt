package com.inos.mudgdx

import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonWriter
import com.badlogic.gdx.utils.TimeUtils
import java.io.Serializable
import java.util.*


class PlayerProfile : Serializable {

    var UUID: String = "UUID"
    var playerName : String = "newbie"
    var mapName : String = "arworld1"
    var positionXY : Array<Float> = arrayOf(0f,0f)
    var charTextureFileName : String = "charImage/blueP.png"

    var baseLifeAttributeMap = mutableMapOf<String, Int>()

    var baseMainAttributeMap = mutableMapOf<String, Int>()
    var baseSpecialAttributeMap = mutableMapOf<String, Int>()

    var foodMainAttributeMap = mutableMapOf<String, Int>()
    var medicineMainAttributeMap = mutableMapOf<String, Int>()

    var foodSpecialAttributeMap = mutableMapOf<String, Int>()
    var medicineSpecialAttributeMap = mutableMapOf<String, Int>()

    var medicinalMap = mutableMapOf<String, Int>()
    var cookingCondimentMap = mutableMapOf<String, Int>()
    var cookingIngredientMap = mutableMapOf<String, Int>()
    var cookingRecipeMap = mutableMapOf<String, Int>()
    var learntCookingRecipe = mutableListOf<String>()
    var learntHerbalismRecipe = mutableListOf<String>()
    var learntWeaponryRecipe = mutableListOf<String>()
    var learntArmoryRecipe = mutableListOf<String>()
    var nPSkillMap = mutableMapOf<String,NPSkill>()
    var money : Int = 0
    var recipeFoodMap = mutableMapOf<String,Int>()
    var noNameFoodMap = mutableMapOf<String,Int>()
    var failedFoodMap = mutableMapOf<String, Int>()
    var foodEffectStopTime: Long = TimeUtils.millis()

    var medicineEffectStopTime: Long = TimeUtils.millis()




    companion object {

        private val objectUUIDToNameMap = mudObjectUUIDSetting.ObjectUUIDToName.objectUUIDToNameMap

        var playerProfile = PlayerProfile()

        fun newPlayer(charName: String) {

            var playerUUID : String

            do {
                playerUUID = UUID.randomUUID().toString()
            } while (objectUUIDToNameMap.contains(playerUUID))
           // playerProfile.UUID = UUID.randomUUID().toString()

            playerProfile.UUID = playerUUID
            playerProfile.playerName = charName

            objectUUIDToNameMap[playerUUID] = charName

            playerProfile.positionXY = arrayOf(DimCon.vpWidth*0.5f,DimCon.vpHeight*0.5f)
            playerProfile.mapName = "arworld1"
            playerProfile.charTextureFileName ="charImage/blueP.png"

            val baseLifeAttributeMap = playerMaps.PlayerAttributeMap.baseLifeAttributeNameToValueMap()
            for ((lifeAttrName, lifeAttrValue) in baseLifeAttributeMap){
                val attrUUID = FindUUID.findUUID(lifeAttrName)
                playerProfile.baseLifeAttributeMap[attrUUID] = lifeAttrValue
            }

            val baseMainAttributeNameMap = playerMaps.PlayerAttributeMap.baseMainAttributeNameToValueMap()
            for ((mainAttrName, mainAttrValue) in baseMainAttributeNameMap) {
                val attrUUID = FindUUID.findUUID(mainAttrName)
                playerProfile.baseMainAttributeMap[attrUUID] = mainAttrValue
            }


            val baseSpecialAttributeNameMap = playerMaps.PlayerAttributeMap.baseSpecialAttributeNameToValueMap()
            for ((specialAttrName, specialAttrValue) in baseSpecialAttributeNameMap){
                val attrUUID = FindUUID.findUUID(specialAttrName)
                playerProfile.baseSpecialAttributeMap[attrUUID] = specialAttrValue
            }

            val foodMainAttributeNameList = playerMaps.PlayerAttributeMap.foodMainAttributeNameList()
            for (attrName in foodMainAttributeNameList){
                val attrUUID = FindUUID.findUUID(attrName)
                playerProfile.foodMainAttributeMap[attrUUID] = 0
            }


            val foodSpecialAttributeNameList = playerMaps.PlayerAttributeMap.foodSpecialAttributeNameList()
            for (attrName in foodSpecialAttributeNameList){
                val attrUUID = FindUUID.findUUID(attrName)
                playerProfile.foodSpecialAttributeMap[attrUUID] = 0
            }


            playerProfile.medicinalMap = mutableMapOf()
            playerProfile.cookingCondimentMap = mutableMapOf()
            playerProfile.cookingIngredientMap = mutableMapOf()
            playerProfile.cookingRecipeMap = mutableMapOf()
            playerProfile.learntCookingRecipe = mutableListOf()
            playerProfile.learntHerbalismRecipe = mutableListOf()
            playerProfile.learntWeaponryRecipe = mutableListOf()
            playerProfile.learntArmoryRecipe = mutableListOf()
            playerProfile.nPSkillMap = NPSkill.nPSkillMap()
            playerProfile.money = 0
            playerProfile.recipeFoodMap = mutableMapOf()
            playerProfile.noNameFoodMap = mutableMapOf()
            playerProfile.failedFoodMap = mutableMapOf()

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val playerProfileString = json.toJson(playerProfile)

            val playerFileHandle = NPFileHandle.playerFileHandle(charName)
            playerFileHandle.writeString(playerProfileString,false)

            System.out.println(json.prettyPrint(playerProfile))
        }


        fun loadPlayerProfile(charName: String): PlayerProfile {
            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val playerFileHandle = NPFileHandle.playerFileHandle(charName)
            playerProfile = json.fromJson(PlayerProfile::class.java, playerFileHandle)
            System.out.println(json.prettyPrint(playerProfile))

            return playerProfile

        }




    }
}
package com.inos.mudgdx


import com.badlogic.gdx.scenes.scene2d.Stage


class Farm {

    companion object {

        fun farmer(npcObj: ObjImage, uiStage: Stage) {

            if (npcObj.available) {

                if (npcObj.clickFlag) {

                    if (npcObj.collideWithPlayerFlag) {

                        farmerAct(npcObj, uiStage)
                        npcObj.collideWithPlayerFlag = false

                    } else {

                        ShowInfo.farAwayInfo(npcObj, uiStage)

                    }
                }
            }
        }


        fun farmerAct(npcObj: ObjImage, uiStage: Stage) {

          //  val skillActivated = PlayerProfile.playerProfile.nPSkill["Cooking"]
          //          ?: throw NullPointerException("Cooking is not found in nPSkill")
         //   if (skillActivated.activated) {
                TradeAct.startTradeAct(npcObj, uiStage)
        //    } else {
         //       Inn.innKeeperConversation(npcObj, uiStage)
        //    }

        }



    }
}
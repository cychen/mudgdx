package com.inos.mudgdx

class FindUUID {

    companion object {

        private val nPObjectUUIDToNameMap = NPUUIDMaps.NPObjectUUIDToNameMap()
        private val nPNoNameObjectUUIDMap = NPUUIDMaps.NPNoNameObjectUUIDMap()

        fun findUUID(itemName:String): String {

            for ( (objUUID, objName) in nPObjectUUIDToNameMap){
                if (objName == itemName){
                    return  objUUID
                }
            }
            return "noObjectUUIDFound"
        }


        fun findNoNameUUID(itemName:String): String {

            for ( (objectUUID, objectName) in nPNoNameObjectUUIDMap){
                if (objectName == itemName){
                    return  objectUUID
                }
            }
            return "noObjectUUIDFound"
        }




    }
}
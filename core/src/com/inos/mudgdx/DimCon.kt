package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.ui.Skin

object DimCon {

    val tileWidthInPx : Float = 32f

    val px: Float = 1f

    val lengthScale : Float = 1/px

    //view port width in Unit
   // val vpUWidth : Float = 640f * lengthScale
    val vpWidth : Float = 32f * 20

    //view port height in unit
   // val vpUHeight : Float = 480f * lengthScale
   val vpHeight : Float = 32f * 15

   val mapCenterPos = arrayOf(DimCon.vpWidth * 0.5f, DimCon.vpHeight * 0.5f)

   val walkSpeed : Float = 0.3f * tileWidthInPx

    val UISkin : Skin = Skin(Gdx.files.internal("UISkin/uiskin.json"))

    val gtosfactor = 1000
    val stocfactor = 1000

    val baseLife = 1000 // baseUnit

    val baseStrength = 100 // baseUnit
    val baseAgility = 100 // baseUnit
    val baseFocus = 100  //baseUnit

    val baseEffectLasting = 0  // baseUnit = 10min/unit (times)
    val baseLifeRecovery = 10 // baseUnit = 1% per unitTime

    val primaryMainMax = 10
    //set the maximum percentage of base attribute points(strength,agility,focus) that primary ingredients possess
    // example, in program, recipe: flour: str 3, need to convert this number to real points,
    // means  3% * primary strength(100) = 3 points
    val primarySpecialMax = 3
    //set the maximum factor of the special effect units (effectLasting, lifeRecovery) that primary ingredients possess
    val primaryGainMax = 3

    //set the maximum gain factor (percentage) that primary condiment can enhance (add extra) the attribute and effect
    // from the primary ingredient in food after cooking

    val intermediateMainMax = 15  //intermediate ingredient base attribute maximum (percentage)
    val intermediateSpecialMax = 6 //intermediate ingredient special attribute maximum
    val intermediateGainMax = 15 //intermediate condiment gain factor maximum

    val advanceMainMax = 30  //advanced ingredient base attribute maximum
    val advanceSpecialMax = 10 //advanced ingredient special attribute maximum
    val advanceGainMax = 30 //advanced condiment gain factor maximum

    val foodUniqueG = 3 //extra gain factor for different food rarity
    val foodSpecialG = 2
    val foodCommonG = 1

    val simpleCIQty = 2 //simple cooking ingredient require qty
    val simpleCDQty = 2 //simple cooking condiment require qty
    val advanceCIQty = 4 //advance cooking
    val advanceCDQty = 4
    val houseCIQty = 6
    val houseCDQty = 6

   // val specialGainMax = 15 //named condiment gain factor maximum

}
package com.inos.mudgdx


import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align


class TableBtnAndWindowAssembly {

    companion object {


        fun addTableToWindow(tradeWindow:Window, itemTable:Table, uiStage:Stage) {

            uiStage.addActor(tradeWindow)

            itemTable.padTop(2f).padLeft(10f).padRight(10f)

            val itemScrollPane = TableModel.myScrollPane(itemTable)

            tradeWindow.add(itemScrollPane)

           // tradeWindow.titleTable.add(closeBtn)

        }

        fun addTableAndCloseBtnToWindow(tradeWindow:Window, itemTable:Table, closeBtn:TextButton, uiStage:Stage) {

            uiStage.addActor(tradeWindow)

            itemTable.padTop(2f).padLeft(10f).padRight(10f)

            val itemScrollPane = TableModel.myScrollPane(itemTable)

            tradeWindow.add(itemScrollPane)

            tradeWindow.titleTable.add(closeBtn)

        }


        fun addTableYesNoBtnToWindow(conversationWindow:Window, table:Table, yesBtn:TextButton, noBtn:TextButton, uiStage:Stage) {

            addTableToWindow(conversationWindow, table, uiStage)

            val yesNoTable = yesNoBtnTable(yesBtn, noBtn, 80f, 80f, 40f)
            yesNoTable.padTop(5f).padLeft(40f).padRight(40f)

            conversationWindow.row()
            conversationWindow.add(yesNoTable)

        }

        fun addTableYesNoAndCloseBtnToWindow(conversationWindow:Window, table:Table, yesBtn:TextButton, noBtn:TextButton, closeBtn:TextButton, uiStage:Stage) {


            addTableAndCloseBtnToWindow(conversationWindow, table, closeBtn, uiStage)

            val yesNoTable = yesNoBtnTable(yesBtn, noBtn, 80f, 80f, 40f)
            yesNoTable.padTop(5f).padLeft(40f).padRight(40f)

            conversationWindow.row()
            conversationWindow.add(yesNoTable)

        }

        fun addTradeInfoTableToWindow(infoWindow:Window, itemName: String, itemPrice:Int, qtyTF: TextField, yesBtn:TextButton, noBtn: TextButton,uiStage:Stage) {

            uiStage.addActor(infoWindow)

            val infoTable = tradeInfoTable(itemName, itemPrice, qtyTF)
            val yesNoTable = yesNoBtnTable(yesBtn, noBtn, 80f, 80f, 40f)
            yesNoTable.padTop(5f).padLeft(40f).padRight(40f)
            infoWindow.add(infoTable)
            infoWindow.row()
            infoWindow.add(yesNoTable)

        }

        fun addItemAndQtyToTable(itemButton:TextButton, qty:Int, itemTable: Table):Table {
            if (qty != 0) {
                val qtyLabel = TableModel.myLabel("$qty")
                itemTable.add(itemButton)
                itemTable.add(qtyLabel)
                itemTable.row()
            }
            return itemTable
        }

        fun addItemAndPriceToTable(itemButton: TextButton, itemPrice:Int, itemTable: Table): Table {
            itemTable.add(itemButton)
            val itemPriceTable = TableModel.itemPriceTable(itemPrice)
            itemTable.add(itemPriceTable)
            itemTable.row()
            return itemTable
        }


        fun addMapItemToTable(title:String, itemMap:MutableMap<String,Int>, itemTable:Table): Table {
            val objectUUIDToNameMap = NPUUIDMaps.NPObjectUUIDToNameMap()
            val titleLabel = TableModel.myTopLeftLabel(title)
            val titleVoidLabel = TableModel.myTopLeftLabel("")

            TableBtnAndWindowAssembly.addTwoLabelsToTable(titleLabel, titleVoidLabel,itemTable)

            for ((key, value) in itemMap)
            {
                val itemName = objectUUIDToNameMap[key].toString()
                val itemNameLabel = TableModel.myTopLeftLabel("    $itemName")
                val itemQtyLabel = TableModel.myLabel("$value")
                addTwoLabelsToTable(itemNameLabel, itemQtyLabel, itemTable)
            }
            return itemTable
        }


        fun addUUIDMapItemToTable(title:String, uuidItemMap:MutableMap<String,Int>, itemTable:Table): Table {
            val objectUUIDToNameMap = NPUUIDMaps.NPObjectUUIDToNameMap()
            val titleLabel = TableModel.myTopLeftLabel(title)
            val titleVoidLabel = TableModel.myTopLeftLabel("")

            TableBtnAndWindowAssembly.addTwoLabelsToTable(titleLabel, titleVoidLabel,itemTable)

            for ((key, value) in uuidItemMap)
            {
                val itemName = objectUUIDToNameMap[key].toString()
                val itemNameLabel = TableModel.myTopLeftLabel("    $itemName")
                val itemQtyLabel = TableModel.myLabel("$value")
                addTwoLabelsToTable(itemNameLabel, itemQtyLabel, itemTable)
            }
            return itemTable
        }


        fun addTwoLabelsToTable(label1: Label, label2:Label, itemTable: Table): Table {
            itemTable.add(label1)
            itemTable.add(label2)
            itemTable.row()
            return itemTable
        }


        fun setSellQtyTFListener(sellQtyTF: TextField, oldQty: Int) {
            sellQtyTF.setAlignment(Align.center)
            sellQtyTF.setTextFieldFilter { _, c ->
                Character.toString(c).matches(Regex("^[0-9]"))
            }

            sellQtyTF.setTextFieldListener { textField, _ ->

                if (textField == null) { sellQtyTF.text = "0" }

                else {
                    when (textField.text) {
                        "" -> { sellQtyTF.text = "0" }
                        else -> { sellQtyTF.text = if (textField.text.toInt() in 0..oldQty)
                            textField.text else oldQty.toString()
                        }
                    }
                }
            }
        }


        fun setBuyQtyTFListener(buyQtyTF: TextField, defaultQty: Int) {
            buyQtyTF.setAlignment(Align.center)
            buyQtyTF.setTextFieldFilter { _, c ->
                Character.toString(c).matches(Regex("^[0-9]"))
            }

            buyQtyTF.setTextFieldListener { textField, _ ->

                if (textField == null) {
                    buyQtyTF.text = defaultQty.toString()
                } else {
                    when (textField.text) {
                        "" -> {
                            buyQtyTF.text = defaultQty.toString()
                        }
                        else -> {
                            buyQtyTF.text = if (textField.text.toInt() in 0..20)
                                textField.text else defaultQty.toString()
                        }
                    }
                }
            }
        }


        fun isBagEmpty(bagItemMapList:MutableList<MutableMap<String,Int>>): Boolean {

            val isEmpty = true

            for (bagItemMap in bagItemMapList){

                if (bagItemMap.isNotEmpty()){
                    return false
                }
            }
            return isEmpty
        }


        fun hasEnoughMaterial(requiredMaterialMap:MutableMap<String,Int>, bagMaterialMap:MutableMap<String, Int>): Boolean {

            for ((requiredMaterialName, requiredMaterialQty) in requiredMaterialMap) {

                val bagContainsThisItemAndQty = bagMaterialMap[requiredMaterialName]?: return false

                if (bagContainsThisItemAndQty < requiredMaterialQty) {

                    return false
                }
            }
            return true
        }


        private fun tradeInfoTable(itemName: String, itemPrice: Int, qtyTF:TextField): Table {

            val infoTable = TableModel.mySellInfoTable()

            val itemButton = TableModel.myTextButton(itemName)
            itemButton.touchable = Touchable.disabled
            val itemPriceTable = TableModel.itemPriceTable(itemPrice)

            infoTable.add(itemButton)
            infoTable.add(qtyTF)
            infoTable.add(itemPriceTable)

            return infoTable
        }


        fun yesNoBtnTable(yesBtn:TextButton, noBtn:TextButton, width1:Float, width2:Float, padRight:Float): Table {
            val yesNoTable = TableModel.twoColumnTable(width1, width2)
            yesNoTable.add(yesBtn).padRight(padRight)
            yesNoTable.add(noBtn)
            return yesNoTable
        }


        fun addYesNoBtnToWindow(funcWindow: Window, yesBtn: TextButton, noBtn: TextButton, uiStage: Stage){
            uiStage.addActor(funcWindow)
            val yesNoTable = yesNoBtnTable(yesBtn, noBtn, 45f, 45f, 10f)
            yesNoTable.padTop(5f)
            funcWindow.add(yesNoTable)
        }


        fun addSellBuyAndCloseBtnToWindow(tradeWindow: Window, sellBtn: TextButton, buyBtn: TextButton, closeBtn:TextButton, uiStage: Stage){
            addYesNoBtnToWindow(tradeWindow, sellBtn, buyBtn, uiStage)
            tradeWindow.titleTable.add(closeBtn)
        }


        fun sectionListTable(actorsList:MutableList<Actor>, tableWidth:Float): Table {
            val itemTable = TableModel.oneColumnTable(tableWidth)
            val likeH = 30f
            for (a in actorsList){
                if (a.height > likeH) {
                    itemTable.add(a).height(a.height)
                } else {itemTable.add(a).height(likeH)}
                itemTable.row()
            }
            return itemTable
        }


    }
}


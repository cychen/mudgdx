package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener


class SellBagItem {

    companion object {

        private val ppMedicinalMap = PlayerProfile.playerProfile.medicinalMap
        private val ppCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap
        private val ppCookingIngredientMap = PlayerProfile.playerProfile.cookingIngredientMap
        private val ppCookingRecipeMap = PlayerProfile.playerProfile.cookingRecipeMap
        private val ppRecipeFoodMap = PlayerProfile.playerProfile.recipeFoodMap
        private val ppNoNameFoodMap = PlayerProfile.playerProfile.noNameFoodMap
        private val ppFailedFoodMap = PlayerProfile.playerProfile.failedFoodMap

        private val nPMedicinalMaps = NPMedicineMaps.NPMedicinalMap()
        private val nPCookingIngredientMaps = NPCookingMaps.NPCookingIngredientMap()
        private val nPCookingCondimentMaps = NPCookingMaps.NPCookingCondimentMap()
        private val nPCookingRecipeMaps = NPCookingMaps.NPCookingRecipeMap()
        private val nPRecipeFoodMaps = NPCookingMaps.NPRecipeFoodMap()
        private val nPNoNameFoodMaps = NPCookingMaps.NPNoNameFoodMap()
        private val nPFailedFoodMaps = NPCookingMaps.NPFailedFoodMap()

        private val bagItemMapList = mutableListOf(
                ppMedicinalMap,
                ppCookingCondimentMap,
                ppCookingIngredientMap,
                ppCookingRecipeMap,
                ppRecipeFoodMap,
                ppNoNameFoodMap
        )


        fun sell(npcObj: ObjImage, uiStage: Stage) {

            npcObj.available = false

            val bagUiBtn = uiStage.root.findActor<TextButton>("BagUiBtn")
            bagUiBtn.touchable = Touchable.disabled

            val sellWindow = TableModel.myItemQtyWindow("Click on item to sell")
            val itemTable = TableModel.itemTable()
            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(sellWindow, itemTable, closeBtn, uiStage)

            closeBtn.addListener(object :ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    bagUiBtn.touchable = Touchable.enabled
                    uiStage.addAction(Actions.removeActor(sellWindow))
                    npcObj.available = true
                }
            })
            setUpPPMapItemAndQtyToTable(itemTable, sellWindow, npcObj, bagUiBtn, uiStage)
        }



        private fun setUpPPMapItemAndQtyToTable(itemTable: Table, sellWindow: Window, npcObj: ObjImage, bagUiBtn: TextButton, uiStage: Stage) {

            npcObj.available = false

            if (TableBtnAndWindowAssembly.isBagEmpty(bagItemMapList)) {
                ShowInfo.infoLabel("Nothing To Sell", uiStage)
                npcObj.available = true
                bagUiBtn.touchable = Touchable.enabled

            } else {
                loadPPMapItemAndQtyToTable(itemTable, sellWindow, npcObj, uiStage)
            }
        }


        private fun loadPPMapItemAndQtyToTable(itemTable: Table, sellWindow: Window, npcObj: ObjImage, uiStage: Stage){

            for ((itemUUID, itemQty) in ppMedicinalMap){
                val thisObj = nPMedicinalMaps[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in NPMedicinalMap")
                val itemPrice = thisObj.price
                val itemName = thisObj.name
                addItemAndQtyToTable(itemUUID, itemName, itemPrice, itemQty, ppMedicinalMap, itemTable, sellWindow, npcObj, uiStage)
            }

            for ((itemUUID, itemQty) in ppCookingCondimentMap){
                val thisObj = nPCookingCondimentMaps[itemUUID]?: throw NullPointerException(
                        "$itemUUID Or not found in NPCookingCondimentMap")
                val itemPrice = thisObj.price
                val itemName = thisObj.name
                addItemAndQtyToTable(itemUUID, itemName, itemPrice, itemQty, ppCookingCondimentMap, itemTable, sellWindow, npcObj, uiStage)
            }

            for ((itemUUID, itemQty) in ppCookingIngredientMap){
                val thisObj = nPCookingIngredientMaps[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in NPCookingIngredientMap")
                val itemPrice = thisObj.price
                val itemName = thisObj.name
                addItemAndQtyToTable(itemUUID, itemName, itemPrice, itemQty, ppCookingIngredientMap, itemTable, sellWindow, npcObj, uiStage)
            }

            for ((itemUUID, itemQty) in ppCookingRecipeMap){
                val cookingRecipeObj = nPCookingRecipeMaps[itemUUID]?: throw NullPointerException(
                        "$itemUUID  not found in NPCookingRecipeMap")
                val itemPrice = cookingRecipeObj.price
                val itemName = cookingRecipeObj.name
                addItemAndQtyToTable(itemUUID, itemName, itemPrice, itemQty, ppCookingRecipeMap, itemTable, sellWindow, npcObj, uiStage)
            }

            for ((itemUUID, itemQty) in ppRecipeFoodMap){
                val recipeFoodObj = nPRecipeFoodMaps[itemUUID]?: throw NullPointerException(
                        "$itemUUID  not found in NPRecipeFoodMap")
                val itemPrice = recipeFoodObj.price
                val itemName = recipeFoodObj.name
                addItemAndQtyToTable(itemUUID, itemName, itemPrice, itemQty, ppRecipeFoodMap, itemTable, sellWindow, npcObj, uiStage)
            }

            for ((itemUUID, itemQty) in ppNoNameFoodMap){
                val noNameFoodObj = nPNoNameFoodMaps[itemUUID]?: throw NullPointerException(
                        "$itemUUID  not found in NPNoNameFoodMap")
                val itemPrice = noNameFoodObj.price
                val itemName = noNameFoodObj.name
                addItemAndQtyToTable(itemUUID, itemName, itemPrice, itemQty, ppNoNameFoodMap, itemTable, sellWindow, npcObj, uiStage)
            }

            for ((itemUUID, itemQty) in ppFailedFoodMap){
                val failedFoodObj = nPFailedFoodMaps[itemUUID]?: throw NullPointerException(
                        "$itemUUID  not found in NPFailedFoodMap")
                val itemPrice = failedFoodObj.price
                val itemName = failedFoodObj.name
                addItemAndQtyToTable(itemUUID, itemName, itemPrice, itemQty, ppFailedFoodMap, itemTable, sellWindow, npcObj, uiStage)
            }

        }



        private fun addItemAndQtyToTable(itemUUID: String, itemName:String, itemPrice:Int, itemQty:Int, ppMap: MutableMap<String, Int>, itemTable: Table, tradeWindow: Window, npcObj: ObjImage, uiStage: Stage): Table {

            val itemButton = TableModel.myTextButton(itemName)

            TableBtnAndWindowAssembly.addItemAndQtyToTable(itemButton, itemQty, itemTable)

            itemButton.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiStage.addAction(Actions.removeActor(tradeWindow))
                    displayItemSellInfoTable(itemUUID, itemName, itemPrice, itemQty, ppMap, npcObj, uiStage)
                }
            })

            return itemTable
        }



        private fun displayItemSellInfoTable(itemUUID:String, itemName:String, itemPrice: Int, itemQty:Int, ppMap: MutableMap<String, Int>, npcObj: ObjImage, uiStage: Stage) {

            val sellInfoWindow = TableModel.myTradeInfoWindow("Sell this item?")
            val sellQtyTF = TextField(itemQty.toString(), DimCon.UISkin)
            val yesBtn = TableModel.myfuncButton("Yes")
            val noBtn = TableModel.myfuncButton("No")

            TableBtnAndWindowAssembly.addTradeInfoTableToWindow(sellInfoWindow,itemName, itemPrice, sellQtyTF, yesBtn, noBtn, uiStage)
            TableBtnAndWindowAssembly.setSellQtyTFListener(sellQtyTF, itemQty)

            yesBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {

                    val qtySold = sellQtyTF.text.toInt()
                    PlayerProfile.playerProfile.money += qtySold * itemPrice
                    Bag.moneyUi(uiStage)
                    ppMap[itemUUID] = itemQty - qtySold
                    yesBtn.clearListeners()
                    uiStage.addAction(Actions.removeActor(sellInfoWindow))
                    npcObj.available = true
                    sell(npcObj, uiStage)
                }
            })

            noBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    noBtn.clearListeners()
                    uiStage.addAction(Actions.removeActor(sellInfoWindow))
                    npcObj.available = true
                    sell(npcObj, uiStage)
                }
            })

        }








    }
}

package com.inos.mudgdx

import com.badlogic.gdx.Gdx

class NPSkill {
    var name: String = "Cooking"
    var activated: Boolean = false

    companion object {

        private val nPSkillMap = mutableMapOf<String,NPSkill>()

        private val defaultNPSkillNameList = mutableListOf("Cooking", "Herbalism", "Weaponry", "Armory")

        fun nPSkillMap(): MutableMap<String,NPSkill> {

            for (skill in defaultNPSkillNameList){
                val thisSkill = NPSkill()
                thisSkill.name = skill
                thisSkill.activated = false
                nPSkillMap[skill] = thisSkill
            }

            return nPSkillMap
        }



    }

}


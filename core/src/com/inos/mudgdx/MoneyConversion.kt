package com.inos.mudgdx

class MoneyConversion {

    companion object {

        val gtos = DimCon.gtosfactor
        val stoc = DimCon.stocfactor

        fun toGSC(copper:Int): Array<Int> {
            val c = copper % stoc
            val s = (copper / stoc) % gtos
            val g = (copper / stoc) / gtos
            return arrayOf(g,s,c)
        }

        fun toCopper(GSC: Array<Int>): Int {
            return  GSC[0] * gtos * stoc + GSC[1] * stoc + GSC[2]
        }
    }
}
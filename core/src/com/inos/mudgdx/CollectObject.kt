package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.TimeUtils


class CollectObject {

    companion object {

        fun collectResponse(objImage: ObjImage, uiStage: Stage){

            if (objImage.available) {

                if (objImage.collideWithPlayerFlag) {

                    CollectObject.collectWindow(uiStage, objImage)
                    objImage.collideWithPlayerFlag = false

                } else {

                    ShowInfo.farAwayInfo( objImage, uiStage)

                }
            }
        }


        fun collectWindow(uiStage: Stage, objImage: ObjImage) {

            objImage.available = false

            val collectWindow = TableModel.myWindow("${objImage.name}")
            val collectBtn = TableModel.myTextButton("Collect")
            val closeBtn = TableModel.myTextButton("X")

            collectWindow.titleTable.add(closeBtn)
            collectWindow.add(collectBtn)

            uiStage.addActor(collectWindow)

            collectWindow.addAction(Actions.sequence(
                    Actions.delay(3f),
                    Actions.run { objImage.available = true },
                    Actions.run { objImage.clickFlag = false },
                    Actions.run { objImage.collideWithPlayerFlag = false },
                    Actions.removeActor(collectWindow)
            ))

            collectBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    objImage.lastCollectTime = TimeUtils.millis()
                    collectBtn.clearListeners()
                    collectWindow.addAction(Actions.removeActor(collectWindow))
                    toCollect(uiStage, objImage)
                }
            })

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    collectWindow.addAction(Actions.removeActor(collectWindow))
                    objImage.available = true
                    objImage.clickFlag = false
                    objImage.collideWithPlayerFlag = false

                }
            })
        }


        fun toCollect(uiStage: Stage, objImage: ObjImage){
            val q = objImage.quantity
            val qCollect = (Math.random()*(3) + q).toInt()

            val ppMedicinalMap = PlayerProfile.playerProfile.medicinalMap

            val collectObjNPUUID = FindUUID.findUUID(objImage.name)

            val qNew = (ppMedicinalMap[collectObjNPUUID]?: 0) + qCollect

            ppMedicinalMap[collectObjNPUUID] = qNew

            val mText = "collect $qCollect ${objImage.name}"
            ShowInfo.infoLabel(mText, uiStage)

            objImage.isVisible = false
            objImage.available = false
            objImage.clickFlag = false
            objImage.collideWithPlayerFlag = false
        }






    }
}


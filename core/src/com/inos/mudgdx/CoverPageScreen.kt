package com.inos.mudgdx

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.viewport.FitViewport

class CoverPageScreen:Screen{

    lateinit var stage: Stage
    lateinit var coverImage: Image
    lateinit var coverTexture: Texture
    lateinit var uiSkin: Skin

    override fun show() {

        coverTexture = Texture(Gdx.files.internal("coverpage.png"))
        coverImage = Image(coverTexture)

        val viewport = FitViewport(
                coverImage.width, coverImage.height
        )

        stage = Stage(viewport)



        val gameButton = gameStartButton()


        stage.addActor(coverImage)
        stage.addActor(gameButton)

        Gdx.input.inputProcessor = stage

    }

    fun gameStartButton(): TextButton {
        uiSkin = Skin(Gdx.files.internal("UISkin/uiskin.json"))

        val gameStartBtn = TextButton("GAME START",uiSkin,"default")
        gameStartBtn.setSize(150f,50f)
        gameStartBtn.setPosition(
                -gameStartBtn.width/2f + coverImage.width * 0.5f,
                -gameStartBtn.height/2f + coverImage.height * 0.15f
        )
        gameStartBtn.addListener(object : ClickListener(){
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                val mudgame = Gdx.app.applicationListener as Game
                setUPObjectUUIDToNameMap()
                mudgame.screen = LogInScreen()
            }
        })

        return gameStartBtn
    }

    private fun setUPObjectUUIDToNameMap(){
        mudObjectUUIDSetting.ObjectUUIDToName.UUIDToNameMap()
    }

    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0f,0f,0f,1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        stage.act()
        stage.draw()

    }

    override fun pause() {

    }

    override fun resize(width: Int, height: Int) {
        stage.viewport.update(width,height)
    }

    override fun resume() {

    }

    override fun hide() {
        dispose()
    }

    override fun dispose() {
        stage.dispose()
        coverTexture.dispose()
        uiSkin.dispose()
    }
}
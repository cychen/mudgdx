package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle

class NPFileHandle {

    companion object {

        fun playerFileHandle(charName:String) : FileHandle {
            val playerFileHandle = Gdx.files.local("playerdata/$charName.json")
            return playerFileHandle
        }


        fun playerTextureFileHandle(playerTextureFileName:String) : FileHandle {
            val playerTextureFileHandle = Gdx.files.local(playerTextureFileName)
            return playerTextureFileHandle
        }


        fun playerDefaultTextureFileHandle(): FileHandle {
            val defaultFileHandle = Gdx.files.internal("charImage/redP.png")
            return defaultFileHandle
        }


        fun condimentMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/condimentMap.json")
        }

        fun cookingIngredientMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/cookingIngredientMap.json")
        }

        fun primaryCondimentMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/condiment/primaryCondimentMap.json")
        }

        fun primaryCondimentPriceMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/condiment/primaryCondimentPriceMap.json")
        }

        fun cookingPrimaryIngredientMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/ingredient/primaryIngredientMap.json")
        }

        fun cookingPrimaryIngredientPriceMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/ingredient/primaryIngredientPriceMap.json")
        }

        fun objectUUIDToNameMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/UUID/objectUUIDToNameMap.json")
        }

        fun noNameObjectUUIDMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/UUID/noNameObjectUUIDMap.json")
        }

        fun defaultCookingRecipeMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/defaultCookingRecipeMap.json")
        }

        fun createdCookingRecipeMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/createdCookingRecipeMap.json")
        }

        fun recipeFoodMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/recipeFoodMap.json")
        }

        fun noNameFoodMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/noNameFoodMap.json")
        }


        fun medicinalMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/Medicinal/medicinalMap.json")
        }

        fun failedFoodMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/cooking/failedFoodMap.json")
        }

        fun failedObjectUUIDMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/UUID/failedObjectUUIDMap.json")
        }
/*
        fun utilityObjectNameToUUIDListMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/UUID/utilityObjectNameToUUIDListMap.json")
        }

        fun nPCObjectNameToUUIDListMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/UUID/nPCObjectNameToUUIDListMap.json")
        }

        fun mobObjectNameToUUIDListMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/UUID/mobObjectNameToUUIDListMap.json")
        }

        fun movingObjectNameToUUIDListMapFH(): FileHandle {
            return Gdx.files.local("mudItemData/UUID/movingObjectNameToUUIDListMap.json")
        }




        */

    }
}
package com.inos.mudgdx

/*
class CookingWithCreateNew {

    companion object {

        private val ppCookingIngredientMap = PlayerProfile.playerProfile.cookingIngredientMap
        private val ppCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap
        private val ppCookingTypeList = NPCookingMaps.NPCookingTypeList()

        private val ppCookingRecipeMap = PlayerProfile.playerProfile.cookingRecipeMap
        private val ppCreateCookedFoodMap = PlayerProfile.playerProfile.createFoodMap
        private val cookingIngredientMap = NPCookingMaps.NPCookingIngredientMap()
        private val cookingCondimentMap = NPCookingMaps.NPCookingCondimentMap()
        private val wokCookingIngredientMap = DNDMapItem.wokCookingIngredientMap()
        private val wokCookingCondimentMap = DNDMapCondimentItem.wokCookingCondimentMap()

        private val bagItemMapList = mutableListOf(
                ppCookingCondimentMap,
                ppCookingIngredientMap)

        fun displayCookingType(skillUiBtn:TextButton, uiStage: Stage) {

           // skillBtn.touchable = Touchable.disabled

            val cookingTypeListWindow = TableModel.myItemQtyWindow("Cooking Types")

            val closeBtn = TableModel.myCloseTextButton("X")

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    cookingTypeListWindow.addAction(Actions.removeActor(cookingTypeListWindow))
                    skillUiBtn.touchable = Touchable.enabled
                }
            })

            val cookingTypeActorList = mutableListOf<Actor>()

            for (cookingType in ppCookingTypeList) {

                val cookingTypeBtn = TableModel.myfuncButton(cookingType)

                cookingTypeBtn.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        cookingTypeListWindow.addAction(Actions.removeActor(cookingTypeListWindow))
                        selectCookingType(cookingType, skillUiBtn, uiStage)
                    }
                })
                cookingTypeActorList.add(cookingTypeBtn)
            }

            val itemTable = TableBtnAndWindowAssembly.sectionListTable(cookingTypeActorList, 100f)
            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(cookingTypeListWindow, itemTable, closeBtn, uiStage)

        }


        fun selectCookingType(cookingType:String, skillUiBtn: TextButton, uiStage: Stage){

            when (cookingType){
                "Simple" -> startPreparingSimpleCooking(skillUiBtn, uiStage)
                "Advance" -> return
                "House" -> return
                else -> return
            }
        }


        fun startPreparingSimpleCooking(skillUiBtn: TextButton, uiStage: Stage){

           val bagAndWokIngredientWindowList = addCookingIngredientToWok(uiStage)
           val bagAndWokCondimentWindowList = addCookingCondimentToWok(uiStage)
            requireQtyInfoWindow(bagAndWokIngredientWindowList, bagAndWokCondimentWindowList, skillUiBtn, uiStage)
        }


        fun addCookingIngredientToWok(uiStage: Stage): MutableList<Window> {
            val dnd = DragAndDrop()

            val bagTable = Table()
            bagTable.columnDefaults(0).width(130f).height(30f)
            bagTable.debug()

            val wokTable = Table()
            wokTable.columnDefaults(0).width(130f).height(30f)
            wokTable.debug()

            val bagAndWokIngredientWindowList = mutableListOf<Window>()
            val bagWindow = TableModel.myItemQtyWindow("Bag Ingredients")
            val wokWindow = TableModel.myItemQtyWindow("Wok Ingredients")
            bagAndWokIngredientWindowList.add(bagWindow)
            bagAndWokIngredientWindowList.add(wokWindow)

            bagWindow.x = bagWindow.x * 0.5f
            bagWindow.y = bagWindow.y * 0.75f

            TableBtnAndWindowAssembly.addTableToWindow(bagWindow, bagTable, uiStage)

            val x = bagWindow.x + bagWindow.width * 1.02f
            val y = bagWindow.y
            wokWindow.setPosition(x,y)

            TableBtnAndWindowAssembly.addTableToWindow(wokWindow, wokTable, uiStage)
            DNDMapItem.dndBag(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)

            return bagAndWokIngredientWindowList
        }


        fun requireQtyInfoWindow(bagAndWokIngredientWindowList:MutableList<Window>, bagAndWokCondimentWindowList:MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage):Window{
            val requireQtyInfoWindow = TableModel.myCreateCookingWindow("Create Simple Cooking")
            val requireIngredientQtyLabel = TableModel.myLabel("Require Qty: ")
            val requireCondimentQtyLabel = TableModel.myLabel("2 Ingredients, 2 Condiments")
            requireIngredientQtyLabel.setWrap(true)
            requireCondimentQtyLabel.setWrap(true)
            val requireQtyInfoTable = TableModel.oneColumnTable(250f)
            requireQtyInfoTable.add(requireIngredientQtyLabel)
            requireQtyInfoTable.row()
            requireQtyInfoTable.add(requireCondimentQtyLabel)

            val cookBtn = TableModel.myfuncButton("Cook")
            val cancelBtn = TableModel.myfuncButton("Cancel")

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(requireQtyInfoWindow, requireQtyInfoTable, cookBtn, cancelBtn, uiStage)
            requireQtyInfoWindow.x = requireQtyInfoWindow.x * 1.2f
            requireQtyInfoWindow.y = requireQtyInfoWindow.y * 1.3f

            val cookMaterialAndQtyWindowList = mutableListOf<Window>()
            for (w in bagAndWokIngredientWindowList){
                cookMaterialAndQtyWindowList.add(w)
            }
            for (w in bagAndWokCondimentWindowList){
                cookMaterialAndQtyWindowList.add(w)
            }
            cookMaterialAndQtyWindowList.add(requireQtyInfoWindow)

            cookBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    simpleCook(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
                }
            })

            cancelBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    requireQtyInfoWindow.addAction(Actions.removeActor(requireQtyInfoWindow))

                    for (w in bagAndWokIngredientWindowList){
                        w.addAction(Actions.removeActor(w))
                    }

                    for (w in bagAndWokCondimentWindowList){
                        w.addAction(Actions.removeActor(w))
                    }

                    for (ingredient in wokCookingIngredientMap){
                        ppCookingIngredientMap[ingredient.key] =
                                (ppCookingIngredientMap[ingredient.key]?:0) + ingredient.value
                    }

                    for (condiment in wokCookingCondimentMap){
                        ppCookingCondimentMap[condiment.key] =
                                (ppCookingCondimentMap[condiment.key]?:0) + condiment.value
                    }

                    wokCookingCondimentMap.clear()
                    wokCookingIngredientMap.clear()

                    skillUiBtn.touchable = Touchable.enabled
                    //put wok qty back to bag item qty
                }
            })
            return requireQtyInfoWindow
        }


        fun simpleCook(cookMaterialAndQtyWindowList:MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            if (!checkEnoughCI()){

                uiStage.addAction(Actions.sequence(
                        Actions.run {
                            for (w in cookMaterialAndQtyWindowList) {
                                w.isVisible = false
                            }
                        },

                    Actions.run {
                        ShowInfo.infoLabel("Cooking Ingredient Qty not correct", uiStage)
                    },
                    Actions.delay(1.5f),

                    Actions.run {
                        for (w in cookMaterialAndQtyWindowList) {
                            w.isVisible = true
                            w.clearListeners()
                        }
                    }
                ))

            } else if (!checkEnoughCD()){


                uiStage.addAction(Actions.sequence(
                        Actions.run {
                            for (w in cookMaterialAndQtyWindowList){
                                w.isVisible =false
                            }
                        },
                        Actions.run {

                            ShowInfo.infoLabel("Cooking Condiment Qty not correct", uiStage)
                        },
                        Actions.delay(1.5f),
                        Actions.run {


                            for (w in cookMaterialAndQtyWindowList) {
                                w.isVisible = true
                                w.clearListeners()
                            }
                        }
                ))

            } else {
                startCreateSimpleCook(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
            }
        }




        fun startCreateSimpleCook(cookMaterialAndQtyWindowList: MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            val isCookingSuccessful = isCookingSuccessful()
            val chosenIngredientList = chosenIngredientList()
            val chosenIngredientBaseAttributeMap = chosenIngredientAttributeMap("base", chosenIngredientList)
            val chosenIngredientSpecialAttributeMap = chosenIngredientAttributeMap("special", chosenIngredientList)
            val chosenCondimentList = chosenCondimentList()
            val chosenCondimentGainAttributeMap = chosenCondimentAttributeMap(chosenCondimentList)

            val mainGf = chosenCondimentGainAttributeMap["mainGFactor"]?: throw NullPointerException(
                    "mainGFactor not found in chosenCondimentGainAttributeMap"
            )
            val specialGf = chosenCondimentGainAttributeMap["specialGFactor"]?: throw NullPointerException(
                    "specialGFactor not found in chosenCondimentGainAttributeMap"
            )
            val foodRarity = foodRarity()

            val foodRarityG = when (foodRarity){
                "Unique" -> DimCon.foodUniqueG
                "Special" -> DimCon.foodSpecialG
                "Common" -> DimCon.foodCommonG
                else -> throw NullPointerException("$foodRarity not U,S,or C")
            }

            val newCookedFood = Food()
            newCookedFood.foodName = "noName"
            newCookedFood.rarity = foodRarity
            newCookedFood.type = "Simple"

            for (element in chosenIngredientList){
                val qty = (newCookedFood.ingredientMap[element]?: 0 ) + 1
                newCookedFood.ingredientMap[element] = qty
            }

            for (element in chosenCondimentList){
                val qty = (newCookedFood.condimentMap[element]?:0) + 1
                newCookedFood.condimentMap[element] = qty
            }

            for (attribute in chosenIngredientBaseAttributeMap) {
                newCookedFood.mainEffectMap[attribute.key] = attribute.value * mainGf * foodRarityG
            }

            for (attribute in chosenIngredientSpecialAttributeMap){
                newCookedFood.specialEffectMap[attribute.key] = attribute.value * specialGf * foodRarityG
            }

            var i = 1
            if (PlayerProfile.playerProfile.noNameFoodMap.contains("noName$i")){
                i += 1
            } else {
                PlayerProfile.playerProfile.noNameFoodMap["noName$i"] = newCookedFood
            }
            cookFood(cookMaterialAndQtyWindowList, newCookedFood, skillUiBtn, uiStage)

        }



        private fun cookFood(cookMaterialAndQtyWindowList: MutableList<Window>, newCookedFood: Food, skillUiBtn: TextButton, uiStage: Stage) {

            for (w in cookMaterialAndQtyWindowList){
                w.isVisible = false
            }
            val progressB = ProgressBar(0f, 100f, 10f, false, DimCon.UISkin)

            uiStage.addActor(progressB)
            progressB.setAnimateDuration(2f)
            progressB.setPosition(250f, 400f)

            progressB.addAction(Actions.sequence(
                    Actions.run { progressB.value += 100f },
                    Actions.delay(2f),

                    Actions.run { progressB.isVisible = false },
                    //in 2(animationDuration) seconds Gdx will animate moving progressB to its final
                    // value, which is set for 100f here  .

                    Actions.run { ShowInfo.infoLabel(" You get one ${newCookedFood.foodName} food !", uiStage) },

                    Actions.run { askToSaveAsNewCreate(cookMaterialAndQtyWindowList, newCookedFood, skillUiBtn, uiStage) },
                    Actions.removeActor(progressB)
            ))
        }


        private fun askToSaveAsNewCreate(cookMaterialAndQtyWindowList: MutableList<Window>, newCookedFood: Food, skillUiBtn: TextButton, uiStage: Stage) {
            val conversationWindow = TableModel.myConversationWindow(" Conversation ")

            val conversationLabel = TableModel.myTopLeftLabel("Do you want to save this" +
                    "creation as a new recipe?")

            conversationLabel.setWrap(true)
            val conversationTable = TableModel.oneColumnTable(220f)
            conversationTable.add(conversationLabel)

           // val closeBtn = TableModel.myCloseTextButton("X")

            val yesBtn = TableModel.myfuncButton("Yes")
            val noBtn = TableModel.myfuncButton("No")

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(conversationWindow, conversationTable, yesBtn, noBtn, uiStage)
           // TableBtnAndWindowAssembly.addTableYesNoAndCloseBtnToWindow(conversationWindow, conversationTable, yesBtn, noBtn, closeBtn, uiStage)


            closeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    closeBtn.clearListeners()
                    conversationWindow.addAction(Actions.removeActor(conversationWindow))
                }
            })

            yesBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    conversationWindow.addAction(Actions.removeActor(conversationWindow))
                    val nameRecipeWindow = TableModel.myConversationWindow(" Name your new recipe ")

                    val nameRecipeTF = TextField("input recipe name", DimCon.UISkin)
                    nameRecipeTF.setTextFieldListener { textField, _ ->
                        if (textField == null) {
                            ShowInfo.infoLabel("recipe name can not be null", uiStage)
                            // nameRecipeTF.clear()
                            // nameRecipeTF.text
                           // nameRecipeTF.clearListeners()
                        } else {
                            if (textField.text == ""){
                                ShowInfo.infoLabel("recipe Name can not be empty", uiStage)
                            } else {
                                newCookedFood.foodName = textField.text.toString()
                                PlayerProfile.playerProfile.createCookedFoodMap["${textField.text}"] = 1
                                nameRecipeWindow.addAction(Actions.removeActor(nameRecipeWindow))
                                val descriptionWindow = TableModel.myConversationWindow("food description")
                                val descriptionTF = TextField("provide food description", DimCon.UISkin)
                                descriptionTF.setTextFieldListener { textField, _ ->
                                    if (textField == null) {
                                        ShowInfo.infoLabel("description can not be null", uiStage)
                                       // descriptionTF.clearListeners()
                                    } else {
                                        newCookedFood.description = textField.text
                                        descriptionWindow.addAction(Actions.removeActor(descriptionWindow))

                                        val label = TableModel.myGLetter("New Cooking Recipe ${newCookedFood.foodName} has been" +
                                                "created  !!  " )
                                        label.setPosition(DimCon.vpWidth*0.5f - label.width*0.5f, DimCon.vpHeight*0.85f)

                                        uiStage.addActor(label)
                                        label.addAction(Actions.sequence(
                                                Actions.fadeIn(1f),
                                                Actions.delay(1f),
                                                Actions.fadeOut(1f),
                                                Actions.removeActor(label)
                                        ))
                                    }
                                }
                            }
                        }
                                val newCookingRecipe = CookingRecipe()
                        val cookingRecipeMap = NPCookingMaps.NPCookingRecipeMap()
                        cookingRecipeMap["${newCookedFood.foodName}"]= newCookingRecipe
                                newCookingRecipe.category = "cookingRecipe"
                                newCookingRecipe.name = newCookedFood.foodName
                                newCookingRecipe.price = 1
                                newCookingRecipe.rarity = newCookedFood.rarity
                                newCookingRecipe.type = newCookedFood.type
                                newCookingRecipe.inventor = "${PlayerProfile.playerProfile.playerName}"
                                newCookingRecipe.description = newCookedFood.description
                                for ( item in newCookedFood.ingredientMap) {
                                    newCookingRecipe.ingredientMap[item.key] = item.value
                                }
                                for (item in newCookedFood.condimentMap) {
                                    newCookingRecipe.condimentMap[item.key] = item.value
                                }
                        for (item in newCookedFood.mainEffectMap) {
                            newCookingRecipe.mainEffectMap[item.key] = item.value
                        }
                        for (item in newCookedFood.specialEffectMap) {
                            newCookingRecipe.specialEffectMap[item.key] = item.value
                        }
                    //    NPCookingMaps.updateCookingRecipeMap(cookingRecipeMap)
                    }
                    for (w in cookMaterialAndQtyWindowList){
                        w.isVisible = true
                        w.clearListeners()
                    }
                }
            })

            noBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    conversationWindow.addAction(Actions.removeActor(conversationWindow))
                    for (w in cookMaterialAndQtyWindowList){
                        w.isVisible = true
                        w.clearListeners()
                    }
                }
            })
        }



        fun chosenIngredientAttributeMap(attributeType:String, chosenList:MutableList<String>):MutableMap<String,Int>{

            val validAttributeMap = mutableMapOf<String,Int>()
            var ciAttributeMap = mutableMapOf<String, Int>()

            for (element in chosenList) {

                if (element != "") {

                val ci = cookingIngredientMap[element]?: throw NullPointerException ("$element not" +
                        "found in NPCookingIngredientMap")

                when (attributeType) {
                    "base" -> ciAttributeMap = ci.baseAttributeMap
                    "special" -> ciAttributeMap = ci.specialAttributeMap
                }

                for (entry in ciAttributeMap) {
                    val validIBAN = entry.key
                    val ibav = (validAttributeMap[validIBAN] ?: 0) + entry.value
                    validAttributeMap[validIBAN] = ibav
                }
                }
            }

            return validAttributeMap
        }



        private fun chosenCondimentAttributeMap(chosenCondimentList:MutableList<String>):MutableMap<String,Int>{

            val validGainAttributeMap = mutableMapOf<String,Int>()

            for (element in chosenCondimentList) {

                if (element != ""){
                    val ci = cookingCondimentMap[element]?: throw NullPointerException(
                            "$element not found in NPCookingCondimentMap"
                    )
                    val ciAttributeMap = ci.gainAttributeMap

                    for (entry in ciAttributeMap){
                        val validIBAN = entry.key
                        val ibav = (validGainAttributeMap[validIBAN]?:0) + entry.value
                        validGainAttributeMap[validIBAN]= ibav
                    }
                }
            }
            return validGainAttributeMap
        }


        private fun foodRarity(): String {
            // for rarity,assume
            // 1/1000 = unique , 1/100 = special, 1-1/100-1/1000 = 989/1000 common
            //implement in 1-1000 prob range, random n:
            // 500 : unique
            // 1-10 - special
            // else - common

            val probRange = 1000
            val chosenN = (1..probRange).random().toInt()

            return when (chosenN){
                500 -> "Unique"
                in 1..10 -> "Special"
                else -> "Common"
            }

        }

        private fun isCookingSuccessful():Boolean {

            //simple cook
            // 1/100 prob = fail
            // implement
            // chosenN = 50 fail

            val probRange = 100
            val chosenN = (1..probRange).random().toInt()
            return (chosenN == 50)
        }


        private fun chosenIngredientList():MutableList<String> {

            //  1/100 fail
            //  99/100 success

            //simple cook
            //  1/10 prob = two valid items
            //  1-1/10 = 9/10 one valid item


            //  99/100 * 1/10 = 99/1000 twoIngredients
            //  99/100 * 9/10 = 891/1000 oneIngredient

            //  99/1000 * 1/10 = 99/10000 twoIngredient,twoCondiment
            //  99/1000 * 9/10 = 891/10000 twoIngredient,oneCondiment
            //  891/1000 * 1/10 = 891/10000 oneIngredient,twoCondiment
            //  891/1000 * 9/10 = 8019/10000 oneIngredient,oneCondiment


            // 1- 100 fail
            // 101 - 199 two,two
            // 200 - 1090 two, one
            // 1091 - 1981 one, two
            // 1982 - 10000 one, one

            //implement

            // 1 - 100 fail
            // 101 - 199  twoIngredient,twoCondiment
            // 200 - 646 twoIngredient, ACondiment
            // 647 - 1090 twoIngredient,BCondiment

            // 1091 - 1537 AIngredient, twoCondiment
            // 1538 - 1981 BIngredient, twoCondiment

            // 1982 - 3986 AIngredient,ACondiment
            // 3987 - 5991 AIngredient, BCondiment
            // 5992 - 7996 BIngredient, ACondiment
            // 7997 - 10000 BIngredient,BCondiment


            val probRange = 10000
            val chosenN = (1..probRange).random().toInt()

            val wokIngredientPortionList = mutableListOf<String>()

            for (entry in wokCookingIngredientMap) {
                val itemQty = entry.value
                val itemName = entry.key
                for (i in 1..itemQty) {
                    wokIngredientPortionList.add(itemName)
                }
            }

            return when(chosenN) {
                in 1..4500 -> { mutableListOf(wokIngredientPortionList[0]) }
              //  in 4901..5000 -> {  mutableListOf("") }
                in 4501..5500 -> {

                    val twoValidItemList = mutableListOf<String>()
                    for (element in wokIngredientPortionList) {
                        twoValidItemList.add(element)
                    }
                    return twoValidItemList
                }
                in 5501..10000 -> { mutableListOf(wokIngredientPortionList[1]) }
                else -> throw NullPointerException("chosenN: $chosenN out of $probRange range")
            }
        }


        private fun chosenCondimentList():MutableList<String> {

            //simple cook
            // 1/100 prob = no valid item, fail
            // 1/100 prob = two valid items
            // 1-1/100-1/100 = 98/100 one valid item

            //implement, in 1-10000 prob range, random n :
            // 1 - 4900 : itemA valid item
            // 4901- 5000 : no valid items
            // 5001 - 5100 : two valid items
            // 5101 - 10000 : itemB valid

            val probRange = 10000
            val chosenN = (1..probRange).random().toInt()

            val wokCondimentPortionList = mutableListOf<String>()

            for (entry in wokCookingCondimentMap) {
                val itemQty = entry.value
                val itemName = entry.key
                for (i in 1..itemQty) {
                    wokCondimentPortionList.add(itemName)
                }
            }

            return when(chosenN) {
                in 1..4900 -> { mutableListOf(wokCondimentPortionList[0]) }
                in 4901..5000 -> {  mutableListOf("") }
                in 5001..5100 -> {

                    val twoValidItemList = mutableListOf<String>()
                    for (element in wokCondimentPortionList) {
                        twoValidItemList.add(element)
                    }
                    return twoValidItemList
                }
                in 5101..10000 -> { mutableListOf(wokCondimentPortionList[1]) }
                else -> throw NullPointerException("chosenN: $chosenN out of $probRange range")
            }
        }





        private fun addCookingCondimentToWok(uiStage: Stage): MutableList<Window> {
            val dnd = DragAndDrop()

            val bagTable = Table()
            bagTable.columnDefaults(0).width(130f).height(30f)
            bagTable.debug()

            val wokTable = Table()
            wokTable.columnDefaults(0).width(130f).height(30f)
            wokTable.debug()

            val bagWindow = TableModel.myItemQtyWindow("Bag Condiments")
            val wokWindow = TableModel.myItemQtyWindow("Wok Condiments")

            val bagAndWokCondimentWindowList = mutableListOf<Window>()
            bagAndWokCondimentWindowList.add(bagWindow)
            bagAndWokCondimentWindowList.add(wokWindow)

            bagWindow.x = bagWindow.x * 0.5f
            bagWindow.y = bagWindow.y - bagWindow.height * 1.5f
            TableBtnAndWindowAssembly.addTableToWindow(bagWindow, bagTable, uiStage)

            wokWindow.x = bagWindow.x + bagWindow.width * 1.02f
            wokWindow.y = bagWindow.y
            TableBtnAndWindowAssembly.addTableToWindow(wokWindow, wokTable, uiStage)

            DNDMapCondimentItem.dndBag(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)
            return bagAndWokCondimentWindowList
        }


        fun checkEnoughCI(): Boolean {
            var ciQty = 0
            for (entry in wokCookingIngredientMap){ ciQty += entry.value }
            return (ciQty == 2)
        }


        fun checkEnoughCD(): Boolean {
            var cdQty = 0
            for (entry in wokCookingCondimentMap){ cdQty += entry.value }
            return (cdQty == 2)
        }


        private fun ClosedRange<Int>.random() = Math.random() * ((endInclusive + 1) - start) + start







    }
}



*/

package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Stack
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop
import com.badlogic.gdx.utils.Align

class DNDMapCondimentItem {

    companion object {

        private val bagCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap
        private val wokCookingCondimentMap = mutableMapOf<String,Int>()
        private val nPCookingCondimentMap = NPCookingMaps.NPCookingCondimentMap()

        fun wokCookingCondimentMap():MutableMap<String, Int>{
            return wokCookingCondimentMap
        }

        fun dndBag(dnd: DragAndDrop, bagWindow: Window, wokWindow: Window, bagTable: Table, wokTable: Table, uiStage: Stage){
            bagTable.clear()
            val targetWok = dndTargetWok(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)
            addItemToDnd(dnd, targetWok, bagCookingCondimentMap, bagTable, uiStage)
        }

        private fun dndWok(dnd:DragAndDrop, bagWindow: Window, wokWindow: Window, bagTable: Table, wokTable: Table, uiStage: Stage){
            wokTable.clear()
            val targetBag = dndTargetBag(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)
            addItemToDnd(dnd, targetBag, wokCookingCondimentMap, wokTable, uiStage)
        }

        private fun addItemToDnd(dnd: DragAndDrop, dndTarget:DragAndDrop.Target, srcCondimentMap:MutableMap<String,Int>, srcTable:Table, uiStage: Stage){

            for ((itemUUID, itemQty) in srcCondimentMap){
                if (itemQty > 0) {

                    val thisObj = nPCookingCondimentMap[itemUUID]?: throw NullPointerException(
                            "$itemUUID not found in nPCookingCondimentMap")

                    val nameLabel = TableModel.myTransparentLabel(thisObj.name)
                    val qtyLabel = TableModel.myLabel(itemQty.toString())

                    nameLabel.setAlignment(Align.topLeft)
                    qtyLabel.setAlignment(Align.bottomRight)

                    val labelStack = Stack(qtyLabel, nameLabel)
                    srcTable.add(labelStack).fillX
                    srcTable.row()

                    dnd.addSource(dndSource(nameLabel, qtyLabel, itemUUID, srcCondimentMap, uiStage))
                    dnd.addTarget(dndTarget)
                }
            }
        }


        private fun dndSource(srcNameLabel: Label, srcQtyLabel: Label, itemUUID:String, srcCondimentMap:MutableMap<String,Int>, uiStage: Stage):DragAndDrop.Source{

            return object : DragAndDrop.Source(srcNameLabel) {

                override fun dragStart(event: InputEvent?, x: Float, y: Float, pointer: Int): DragAndDrop.Payload {

                    val dndpl = DragAndDrop.Payload()

                    val srcItemQtyOld = srcCondimentMap[itemUUID]?: throw NullPointerException(
                            "$itemUUID not found in srcIngredientMap")

                    srcCondimentMap[itemUUID] = srcItemQtyOld - 1

                    dndpl.validDragActor = TableModel.myTransparentLabel(srcNameLabel.text.toString())
                    dndpl.validDragActor.name = itemUUID
                    dndpl.invalidDragActor = TableModel.myGLabel("not accepted")

                    return dndpl
                }

                override fun dragStop(event: InputEvent?, x: Float, y: Float, pointer: Int, payload: DragAndDrop.Payload?, target: DragAndDrop.Target?) {
                    //when dndTarget.drag(..) return false -> here means target == null

                    if (target == null) {

                        ShowInfo.infoLabel("not added", uiStage)

                        if (payload != null ){
                            val srcItemQtyOld = srcCondimentMap[itemUUID] ?: throw NullPointerException(
                                    "$itemUUID not found in srcIngredientMap")
                            srcCondimentMap[itemUUID] = srcItemQtyOld + 1
                        }
                    }
                    srcQtyLabel.setText(srcCondimentMap[itemUUID].toString())
                }
            }
        }



        private fun dndTargetWok(dnd: DragAndDrop, bagWindow:Window, wokWindow:Window, bagTable:Table, wokTable:Table, uiStage: Stage):DragAndDrop.Target {

            return object : DragAndDrop.Target(wokWindow) {

                override fun drag(source: DragAndDrop.Source?, payload: DragAndDrop.Payload?, x: Float, y: Float, pointer: Int): Boolean {

                    var wokTotalIngredientQty = 0

                    for (entry in wokCookingCondimentMap) {
                        wokTotalIngredientQty += entry.value
                    }

                    if (wokTotalIngredientQty > 1) {
                        //1 : required qty - 1
                        ShowInfo.infoLabel("exceeds max cooking ingredients", uiStage)
                        return false
                    }
                    if (payload == null) {
                        throw NullPointerException("payload is null")
                    }
                    return true
                }

                override fun drop(source: DragAndDrop.Source?, payload: DragAndDrop.Payload?, x: Float, y: Float, pointer: Int) {

                    val droppedItemUUID = payload!!.validDragActor.name.toString()

                    val oldq = bagCookingCondimentMap[droppedItemUUID]?: throw NullPointerException(
                            "$droppedItemUUID not found in bagCookingIngredientMap")

                    //oldq : check the remained qty after dndSource.drop, dnd action success is only confirmed
                    //after drop. During drag, both tgtBag.drag and tgtWok.drag are active, if we try to set
                    // like qty check in fun tgt.drag will likely get wrong result or null. Better confirm it
                    // after source.dragStop and tgt.dro.
                    //show message not enough material and reset item qty if remained qty < 0

                    if (oldq < 0) {
                        ShowInfo.infoLabel("not enough qty", uiStage)
                        bagCookingCondimentMap[droppedItemUUID] = oldq + 1
                    } else {
                        wokCookingCondimentMap[droppedItemUUID] = (wokCookingCondimentMap[droppedItemUUID] ?: 0) + 1
                    }
                    DNDMapCondimentItem.dndWok(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)
                }
            }
        }



        private fun dndTargetBag(dnd: DragAndDrop, bagWindow:Window, wokWindow: Window, bagTable:Table, wokTable: Table, uiStage: Stage):DragAndDrop.Target {

            return object : DragAndDrop.Target(bagWindow) {

                override fun drag(source: DragAndDrop.Source?, payload: DragAndDrop.Payload?, x: Float, y: Float, pointer: Int): Boolean {
                    if (payload == null) {
                        throw NullPointerException("payload is null")
                    }
                    return true
                }

                override fun drop(source: DragAndDrop.Source?, payload: DragAndDrop.Payload?, x: Float, y: Float, pointer: Int) {

                    val droppedItemUUID = payload?.validDragActor?.name.toString()

                    val oldWokqty = wokCookingCondimentMap[droppedItemUUID]?: throw NullPointerException(
                            "$droppedItemUUID not found in wokCookingIngredientMap")

                    if (oldWokqty < 0){
                        ShowInfo.infoLabel("not enough qty", uiStage)
                        wokCookingCondimentMap[droppedItemUUID] = oldWokqty + 1

                    } else {
                        val oldBagqty = bagCookingCondimentMap[droppedItemUUID] ?: throw NullPointerException(
                                "$droppedItemUUID not found in bagCookingIngredientMap")
                        bagCookingCondimentMap[droppedItemUUID] = oldBagqty + 1
                    }
                    DNDMapCondimentItem.dndBag(dnd,bagWindow, wokWindow, bagTable, wokTable, uiStage)
                }
            }

        }



    }
}
package com.inos.mudgdx


import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.utils.Align

class TableModel {

    companion object {

        fun myScrollPane(itemTable: Table): ScrollPane {
            val scrollPane = ScrollPane(itemTable, DimCon.UISkin)
            scrollPane.setFadeScrollBars(false)
            scrollPane.setScrollingDisabled(true,false)
            return scrollPane
        }

        fun oneColumnTable(width:Float) : Table {
            val table = Table()
            table.columnDefaults(0).width(width)
            table.x = DimCon.vpWidth * 0.5f
            table.y = DimCon.vpHeight * 0.5f
            return table
        }

        fun dndTable(): Table {
            val dndTable = Table()
            dndTable.columnDefaults(0).width(130f).height(30f)
            dndTable.debug()
            return dndTable
        }

        fun twoColumnTable(width1:Float, width2:Float): Table {
            val table = Table()
            table.columnDefaults(0).width(width1).height(30f)
            table.columnDefaults(1).width(width2).height(30f)
            table.x = DimCon.vpWidth * 0.5f
            table.y = DimCon.vpHeight * 0.5f
            return table
        }


        fun itemTable(): Table {
            return twoColumnTable(130f,70f)
        }


        fun myGrocerItemTable(): Table {
            return twoColumnTable(130f, 130f)
        }

        fun mySellInfoTable(): Table {

            val sellInfoTable = Table()
            sellInfoTable.columnDefaults(0).width(130f).height(30f).fillX()
            sellInfoTable.columnDefaults(1).width(40f).height(30f).fillX()
            sellInfoTable.columnDefaults(2).width(130f).height(30f).fillX()

            val itemNameLabel = TableModel.myLabel("Name")
            val quantityLabel = TableModel.myLabel("Qty")
            val unitPriceLabel = TableModel.myLabel("Each")
            sellInfoTable.add(itemNameLabel)
            sellInfoTable.add(quantityLabel)
            sellInfoTable.add(unitPriceLabel)
            sellInfoTable.row()
            sellInfoTable.x = DimCon.vpWidth * 0.5f
            sellInfoTable.y = DimCon.vpHeight * 0.5f

            return sellInfoTable
        }

        fun myItemSectionWindow(title:String): Window {
            return windowPos(title, 120f, 150f)
        }

        fun mySkillOptionWindow(title:String): Window {
            return windowPos(title, 120f,90f)
        }


        fun myItemQtyWindow(title:String): Window {
            return windowPos(title, 250f, 150f)
        }

        fun myCreateCookingWindow(title:String): Window {
            return windowPos(title, 250f, 120f)
        }

        fun myItemPriceWindow(title:String): Window {
            return windowPos(title, 300f, 150f)
        }

        fun myTradeInfoWindow(title:String): Window {
            return windowPos(title, 300f, 120f)
        }

        fun myConversationWindow(title: String): Window {
            return windowPos(title, 250f, 200f)
        }


        fun myWindow(title:String): Window {
            return windowPos(title, 120f, 60f)
        }


        fun windowPos(title: String, width:Float, height: Float): Window {
            val window = Window(title, DimCon.UISkin, "dialog")
            window.setSize(width,height)
            window.setKeepWithinStage(true)
            window.titleLabel.setAlignment(Align.center)
            window.titleLabel.style = TableModel.myWindowLabel(title).style
            val x = DimCon.vpWidth * 0.5f - window.width * 0.5f
            val y = DimCon.vpHeight * 0.8f  - window.height
            window.setPosition(x, y)
            window.isMovable = true
            return window
        }


        fun myfuncButton(func:String): TextButton {
            return TextButton(func, DimCon.UISkin)
        }

        fun myCloseTextButton(text:String): TextButton {
            return TextButton(text, DimCon.UISkin, "windowClose")
        }

        fun myNewButton(text: String): TextButton {
            return TextButton(text, DimCon.UISkin, "new")
        }

        fun myTextButton(itemName:String): TextButton {
            return TextButton(itemName, DimCon.UISkin, "WhiteFontBlackBG")
        }

        fun myWindowLabel(labelText:String): Label {
            val l = Label(" $labelText ", DimCon.UISkin, "LightSkyBlueFont")
           // l.setAlignment(Align.center)
            return l
        }

        fun myLabel(labelText:String): Label {
            val l = Label(" $labelText ", DimCon.UISkin, "WhiteFontBlackBG")
            l.setAlignment(Align.center)
            return l
        }

        fun myTransparentLabel(labelText: String): Label {
            val l = Label(labelText, DimCon.UISkin, "SemiTransparentBg")
            return l
        }

        fun myTopLeftLabel(labelText:String): Label {
            val l = Label(labelText , DimCon.UISkin, "WhiteFontBlackBG")
            l.setAlignment(Align.topLeft)
            return l
        }

        fun infoLabel(mText: String): Label {

            val label = Label(" $mText ", DimCon.UISkin, "WhiteFontBlackBG")
            label.x = DimCon.vpWidth * 0.5f - label.width * 0.5f
            label.y = DimCon.vpHeight * 0.8f + label.height * 0.5f
            return label
        }


        fun myGLetter(labelText: String): Label {
            val gl =Label(labelText, DimCon.UISkin, "GoldFont")
            gl.setAlignment(Align.left)
            return gl
        }

        fun mySLetter(labelText: String): Label {
            val sl = Label(labelText, DimCon.UISkin, "LightSkyBlueFont")
            sl.setAlignment(Align.left)
            return sl
        }

        fun myCLetter(labelText: String): Label {
            val cl = Label(labelText,DimCon.UISkin, "SandyBrownFont")
            cl.setAlignment(Align.left)
            return cl
        }

        fun myGLabel(golds:String): Label {
            val glabel = Label(golds, DimCon.UISkin, "WhiteFontBlackBG")
            glabel.setAlignment(Align.right)
            return glabel
        }

        fun moneyTable(): Table {

            val moneyTable = Table()
            moneyTable.columnDefaults(0).width(80f).height(30f)
            moneyTable.columnDefaults(1).width(14f).height(30f)
            moneyTable.columnDefaults(2).width(32f).height(30f)
            moneyTable.columnDefaults(3).width(14f).height(30f)
            moneyTable.columnDefaults(4).width(32f).height(30f)
            moneyTable.columnDefaults(5).width(14f).height(30f)


            return moneyTable
        }


        fun itemPriceTable(money: Int): Table {
            val table = Table()
            table.columnDefaults(0).width(36f).height(30f)
            table.columnDefaults(1).width(14f).height(30f)
            table.columnDefaults(2).width(26f).height(30f)
            table.columnDefaults(3).width(14f).height(30f)
            table.columnDefaults(4).width(24f).height(30f)
            table.columnDefaults(5).width(14f).height(30f)

            val aGSC = MoneyConversion.toGSC(money)

            val golds = TableModel.myGLabel("${aGSC[0]}")
            val silvers = TableModel.myGLabel("${aGSC[1]}")
            val coppers = TableModel.myGLabel("${aGSC[2]}")

            val gL = TableModel.myGLetter("G")
            val sL = TableModel.mySLetter("S")
            val cL = TableModel.myCLetter("C")

            table.add(golds)
            table.add(gL)
            table.add(silvers)
            table.add(sL)
            table.add(coppers)
            table.add(cL)

            table.x = DimCon.vpWidth*0.5f
            table.y = DimCon.vpHeight * 0.5f

            return table
        }



    }

}
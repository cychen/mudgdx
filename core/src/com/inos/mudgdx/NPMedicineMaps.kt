package com.inos.mudgdx

import mudMedicineSetting.Medicinal

class NPMedicineMaps {

    companion object {


        fun setUp(){

            setUpNPMedicinalMap()
        }


        private fun setUpNPMedicinalMap(){
             mudMedicineSetting.Medicinal.medicinalMap()
        }

        fun NPMedicinalMap(): MutableMap<String, Medicinal>{
            return mudMedicineSetting.Medicinal.medicinalMap
        }
    }
}
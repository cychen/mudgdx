package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop


class SetUpDNDBagVsWok {


    companion object {


        fun dNDCookingCondiment(uiStage: Stage): MutableList<Window> {
            val dnd = DragAndDrop()
            val bagTable = TableModel.dndTable()
            val wokTable = TableModel.dndTable()
            val bagAndWokWindowList = createBagAndWokWindowList("D","Bag Condiments", "Wok Condiments", bagTable, wokTable, uiStage)
            val bagWindow = bagAndWokWindowList[0]
            val wokWindow = bagAndWokWindowList[1]

            DNDMapCondimentItem.dndBag(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)
            return bagAndWokWindowList
        }


        fun dNDCookingIngredient(uiStage: Stage): MutableList<Window> {
            val dnd = DragAndDrop()
            val bagTable = TableModel.dndTable()
            val wokTable = TableModel.dndTable()

            val bagAndWokWindowList = createBagAndWokWindowList("U","Bag Ingredients", "Wok Ingredients", bagTable, wokTable, uiStage)
            val bagWindow = bagAndWokWindowList[0]
            val wokWindow = bagAndWokWindowList[1]

            DNDMapItem.dndBag(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)

            return bagAndWokWindowList
        }


        private fun createBagAndWokWindowList(UDposition:String, bagWinTitle:String, wokWinTitle:String, bagTable:Table, wokTable:Table, uiStage: Stage): MutableList<Window>{

            val bagWindow = TableModel.myItemQtyWindow(bagWinTitle)
            val wokWindow = TableModel.myItemQtyWindow(wokWinTitle)

            val bagAndWokWindowList = mutableListOf<Window>()
            bagAndWokWindowList.add(bagWindow)
            bagAndWokWindowList.add(wokWindow)

            bagWindow.x = bagWindow.x * 0.5f

            if (UDposition == "U"){
                bagWindow.y = bagWindow.y * 0.75f
            } else {
                bagWindow.y = bagWindow.y - bagWindow.height * 1.5f
            }

            wokWindow.x = bagWindow.x + bagWindow.width * 1.02f
            wokWindow.y = bagWindow.y

            TableBtnAndWindowAssembly.addTableToWindow(bagWindow, bagTable, uiStage)
            TableBtnAndWindowAssembly.addTableToWindow(wokWindow, wokTable, uiStage)

            return bagAndWokWindowList
        }

    }
}
package com.inos.mudgdx

import com.badlogic.gdx.maps.MapLayer
import com.badlogic.gdx.maps.MapObject
import com.badlogic.gdx.maps.objects.TextureMapObject
import com.badlogic.gdx.maps.tiled.TiledMap
import com.badlogic.gdx.maps.tiled.TiledMapTile
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.ui.Image

class GeoObject {


    companion object {

        var geoObjectRectList = mutableListOf<Rectangle>()

        fun createGeoObjectRectList(geoStructureLayer: MapLayer){

            geoObjectRectList.clear()
            geoStructureLayer as TiledMapTileLayer
            val c = geoStructureLayer.width
            val r = geoStructureLayer.height
            val tileW = geoStructureLayer.tileWidth

            for (j in 0 until r ){
                for (i in 0 until c){
                    val targetCell = geoStructureLayer.getCell(i, j)
                    if (targetCell != null && targetCell.tile.properties.containsKey("blocked")) {

                        val tileRectangle = Rectangle(i * tileW, j * tileW, tileW, tileW)
                        geoObjectRectList.add(tileRectangle)
                    }
                }
            }

        }


        fun geoObjectRectList(): MutableList<Rectangle> {
            return geoObjectRectList

        }




    }
}
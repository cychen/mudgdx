package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.ui.Window
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener

class BuyStoreItem {

    companion object {


        private val nPCookingCondimentMap = NPCookingMaps.NPCookingCondimentMap()
        private val nPInnCookingCondimentMap = NPCookingMaps.NPInnCookingCondimentMap()
        private val nPCookingIngredientMap = NPCookingMaps.NPCookingIngredientMap()
        private val nPCookingRecipeMap = NPCookingMaps.NPCookingRecipeMap()
        private val nPMedicinalMap = NPMedicineMaps.NPMedicinalMap()
        private val ppCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap
        private val ppCookingIngredientMap = PlayerProfile.playerProfile.cookingIngredientMap
        private val ppCookingRecipeMap = PlayerProfile.playerProfile.cookingRecipeMap
        private val ppMedicinalMap = PlayerProfile.playerProfile.medicinalMap


        fun buy( npcObj: ObjImage, uiStage: Stage) {

            npcObj.available = false

            val buyWindow = TableModel.myItemPriceWindow("Click on item to buy")
            val itemTable = TableModel.myGrocerItemTable()
            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(buyWindow, itemTable, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    closeBtn.clearListeners()
                    uiStage.addAction(Actions.removeActor(buyWindow))
                    npcObj.available = true
                }
            })

            setUpItemAndPriceToTable(itemTable, buyWindow, npcObj, uiStage)
        }


        fun setUpItemAndPriceToTable(itemTable: Table, buyWindow: Window, npcObj: ObjImage, uiStage: Stage) {
            return when (npcObj.name){
                "grocer" -> setUpGroceryItemAndPriceToTable(itemTable, buyWindow, npcObj, uiStage)
                "innkeeper" -> setUpInnItemAndPriceToTable(itemTable, buyWindow, npcObj, uiStage)
                "farmer" -> setUpFarmItemAndPriceToTable(itemTable, buyWindow, npcObj, uiStage)
                else -> throw NullPointerException("${npcObj.name} not ready for setUpItemTable")
            }
        }


        private fun setUpGroceryItemAndPriceToTable(itemTable: Table, buyWindow: Window, npcObj: ObjImage, uiStage: Stage ) {

            npcObj.available = false

            for (cookingCondiment in nPCookingCondimentMap){
                if (!nPInnCookingCondimentMap.containsKey(cookingCondiment.key)){
                    val itemUUID = cookingCondiment.key
                    val itemName = cookingCondiment.value.name
                    val itemPrice = cookingCondiment.value.price
                    addItemAndPriceToTable(itemUUID, itemName, itemPrice, ppCookingCondimentMap, itemTable, buyWindow, npcObj, uiStage)
                }
            }

        }


        private fun setUpInnItemAndPriceToTable(itemTable: Table, buyWindow: Window, npcObj: ObjImage, uiStage: Stage) {

            npcObj.available = false

            for (entry in nPCookingRecipeMap) {
                if (entry.value.category == "System") {
                    val itemUUID = entry.key
                    val itemName = entry.value.name
                    val itemPrice = entry.value.price
                    addItemAndPriceToTable(itemUUID, itemName, itemPrice, ppCookingRecipeMap, itemTable, buyWindow, npcObj, uiStage)
                }
            }

            for (entry in nPInnCookingCondimentMap){
                val itemUUID = entry.key
                val itemName = entry.value.name
                val itemPrice = entry.value.price
                addItemAndPriceToTable(itemUUID, itemName, itemPrice, ppCookingCondimentMap, itemTable, buyWindow, npcObj, uiStage)
            }
        }


        private fun setUpFarmItemAndPriceToTable(itemTable: Table, buyWindow: Window, npcObj: ObjImage, uiStage: Stage) {

            npcObj.available = false

            for (entry in nPCookingIngredientMap) {
                val itemUUID = entry.key
                val itemName = entry.value.name
                val itemPrice = entry.value.price
                addItemAndPriceToTable(itemUUID, itemName, itemPrice, ppCookingIngredientMap, itemTable, buyWindow, npcObj, uiStage)
            }

        }


        private fun addItemAndPriceToTable(itemUUID: String, itemName:String, itemPrice: Int, ppMap: MutableMap<String, Int>, itemTable: Table, tradeWindow: Window, npcObj: ObjImage, uiStage: Stage): Table{

          val itemButton = TableModel.myTextButton(itemName)

            TableBtnAndWindowAssembly.addItemAndPriceToTable(itemButton, itemPrice, itemTable)

            itemButton.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiStage.addAction(Actions.removeActor(tradeWindow))
                    displayItemBuyInfoTable(itemUUID, itemName, itemPrice, ppMap, npcObj, uiStage)
                }
            })

            return itemTable
        }


        private fun displayItemBuyInfoTable(itemUUID:String, itemName:String, itemPrice: Int, ppMap: MutableMap<String, Int>, npcObj: ObjImage, uiStage: Stage){
            npcObj.available = false

            val buyInfoWindow = TableModel.myTradeInfoWindow("Want to buy this?")
            val defaultQty = 1
            val buyQtyTF = TextField(defaultQty.toString(), DimCon.UISkin)
            val yesBtn = TableModel.myfuncButton("Yes")
            val noBtn = TableModel.myfuncButton("No")

            TableBtnAndWindowAssembly.addTradeInfoTableToWindow(buyInfoWindow, itemName, itemPrice, buyQtyTF, yesBtn, noBtn, uiStage)
            TableBtnAndWindowAssembly.setBuyQtyTFListener(buyQtyTF, defaultQty)

            yesBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {

                    val qtyBought = buyQtyTF.text.toInt()

                    if (PlayerProfile.playerProfile.money < qtyBought * itemPrice) {
                        buyInfoWindow.addAction(Actions.removeActor(buyInfoWindow))
                        ShowInfo.infoLabel("Not enough money", uiStage)
                        uiStage.addAction(Actions.sequence(
                                Actions.delay(0.5f),
                                Actions.run {buy(npcObj,uiStage)}
                        ))

                    } else {

                        PlayerProfile.playerProfile.money -= qtyBought * itemPrice
                        Bag.moneyUi(uiStage)

                        ppMap[itemUUID] = (ppMap[itemUUID]?: 0) + qtyBought
                        yesBtn.clearListeners()
                        uiStage.addAction(Actions.removeActor(buyInfoWindow))
                        npcObj.available = true
                        buy(npcObj, uiStage)
                    }
                }
            })

            noBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    noBtn.clearListeners()
                    uiStage.addAction(Actions.removeActor(buyInfoWindow))
                    npcObj.available = true
                    buy(npcObj, uiStage)
                }
            })
        }



    }


}
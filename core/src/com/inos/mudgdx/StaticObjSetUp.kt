package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.TimeUtils

class StaticObjSetUp {

    companion object {

        fun setUpStaticObj(player: Image, staticObjList:MutableList<ObjImage>, stage: Stage, uiStage: Stage){

            for ( objImage in staticObjList){
                objImage.addListener(object : ClickListener(){
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        objImage.clickFlag = true
                        objImage.recentClickTime = TimeUtils.millis()
                        objImageClickResponse(player, objImage, uiStage)
                    }
                })
                stage.addActor(objImage)
            }
        }


        fun objImageClickResponse(player: Image, objImage:ObjImage, uiStage: Stage) {

            if (objImage.actionType == "collect") {

                CollectObject.collectResponse(objImage, uiStage)

            } else {

                when (objImage.name) {

                    "well" -> WaterWell.useWell(player, objImage, uiStage)

                    "grocer" -> Grocery.grocer(objImage, uiStage)

                    "innkeeper" -> Inn.keeper(objImage, uiStage)

                    "farmer" -> Farm.farmer(objImage, uiStage)

                    else -> {

                        if (objImage.collideWithPlayerFlag) {

                            ShowInfo.closeContactInfo(objImage, uiStage)

                        } else ShowInfo.farAwayInfo(objImage, uiStage)

                    }
                }
            }
        }


    }
}
package com.inos.mudgdx

import com.badlogic.gdx.maps.Map
import com.badlogic.gdx.maps.objects.TextureMapObject


class ObjImageList  {

    companion object {

        var staticObjList = mutableListOf<ObjImage>()
        var movingObjList = mutableListOf<ObjImage>()

        fun createImageList(gardenMap:Map) {

            staticObjList.clear()
            movingObjList.clear()


            for (layer in gardenMap.layers){

                for (obj in layer.objects) {

                    obj as TextureMapObject

                    when (layer.name){

                        "CollectableObject" -> {

                            val objImage = ObjImage.cOImage(obj)
                            staticObjList.add(objImage)
                        }

                        "UtilityObject" -> {
                            val objImage = ObjImage.uOImage(obj)
                            staticObjList.add(objImage)
                        }

                        "NpcObject" -> {
                            val objImage = ObjImage.nPCImage(obj)
                            staticObjList.add(objImage)
                        }

                        "MobObject" -> {
                            val objImage = ObjImage.mOBImage(obj)
                            staticObjList.add(objImage)
                        }

                        "MovingObject" -> {
                            val objImage = ObjImage.movingOImage(obj)
                            movingObjList.add(objImage)
                        }
                    }
                }
            }
        }








    }
}


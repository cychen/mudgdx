package com.inos.mudgdx

import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.maps.objects.TextureMapObject
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.utils.TimeUtils
import java.util.*


class ObjImage : Image {

    constructor(tr: TextureRegion): super(tr)

    var enterFlag: Boolean = false
    var exitFlag : Boolean = false
    var clickFlag : Boolean = false

    var clickState: Boolean = false
    var collideWithPlayerFlag : Boolean = false
    var useFlag : Boolean = false
    var actionType: String = "collect"
    var collectable : Boolean = true
    var available : Boolean = true

    var quantity: Int = 3

    var lastCollectTime : Long = 0L
    var respawn : Long = 5L

    var friend : Boolean = true
    var life : Int = 5
    var lifeStatus : Boolean = true
    var recentClickTime: Long = TimeUtils.millis()
    var category: String = ""

    var UUID: String = "UUID"

    var IRectange: Rectangle = Rectangle(0f,0f,32f,32f)

    companion object {


        fun cOImage(obj: TextureMapObject): ObjImage {
            val cOImage = ObjImage(obj.textureRegion)
            cOImage.name = obj.name
            cOImage.x = obj.x
            cOImage.y = obj.y
            cOImage.setBounds(cOImage.x, cOImage.y, cOImage.width, cOImage.height)
            cOImage.UUID = UUID.randomUUID().toString()
          //  cOImage.UUID = obj.properties["UUID"].toString()
            cOImage.actionType = "collect"
            cOImage.collectable = true
            cOImage.available = true
            cOImage.category = "collection"
            cOImage.IRectange = Rectangle(cOImage.x, cOImage.y, cOImage.width, cOImage.height)
            return cOImage
        }


        fun uOImage(obj: TextureMapObject): ObjImage {
            val uOImage = ObjImage(obj.textureRegion)
            uOImage.name = obj.name
            uOImage.x = obj.x
            uOImage.y = obj.y
            uOImage.setBounds(uOImage.x, uOImage.y, uOImage.width, uOImage.height)
            uOImage.UUID = UUID.randomUUID().toString()
           // uOImage.UUID = obj.properties["UUID"].toString()
            uOImage.actionType = "use"
            uOImage.collectable = false
            uOImage.available = true
            uOImage.category = "utility"
            uOImage.IRectange = Rectangle(uOImage.x, uOImage.y, uOImage.width, uOImage.height)
            return uOImage
        }


        fun nPCImage(obj: TextureMapObject): ObjImage {
            val nPCImage = ObjImage(obj.textureRegion)
            nPCImage.name = obj.name
            nPCImage.x = obj.x
            nPCImage.y = obj.y
            nPCImage.setBounds(nPCImage.x, nPCImage.y, nPCImage.width, nPCImage.height)
            nPCImage.UUID = UUID.randomUUID().toString()
          //  nPCImage.UUID = obj.properties["UUID"].toString()
            nPCImage.actionType = "talk"
            nPCImage.available = true
            nPCImage.category = "npc"
            nPCImage.friend = true
            nPCImage.IRectange = Rectangle(nPCImage.x, nPCImage.y, nPCImage.width, nPCImage.height)

            return nPCImage
        }


        fun mOBImage(obj: TextureMapObject): ObjImage {

            val mOBImage = ObjImage(obj.textureRegion)
            mOBImage.name = obj.name
            mOBImage.x = obj.x
            mOBImage.y = obj.y
            mOBImage.setBounds(mOBImage.x, mOBImage.y, mOBImage.width, mOBImage.height)
            mOBImage.UUID = UUID.randomUUID().toString()
          //  mOBImage.UUID = obj.properties["UUID"].toString()
            mOBImage.actionType = "attack"
            mOBImage.friend = false
            mOBImage.life = 3
            mOBImage.lifeStatus = true
            mOBImage.IRectange = Rectangle(mOBImage.x, mOBImage.y, mOBImage.width, mOBImage.height)
            return mOBImage
        }

        fun movingOImage(obj: TextureMapObject): ObjImage {

            val movingOImage = ObjImage(obj.textureRegion)
            movingOImage.name = obj.name
            movingOImage.x = obj.x
            movingOImage.y = obj.y
            movingOImage.setBounds(movingOImage.x, movingOImage.y, movingOImage.width, movingOImage.height)
            movingOImage.UUID = UUID.randomUUID().toString()
          //  movingOImage.UUID = obj.properties["UUID"].toString()
            movingOImage.actionType = "talk"
            movingOImage.friend = true
            movingOImage.life = 3
            movingOImage.lifeStatus = true
            movingOImage.IRectange = Rectangle(movingOImage.x, movingOImage.y, movingOImage.width, movingOImage.height)
            return movingOImage
        }

    }
}


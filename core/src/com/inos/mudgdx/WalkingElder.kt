package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image


class WalkingElder {

    companion object {

        var walkSpeed = DimCon.walkSpeed

        fun clickElder(objImage: ObjImage, uiStage:Stage) {

            if (objImage.collideWithPlayerFlag) {

                ShowInfo.closeContactInfo(objImage, uiStage)

            } else ShowInfo.farAwayInfo(objImage,uiStage)

            objImage.clickFlag = false
        }


        fun horizontalWalk(player: Image, objImage: ObjImage, stage: Stage, uiStage: Stage) {

            val xmin = (Gdx.graphics.width * 0.5 + objImage.width).toInt()
            val xmax = (Gdx.graphics.width - objImage.width*3).toInt()

            val playerR = Rectangle(player.x, player.y, player.imageWidth, player.imageHeight)

            val objImageRect = objImageCollisionRectange(objImage)

// two moving objects, collision status and response need to be checked by both individual sides
// playerMove.collision checks from player side and do what player needs to response for it
// here objImageRect.overlaps(playerR) checks from moving object side and do what moving object wants to response

            objImage.collideWithPlayerFlag = (objImageRect.overlaps(playerR))

            if (objImage.collideWithPlayerFlag){ return }

            if ((objImage.x > xmax) || (objImage.x < xmin)) {

                walkSpeed = -walkSpeed

            }

            objImage.x += walkSpeed * Gdx.graphics.deltaTime

        }


        fun objImageCollisionRectange(objImage: ObjImage): Rectangle {

           val x = if ((objImage.x - 0.1f * objImage.imageWidth) >= 0f) {
                objImage.x - 0.1f * objImage.imageWidth
            } else {
                objImage.x
            }
            val y = if ((objImage.y - 0.1f * objImage.imageHeight) >= 0f) {
                 objImage.y - 0.1f * objImage.imageHeight
            } else {
                 objImage.y
            }
            val width = if ((objImage.x + 1.2f * objImage.imageWidth) <= DimCon.vpWidth) {
                  1.2f * objImage.imageWidth
            } else {
                  objImage.imageWidth
            }
            val height = if ((objImage.y + 1.2f * objImage.imageHeight) <= DimCon.vpHeight) {
                 1.2f * objImage.imageHeight
            } else {
                objImage.imageHeight
            }

            return Rectangle(x, y, width, height)
        }



    }
}
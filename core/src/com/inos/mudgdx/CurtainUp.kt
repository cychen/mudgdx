package com.inos.mudgdx

import com.badlogic.gdx.*

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer
import com.badlogic.gdx.maps.tiled.TmxMapLoader
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer

import com.badlogic.gdx.scenes.scene2d.*


import com.badlogic.gdx.utils.TimeUtils

import com.badlogic.gdx.utils.viewport.FitViewport


class CurtainUp(charName:String) : Screen,InputAdapter() {

    private var viewport = FitViewport(DimCon.vpWidth, DimCon.vpHeight)
    private var stage = Stage(viewport)
    private var uiStage = Stage(viewport)
    private val multiplexer = InputMultiplexer()

    private var player =  PlayerSetUp.loadPlayerProfile(charName)

    private val gameMapName = PlayerProfile.playerProfile.mapName
    private val gameMap = TmxMapLoader().load("ARWorld/$gameMapName.tmx")

    private val gameMapRenderer = OrthogonalTiledMapRenderer(gameMap)
    private val bgLayer = gameMap.layers["Background"] as TiledMapTileLayer

    private val mapXmax = (bgLayer.width - 1) * bgLayer.tileWidth
    private val mapYmax = (bgLayer.height - 1) * bgLayer.tileHeight

    private val geoObjectLayer = gameMap.layers["GeoStructure"]

    private var staticObjList : MutableList<ObjImage>
    private var movingObjList: MutableList<ObjImage>

    private val playerKeyMoveListener = PlayerMoveListener()


    init {

        NPUUIDMaps.setUp()
        NPCookingMaps.setUp()
        NPMedicineMaps.setUp()

        ObjImageList.createImageList(gameMap)

        staticObjList = ObjImageList.staticObjList
        movingObjList = ObjImageList.movingObjList

        GeoObject.createGeoObjectRectList(geoObjectLayer)

        StaticObjSetUp.setUpStaticObj(player, staticObjList, stage, uiStage)

        MovingObjSetUp.setUpMovingObj(movingObjList, stage, uiStage)

        Bag.moneyUi(uiStage)
        Bag.bagUi(uiStage)

        PlayerAttrUI.playerAttrUi(uiStage)
        Skill.skillUi(uiStage)

        player.addListener(playerKeyMoveListener)

        stage.addActor(player)

        GameExit.exit(uiStage, player)

    }


    override fun show() {

        stage.keyboardFocus = player

        Gdx.input.inputProcessor = multiplexer
        multiplexer.addProcessor(uiStage)
        multiplexer.addProcessor(stage)

    }


    override fun render(delta: Float) {

        Gdx.gl.glClearColor(0f, 0f, 0f, 0f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        updatePlayer()

        gameMapRenderer.setView(stage.camera as OrthographicCamera)
        gameMapRenderer.render(intArrayOf(0, 1))

        val now = TimeUtils.millis()

        for (objImage in staticObjList) {

            if (!objImage.isVisible) {
                if (objImage.respawn < (now - objImage.lastCollectTime) / 1000) {
                    objImage.isVisible = true
                    objImage.available = true
                }
            }
        }

        for (movingObj in movingObjList) {
            if (movingObj.name == "elder") {
                WalkingElder.horizontalWalk(player, movingObj, stage, uiStage)
            }
        }

        stage.act()
        stage.draw()
        uiStage.act()
        uiStage.draw()


    }



    private fun updatePlayer() {

        PlayerMove().move(playerKeyMoveListener, player, mapXmax, mapYmax)

        if (player.x in viewport.worldWidth/2 .. bgLayer.width*bgLayer.tileWidth - viewport.worldWidth/2)

        { stage.camera.position.x = player.x}

        if (player.y in viewport.worldHeight/2 .. bgLayer.height*bgLayer.tileWidth - viewport.worldHeight/2)

        { stage.camera.position.y = player.y }

        stage.camera.update()
    }

    override fun hide() {
        dispose()
    }

    override fun resize(width: Int, height: Int) {
     //   stage.viewport.update(width/px,height/px)
    }


    override fun dispose() {
        stage.dispose()
    }

    override fun pause() {}

    override fun resume() {}




}


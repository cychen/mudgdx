package com.inos.mudgdx


import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener


class BagItem {

    companion object {

        private val ppMedicinalMap = PlayerProfile.playerProfile.medicinalMap
        private val ppCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap
        private val ppCookingIngredientMap = PlayerProfile.playerProfile.cookingIngredientMap
        private val ppCookingRecipeMap = PlayerProfile.playerProfile.cookingRecipeMap
        private val ppRecipeFoodMap = PlayerProfile.playerProfile.recipeFoodMap
        private val ppNoNameFoodMap = PlayerProfile.playerProfile.noNameFoodMap
        private val ppFailedFoodMap = PlayerProfile.playerProfile.failedFoodMap

        private val nPCookingIngredientMap = NPCookingMaps.NPCookingIngredientMap()
        private val nPCookingCondimentMap = NPCookingMaps.NPCookingCondimentMap()
        private val nPCookingRecipeMap = NPCookingMaps.NPCookingRecipeMap()
        private val nPRecipeFoodMap = NPCookingMaps.NPRecipeFoodMap()
        private val nPNoNameFoodMap = NPCookingMaps.NPNoNameFoodMap()
        private val nPMedicinalMap = NPMedicineMaps.NPMedicinalMap()
        private val nPFailedFoodMap = NPCookingMaps.NPFailedFoodMap()

        private val bagItemMapList = mutableListOf(
                ppMedicinalMap,
                ppCookingCondimentMap,
                ppCookingIngredientMap,
                ppCookingRecipeMap,
                ppRecipeFoodMap,
                ppNoNameFoodMap,
                ppFailedFoodMap)

        fun listBagItem(bagUiBtn: TextButton, uiStage: Stage) {

            val bagItemWindow = TableModel.myItemQtyWindow("Items in the bag")
            val closeBtn = TableModel.myCloseTextButton("X")
            val itemTable = TableModel.itemTable()

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(bagItemWindow, itemTable, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    bagUiBtn.touchable = Touchable.enabled
                    bagItemWindow.addAction(Actions.removeActor(bagItemWindow))
                }
            })
            setUpPPMapItemAndQtyToTable(itemTable, bagItemWindow, bagUiBtn, uiStage)
        }


        private fun setUpPPMapItemAndQtyToTable(itemTable: Table, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage){

            if (TableBtnAndWindowAssembly.isBagEmpty(bagItemMapList)) {
                ShowInfo.infoLabel("Bag is empty!", uiStage)
                bagUiBtn.touchable = Touchable.enabled

            } else {

               loadPPMapItemAndQtyToTable(itemTable, bagItemWindow, bagUiBtn, uiStage)
            }
        }


        private fun loadPPMapItemAndQtyToTable(itemTable: Table, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage){

            for ((itemUUID, itemQty) in ppMedicinalMap){
                val thisMedicinal = nPMedicinalMap[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in nPMedicinalMap"
                )
                val itemName = thisMedicinal.name
                val itemButton = TableModel.myTextButton(itemName)
                TableBtnAndWindowAssembly.addItemAndQtyToTable(itemButton, itemQty, itemTable)

                itemButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        return
                    }
                })
            }


            for ((itemUUID, itemQty) in ppCookingCondimentMap){
                val thisCondiment = nPCookingCondimentMap[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in nPCookingCondimentMap"
                )
                val itemName = thisCondiment.name
                val itemButton = TableModel.myTextButton(itemName)
                TableBtnAndWindowAssembly.addItemAndQtyToTable(itemButton, itemQty, itemTable)

                itemButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        return
                    }
                })
            }


            for ((itemUUID, itemQty) in ppCookingIngredientMap){
                val thisIngredient = nPCookingIngredientMap[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in nPCookingIngredientMap"
                )
                val itemName = thisIngredient.name
                val itemButton = TableModel.myTextButton(itemName)
                TableBtnAndWindowAssembly.addItemAndQtyToTable(itemButton, itemQty, itemTable)

                itemButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        return
                    }
                })
            }


            for ((itemUUID, itemQty) in ppCookingRecipeMap){
                val thisRecipe = nPCookingRecipeMap[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in nPCookingRecipeMap")
                val itemName = thisRecipe.name
                val itemButton = TableModel.myTextButton(itemName)
                TableBtnAndWindowAssembly.addItemAndQtyToTable(itemButton, itemQty, itemTable)

                itemButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        itemButton.touchable = Touchable.disabled
                        DisplayRecipeInfo.displayInfo(thisRecipe, itemButton, bagItemWindow, bagUiBtn, uiStage)
                    }
                })
            }


            for ((itemUUID, itemQty) in ppRecipeFoodMap){
                val thisFood = nPRecipeFoodMap[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in nPRecipeFoodMap")
                val itemName = thisFood.name
                val itemButton = TableModel.myTextButton(itemName)
                TableBtnAndWindowAssembly.addItemAndQtyToTable(itemButton, itemQty, itemTable)

                itemButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        itemButton.touchable = Touchable.disabled
                        DisplayFoodInfo.displayRecipeFoodInfo(thisFood, itemButton, bagItemWindow, bagUiBtn, uiStage)
                    }
                })
            }


            for ((itemUUID, itemQty) in ppNoNameFoodMap){
                val thisFood = nPNoNameFoodMap[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in nPNoNameFoodMap")
                val itemName = thisFood.name
                val itemButton = TableModel.myTextButton(itemName)
                TableBtnAndWindowAssembly.addItemAndQtyToTable(itemButton, itemQty, itemTable)

                itemButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        itemButton.touchable = Touchable.disabled
                        DisplayFoodInfo.displayNoNameFoodInfo(thisFood, itemButton, bagItemWindow, bagUiBtn, uiStage)
                    }
                })
            }

            for ((itemUUID, itemQty) in ppFailedFoodMap){
                val thisFood = nPFailedFoodMap[itemUUID]?: throw NullPointerException(
                        "$itemUUID not found in nPFailedFoodMap")
                val itemName = thisFood.name
                val itemButton = TableModel.myTextButton(itemName)
                TableBtnAndWindowAssembly.addItemAndQtyToTable(itemButton, itemQty, itemTable)

                itemButton.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        itemButton.touchable = Touchable.disabled
                        DisplayFoodInfo.displayFailedFoodInfo(thisFood, itemButton, bagItemWindow, bagUiBtn, uiStage)
                    }
                })
            }

        }





    }


}
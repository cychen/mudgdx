package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.InputListener


class PlayerMoveListener : InputListener() {

    var moveR : Boolean = false
    var moveL = false
    var moveU = false
    var moveD = false


    override fun keyDown(event: InputEvent?, keycode: Int): Boolean {

        when (keycode) {

            Input.Keys.A -> {
                moveL = true
                return true
            }

            Input.Keys.D -> {
                moveR = true
                return true
            }

            Input.Keys.W -> {
                moveU = true
                return true
            }

            Input.Keys.X -> {
                moveD = true
                return true
            }
        }
        return false
    }


    override fun keyUp(event: InputEvent?, keycode: Int): Boolean {
        when (keycode) {

            Input.Keys.A -> {
                if (moveL){moveL = false}
                return true
            }

            Input.Keys.D -> {
                if (moveR){moveR = false}
                return true
            }

            Input.Keys.W -> {
                if (moveU){moveU = false}
                return true
            }

            Input.Keys.X -> {
                if (moveD){moveD = false}
                return true
            }
        }
        return false
    }

}

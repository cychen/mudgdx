package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import mudCookingSetting.food.Food
import mudCookingSetting.recipe.CookingRecipe
import java.util.*


/*

class SaveAsNewCookingRecipe {

    companion object {

        private val nPObjectUUIDToNameMap = NPUUIDMaps.NPObjectUUIDToNameMap()


        fun saveAsNewRecipe(newNoNameFood: Food, wokCookingIngredientMap: MutableMap<String, Int>, wokCookingCondimentMap: MutableMap<String, Int>, skillUiBtn: TextButton, uiStage: Stage) {

            val saveAsNewRecipeWindow = TableModel.myItemQtyWindow(" Name your new recipe ")
            val nameTFTable = Table()
            val defaultRecipeName = newNoNameFood.name
            var newRecipeName = defaultRecipeName
            val nameRecipeTF = TextField(defaultRecipeName, DimCon.UISkin)

            val okBtn = TableModel.myfuncButton("OK")

            val cancelBtn = TableModel.myfuncButton("Cancel")

            nameTFTable.add(nameRecipeTF)
            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(saveAsNewRecipeWindow, nameTFTable, okBtn, cancelBtn, uiStage)

            nameRecipeTF.setTextFieldFilter { _, c ->

                c.toString().matches(Regex("^[a-zA-Z0-9#]"))
            }

            nameRecipeTF.setTextFieldListener { textField, _ ->
                if (textField == null) {
                    ShowInfo.infoLabel("recipe name can not be null", uiStage)
                    nameRecipeTF.text = defaultRecipeName

                } else if (textField.text == "") {
                    ShowInfo.infoLabel("recipe Name can not be empty", uiStage)
                    nameRecipeTF.text = defaultRecipeName
                }
                newRecipeName = textField.text.toString()
            }

            okBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    if (nPObjectUUIDToNameMap.containsValue(newRecipeName)) {
                        ShowInfo.infoLabel("recipe name exists", uiStage)
                        saveAsNewRecipeWindow.addAction(Actions.removeActor(saveAsNewRecipeWindow))
                        saveAsNewRecipe(newNoNameFood, wokCookingIngredientMap, wokCookingCondimentMap, skillUiBtn, uiStage)
                    } else {
                        saveAsNewRecipeWindow.addAction(Actions.removeActor(saveAsNewRecipeWindow))
                        val newRecipe = CookingRecipe()
                        do {
                            newRecipe.UUID = UUID.randomUUID().toString()
                        } while (nPObjectUUIDToNameMap.contains(newRecipe.UUID))
                        newRecipe.name = newRecipeName
                        newRecipe.type = newNoNameFood.type
                        newRecipe.price = newNoNameFood.price
                        newRecipe.rarity = newNoNameFood.rarity
                        newRecipe.category = "Create"
                        newRecipe.inventor = PlayerProfile.playerProfile.playerName
                        newRecipe.description = newNoNameFood.description

                        for (ingredient in newNoNameFood.ingredientMap) {
                            newRecipe.ingredientMap[ingredient.key] = ingredient.value
                        }
                        for (condiment in newNoNameFood.condimentMap) {
                            newRecipe.condimentMap[condiment.key] = condiment.value
                        }

                        for (mainEffect in newNoNameFood.mainEffectMap) {
                            newRecipe.mainEffectMap[mainEffect.key] = mainEffect.value
                        }

                        for (specialEffect in newNoNameFood.specialEffectMap) {
                            newRecipe.specialEffectMap[specialEffect.key] = specialEffect.value
                        }

                        //*** update CreateCookingRecipeMap and RecipeFoodMap ***
                        NPCookingMaps.updateNPCreatedCookingRecipeMap(newRecipe)

                        val pplearntRecipeMap = PlayerProfile.playerProfile.learntCookingRecipe

                        if (pplearntRecipeMap.contains(newRecipe.UUID)) throw NullPointerException(

                                "pplearntRecipeMap contains newRecipeUUID"

                        ) else {
                            pplearntRecipeMap.add(newRecipe.UUID)
                        }

                        giveRecipeDescription(newRecipe, wokCookingIngredientMap, wokCookingCondimentMap, skillUiBtn, uiStage)
                    }
                }
            })
        }


        fun giveRecipeDescription(newRecipe: CookingRecipe, wokCookingIngredientMap: MutableMap<String, Int>, wokCookingCondimentMap: MutableMap<String, Int>, skillUiBtn: TextButton, uiStage: Stage) {

            val descriptionWindow = TableModel.myItemQtyWindow("food description")
            val defaultDescription = "How special the food is!"
            var newDescription = defaultDescription
            val descriptionTF = TextField(defaultDescription, DimCon.UISkin)

            val okBtn = TableModel.myfuncButton("OK")
            val cancelBtn = TableModel.myfuncButton("Cancel")

            val descriptionTable = Table()
            descriptionTable.add(descriptionTF)

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(descriptionWindow, descriptionTable, okBtn, cancelBtn, uiStage)

            descriptionTF.setTextFieldFilter { _, c ->
                c.toString().matches(Regex("^[a-zA-Z0-9#]"))
            }

            descriptionTF.setTextFieldListener { textField, _ ->
                if (textField == null) {
                    ShowInfo.infoLabel("description can not be null", uiStage)
                    descriptionTF.text = defaultDescription
                } else if (textField.text == "") {
                    ShowInfo.infoLabel("description can not be empty", uiStage)
                    descriptionTF.text = defaultDescription
                }
                newDescription = textField.text.toString()
            }

            okBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {

                    newRecipe.description = newDescription

                    descriptionWindow.addAction(Actions.removeActor(descriptionWindow))

                    val label = TableModel.myGLetter("New Cooking Recipe ${newRecipe.name} has been" +
                            "created  !!  ")
                    label.setPosition(DimCon.vpWidth * 0.5f - label.width * 0.5f, DimCon.vpHeight * 0.85f)

                    uiStage.addActor(label)

                    //  saveNewRecipeLabelAction("Simple", label, wokCookingIngredientMap, wokCookingCondimentMap, skillUiBtn, uiStage)


                }
            })
        }


        fun saveNewRecipeLabelAction(CookingType: String, label: Label, wokCIMap: MutableMap<String, Int>, wokCDMap: MutableMap<String, Int>, skillUiBtn: TextButton, uiStage: Stage) {
            label.addAction(Actions.sequence(
                    Actions.delay(1f),
                    Actions.fadeOut(1f),
                    Actions.run {
                        wokCIMap.clear()
                        wokCDMap.clear()
                        if (CookingType == "Simple") {
                            CookingWithCreate.startPreparingSimpleCooking(skillUiBtn, uiStage)
                        }
                    },
                    Actions.removeActor(label)
            ))
        }


    }
}

*/
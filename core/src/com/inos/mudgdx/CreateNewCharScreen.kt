package com.inos.mudgdx

import com.badlogic.gdx.Game
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.ui.TextField
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.Align

class CreateNewCharScreen : Screen {

    private val stage = Stage()
    private val uiSkin = Skin(Gdx.files.internal("UISkin/uiskin.json"))
    private var charName = "newbie"
    private val nameTF = charNameTextField()
    private val enterBtn = enterButton()
    private var popMessageLabel = messageD()

    override fun show() {

        popMessageLabel.isVisible = false

        nameTF.setTextFieldListener( object : TextField.TextFieldListener {
            override fun keyTyped(textField: TextField?, c: Char) {
                charName = textField!!.text
            }
        })

        enterBtn.addListener( object : ClickListener(){
            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                checkCharNameExistence(charName)
            }
        })

        stage.addActor(nameTF)
        stage.addActor(enterBtn)
        stage.addActor(popMessageLabel)
        Gdx.input.inputProcessor = stage

    }


    fun charNameTextField() : TextField {

        val TF = TextField("Enter Your Char Name", uiSkin)
        TF.setAlignment(Align.center)
        TF.setSize(150f, 50f)
        TF.setPosition(
                -TF.width/2f + Gdx.graphics.width/2f,
                -TF.height/2f + Gdx.graphics.height * 0.4f
        )
        return TF

    }


    fun enterButton() : TextButton {

        val btn = TextButton("ENTER", uiSkin)
        btn.align(Align.center)
        btn.setSize(150f,50f)
        btn.setPosition(
                -btn.width/2f + Gdx.graphics.width/2f,
                -btn.height/2f + Gdx.graphics.height * 0.25f
        )
        return btn

    }


    fun messageD(): Label {

        val label = Label("name already in use, try another one",uiSkin)
        label.setAlignment(Align.center)
        label.setFontScale(2f)
        label.setSize(250f,40f)
        label.setPosition(
                -label.width/2f + Gdx.graphics.width * 0.5f,
                -label.height/2f + Gdx.graphics.height * 0.65f
        )
        return label

    }


    fun checkCharNameExistence(charName:String){

        if (NPFileHandle.playerFileHandle("${charName.toLowerCase()}").exists()){

            popMessageLabel.addAction(
                Actions.sequence(
                    Actions.visible(true),
                    Actions.delay(3f),
                    Actions.visible(false)
                )
            )

        } else {

            PlayerProfile.newPlayer(charName)
            val applistener = Gdx.app.applicationListener as Game
            applistener.screen = CurtainUp(charName)

        }

    }



    override fun render(delta: Float) {
        Gdx.gl.glClearColor(0.2f,0.4f,0.6f,1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        stage.act()
        stage.draw()
    }

    override fun dispose() {
        stage.dispose()
        uiSkin.dispose()
    }

    override fun resize(width: Int, height: Int) {}

    override fun pause() {}

    override fun resume() {}

    override fun hide() { dispose() }

}
package com.inos.mudgdx

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.scenes.scene2d.ui.Image

class PlayerSetUp {

    companion object {

        fun loadPlayerProfile(charName:String): Image {

            PlayerProfile.loadPlayerProfile(charName)
            return charImage()
        }

        private fun charImage(): Image {

            val playerProfile = PlayerProfile.playerProfile
            val playerTextureFileName = playerProfile.charTextureFileName
            val playerTexture = Texture(NPFileHandle.playerTextureFileHandle(playerTextureFileName))
            val defaultImage = Image(Texture(NPFileHandle.playerDefaultTextureFileHandle()))

            val charImage = Image(playerTexture)
            charImage.x = playerProfile.positionXY[0]
            charImage.y = playerProfile.positionXY[1]

            return charImage
        }

    }

}
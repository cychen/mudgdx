package com.inos.mudgdx


import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener


class Inn {

    companion object {

        fun keeper(npcObj: ObjImage, uiStage: Stage ) {

            if (npcObj.available) {

                if (npcObj.clickFlag) {

                    if (npcObj.collideWithPlayerFlag) {

                        innKeeperAct(npcObj, uiStage)
                        npcObj.collideWithPlayerFlag = false

                    } else {

                        ShowInfo.farAwayInfo(npcObj, uiStage)

                    }
                }
            }
        }


        fun innKeeperAct(npcObj: ObjImage, uiStage: Stage) {

            val skillActivated = PlayerProfile.playerProfile.nPSkillMap["Cooking"]
                    ?: throw NullPointerException("Cooking is not found in nPSkill")

            if (skillActivated.activated) {

                TradeAct.startTradeAct(npcObj,uiStage )

            } else {

                innKeeperConversation(npcObj, uiStage)
            }

        }

        fun innKeeperConversation(npcObj: ObjImage, uiStage: Stage){

            npcObj.available = false

            val conversationWindow = TableModel.myConversationWindow(" Conversation ")

            val conversationLabel = TableModel.myTopLeftLabel("Nothing is more important than " +
                    "taking good care of your stomach with wonderful food in NP World! You can " +
                    "learn all kinds of cooking recipes and do the cooking by following the " +
                    "instructions. You can even make a name for yourself by creating your own Cooking " +
                    "recipes and make a good fortune by selling copies of it to others. " +
                    "" +
                    "Are you excited to enable your cooking skill now?")

            conversationLabel.setWrap(true)
            val conversationTable = TableModel.oneColumnTable(220f)
            conversationTable.add(conversationLabel)

            val closeBtn = TableModel.myCloseTextButton("X")

            val yesBtn = TableModel.myfuncButton("Yes")
            val noBtn = TableModel.myfuncButton("No")

            TableBtnAndWindowAssembly.addTableYesNoAndCloseBtnToWindow(conversationWindow, conversationTable, yesBtn, noBtn, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    closeBtn.clearListeners()
                    conversationWindow.addAction(Actions.removeActor(conversationWindow))
                    npcObj.available = true
                }
            })

            yesBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    conversationWindow.addAction(Actions.removeActor(conversationWindow))
                    PlayerProfile.playerProfile.nPSkillMap["Cooking"]?.activated = true
                    npcObj.available = true

                    val label = TableModel.myGLetter("   Cooking Skill Enabled  !!  " )
                    label.setPosition(DimCon.vpWidth*0.5f - label.width*0.5f, DimCon.vpHeight*0.85f)

                    uiStage.addActor(label)
                    label.addAction(Actions.sequence(
                            Actions.fadeIn(1f),
                            Actions.delay(1f),
                            Actions.fadeOut(1f),
                            Actions.removeActor(label)
                    ))
                }
            })

            noBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    conversationWindow.addAction(Actions.removeActor(conversationWindow))
                    npcObj.available = true
                }
            })
        }


    }
}
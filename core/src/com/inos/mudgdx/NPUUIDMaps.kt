package com.inos.mudgdx


class NPUUIDMaps {


    companion object {


        /*
        private var utilityObjectNameToUUIDListMap = mutableMapOf<String, MutableList<String>>()
        private var nPCObjectNameToUUIDListMap = mutableMapOf<String, MutableList<String>>()
        private var mobObjectNameToUUIDListMap = mutableMapOf<String, MutableList<String>>()
        private var movingObjectNameToUUIDListMap = mutableMapOf<String, MutableList<String>>()
*/

        fun setUp() {

            setUpNPObjectUUIDToNameMap()
            setUpNPNoNameObjectUUIDMap()
            setUpNPFailedObjectUUIDMap()

          //  setUpMedicinalNameToUUIDMap()
            /*
            setUpUtilityObjectNameToUUIDListMap()
            setUpNPCObjectNameToUUIDListMap()
            setUpMobObjectNameToUUIDListMap()
            setUpMovingObjectNameToUUIDListMap()
            */
        }



        private fun setUpNPObjectUUIDToNameMap() {
            mudObjectUUIDSetting.ObjectUUIDToName.UUIDToNameMap()
        }

        private fun setUpNPNoNameObjectUUIDMap() {
            mudObjectUUIDSetting.NoNameObjectUUID.noNameObjectUUIDMap()
        }

        private fun setUpNPFailedObjectUUIDMap() {
            mudObjectUUIDSetting.FailedObjectUUID.failedObjectUUIDMap()
        }

    //    private fun setUpMedicinalNameToUUIDMap() {
   //         medicinalNameToUUIDMap = MudObjectUUID.NPMedicinalNameToUUID.medicinalNameToUUIDMap()
   //     }

  /*
        private fun setUpUtilityObjectNameToUUIDListMap() {
            utilityObjectNameToUUIDListMap = MudObjectUUID.NPUtilityObjectNameToUUID.utilityObjectNameToUUIDListMap()
        }

        private fun setUpNPCObjectNameToUUIDListMap() {
            nPCObjectNameToUUIDListMap = MudObjectUUID.NPNPCObjectNameToUUID.nPCObjectNameToUUIDListMap()
        }

        private fun setUpMobObjectNameToUUIDListMap() {
            mobObjectNameToUUIDListMap = MudObjectUUID.NPMobObjectNameToUUID.mobObjectNameToUUIDListMap()
        }

        private fun setUpMovingObjectNameToUUIDListMap() {
            movingObjectNameToUUIDListMap = MudObjectUUID.NPMovingObjectNameToUUID.movingObjectNameToUUIDListMap()
        }

*/

        fun NPObjectUUIDToNameMap(): MutableMap<String, String> {
            return mudObjectUUIDSetting.ObjectUUIDToName.objectUUIDToNameMap
        }


        fun NPNoNameObjectUUIDMap(): MutableMap<String, String> {
            return mudObjectUUIDSetting.NoNameObjectUUID.noNameObjectUUIDMap
        }

        fun NPFailedObjectUUIDMap(): MutableMap<String, String> {
            return mudObjectUUIDSetting.FailedObjectUUID.failedObjectUUIDMap
        }



        /*
        fun NPUtilityObjectNameToUUIDListMap(): MutableMap<String, MutableList<String>> {
            setUpUtilityObjectNameToUUIDListMap()
            return collectionObjectNameToUUIDListMap
        }

        fun NPNPCObjectNameToUUIDListMap(): MutableMap<String, MutableList<String>> {
            setUpNPCObjectNameToUUIDListMap()
            return nPCObjectNameToUUIDListMap
        }

        fun NPMobObjectNameToUUIDListMap(): MutableMap<String, MutableList<String>> {
            setUpMobObjectNameToUUIDListMap()
            return mobObjectNameToUUIDListMap
        }

        fun NPMovingObjectNameToUUIDListMap(): MutableMap<String, MutableList<String>> {
            setUpMovingObjectNameToUUIDListMap()
            return movingObjectNameToUUIDListMap
        }

*/


    }
}
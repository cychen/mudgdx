package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image

class Collision {

    companion object {

        fun collisionDetection(dir: String, player: Image) {

            val geoObjectRectList = GeoObject.geoObjectRectList
            val staticObjList = ObjImageList.staticObjList
            val movingObjList = ObjImageList.movingObjList

            val playerRect = playerCollisionCheckRect(player)

            for (staticObj in staticObjList) {

                if (staticObj.isVisible) {

                    val iRect = staticObj.IRectange

                    if (playerRect.overlaps((iRect))) {

                        stepBack(player, staticObj, dir)

                        staticObj.collideWithPlayerFlag = true

                        return
                    }
                }
            }

            for (movingObj in movingObjList){

                val iRect = Rectangle(movingObj.x, movingObj.y, movingObj.imageWidth, movingObj.imageHeight)

                if (playerRect.overlaps(iRect)){

                    movingObj.collideWithPlayerFlag = true

                    stepBack(player, movingObj, dir)

                    return
                }
            }


            for (geoORect in geoObjectRectList) {

                if (playerRect.overlaps(geoORect)) {

                    geoStepBack(player, geoORect, dir)

                    return

                }
            }
        }


        private fun playerCollisionCheckRect(player:Image):Rectangle {
            return Rectangle(
                    player.x + 0.1f * player.width,
                    player.y + 0.1f * player.height,
                    0.8f * player.width,
                    0.8f * player.height)
        }


        private fun stepBack(player: Image, objImage: ObjImage, dir:String){

            val moveR = arrayOf(objImage.x + objImage.width, player.y)
            val moveL = arrayOf(objImage.x - player.imageWidth, player.y)
            val moveD = arrayOf(player.x, objImage.y - player.imageHeight)
            val moveU = arrayOf(player.x, objImage.y + objImage.height)

            val moveRList = arrayListOf(moveR, moveL, moveD, moveU)
            val moveLList = arrayListOf(moveL, moveR, moveD, moveU)
            val moveDList = arrayListOf(moveD, moveU, moveR, moveL)
            val moveUList = arrayListOf(moveU, moveD, moveR, moveL)



            when (dir) {

                "L" ->  {
                    checkNewPlayerPosOk(moveRList, player)
                }
                "R" ->  {
                    checkNewPlayerPosOk(moveLList, player)
                }
                "U" -> {
                    checkNewPlayerPosOk(moveDList, player)
                }
                "D" ->  {
                    checkNewPlayerPosOk(moveUList, player)
                }
            }
        }


        private fun checkNewPlayerPosOk(moveDirList:ArrayList<Array<Float>>, player: Image): Image {

            for ( newPlayerPos in moveDirList) {
                player.x = newPlayerPos[0]
                player.y = newPlayerPos[1]
                val newPlayerRect = playerCollisionCheckRect(player)
                if (checkObjImageOk(newPlayerRect) && (checkGeoObjectOk(newPlayerRect))) {
                    return player
                }
            }
            player.x = DimCon.mapCenterPos[0]
            player.y = DimCon.mapCenterPos[1]
            return player
        }


        private fun checkObjImageOk(newPlayerRect: Rectangle): Boolean {

            val staticObjList = ObjImageList.staticObjList

            for (objImage in staticObjList) {

                if (objImage.isVisible) {

                    val iRect = objImage.IRectange

                    if (newPlayerRect.overlaps((iRect))) {
                      return false
                    }
                }
            }
            return true
        }


        private fun checkGeoObjectOk(newPlayerRect:Rectangle): Boolean {

            val geoObjectRectList = GeoObject.geoObjectRectList

            for (geoORect in geoObjectRectList) {

                if (newPlayerRect.overlaps(geoORect)) {
                    return false
                }
            }

            return true
        }


        private fun geoStepBack(player: Image, geoORect:Rectangle, dir:String){

            when (dir) {

                "L" ->  player.x = geoORect.x + geoORect.width
                "R" ->  player.x = geoORect.x - player.imageWidth
                "U" ->  player.y = geoORect.y - player.imageHeight
                "D" ->  player.y = geoORect.y + geoORect.height
            }

        }


    }

}


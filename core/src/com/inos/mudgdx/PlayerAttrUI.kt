package com.inos.mudgdx

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.TimeUtils
import java.lang.reflect.GenericDeclaration

import kotlin.math.sign

class PlayerAttrUI {

    companion object {

        private val ppBaseLifeAttrMap = PlayerProfile.playerProfile.baseLifeAttributeMap
        private val ppBaseMainAttrMap = PlayerProfile.playerProfile.baseMainAttributeMap
        private val ppFoodMainAttrMap = PlayerProfile.playerProfile.foodMainAttributeMap
        private val ppBaseSpecialAttrMap = PlayerProfile.playerProfile.baseSpecialAttributeMap
        private val ppFoodSpecialAttrMap = PlayerProfile.playerProfile.foodSpecialAttributeMap


        fun playerAttrUi(uiStage: Stage){

            val charName = PlayerProfile.playerProfile.playerName

            val attrUiBtn = TableModel.myfuncButton("$charName")
            attrUiBtn.setSize(48f, 32f)

            val x = attrUiBtn.width * 0.1f
            val y = DimCon.vpHeight - attrUiBtn.height * 1.05f

            attrUiBtn.setPosition(x, y)

            attrUiBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    attrUiBtn.touchable = Touchable.disabled
                    playerAttrWindow(charName, attrUiBtn, uiStage)
                }
            })

            uiStage.addActor(attrUiBtn)

        }


        private fun playerAttrWindow(charName:String, attrUiBtn:TextButton,uiStage: Stage) {

            val attrWindow = TableModel.myConversationWindow(" $charName Details")
            attrWindow.width = 300f
            val closeBtn = TableModel.myCloseTextButton("X")

            closeBtn.addListener( object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    attrWindow.addAction(Actions.removeActor(attrWindow))
                    attrUiBtn.touchable = Touchable.enabled
                }
            })

            val attrTable = TableModel.oneColumnTable(300f)
            attrTable.debug()

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(attrWindow, attrTable,closeBtn, uiStage)

            val remainLifeUUID = FindUUID.findUUID("remainLife")
            val totalLifeUUID = FindUUID.findUUID("totalLife")
            val lifeRecoveryUUID = FindUUID.findUUID("lifeRecovery")
            val effectLastingUUID = FindUUID.findUUID("effectLasting")


            val remainLife = ppBaseLifeAttrMap[remainLifeUUID]
            val totalLife = ppBaseLifeAttrMap[totalLifeUUID]

            val lifeLabel = TableModel.myLabel("Life : ${remainLife} / ${totalLife}")

            attrTable.add(lifeLabel)
            attrTable.row()


            val now = TimeUtils.millis()
            val foodEffectStopTime = PlayerProfile.playerProfile.foodEffectStopTime

            var effectLastingValueInHHMMFormat : String


            val foodEffectRemainTime = foodEffectStopTime  - now

            if (foodEffectRemainTime > 0){

                val totalSec = foodEffectRemainTime / 1000L
                val ss = totalSec % 60
                val mm = (totalSec / 60) % 60
                val hh = (totalSec / 60) / 60

                effectLastingValueInHHMMFormat = "$hh(hr):$mm(min):$ss(sec)"


            } else {

                 for (entry in ppFoodMainAttrMap){
                     ppFoodMainAttrMap[entry.key] = 0
                 }
                 for (entry in ppFoodSpecialAttrMap){
                     ppFoodSpecialAttrMap[entry.key] = 0
                 }
                 effectLastingValueInHHMMFormat = "0(hr): 0(min): 0(sec)"

            }

            mainAttrExp("Str", attrTable)
            mainAttrExp("Agi", attrTable)
            mainAttrExp("Foc", attrTable)


            val baseLifeRecoveryValue = ppBaseSpecialAttrMap[lifeRecoveryUUID]?: throw NullPointerException("$lifeRecoveryUUID not found in specialAttrMap")
            val foodLifeRecoveryValue = ppFoodSpecialAttrMap[lifeRecoveryUUID]?: throw NullPointerException("" +
                    "$lifeRecoveryUUID not found in ppFoodSpecialAttrMap")
            val lifeRecoveryValue = "$baseLifeRecoveryValue + $foodLifeRecoveryValue"

            val effectLastingLabel = TableModel.myLabel("effectLasting : $effectLastingValueInHHMMFormat")
            val lifeRecoveryLabel = TableModel.myLabel("lifeRecovery : $lifeRecoveryValue")

            attrTable.add(effectLastingLabel)
            attrTable.row()
            attrTable.add(lifeRecoveryLabel)
            attrTable.row()

        }




        fun mainAttrExp(attrName:String, attrTable:Table): Table {

            val mainAttrUUID = FindUUID.findUUID(attrName)
           // val foodAttrUUID = FindUUID.findUUID(foodAttrName)

            val ppBaseMainAttrValue = ppBaseMainAttrMap[mainAttrUUID]?: throw NullPointerException("" +
                    "$mainAttrUUID not found in ppBaseMainAttrMap")

            val ppFoodMainAttrValue = ppFoodMainAttrMap[mainAttrUUID]?: throw NullPointerException("" +
                    "$mainAttrUUID not found in ppFoodMainAttrMap")

            val attrExp = "$ppBaseMainAttrValue + $ppFoodMainAttrValue"
           /*
            val attrExp = when (extraAttrValue.sign){
                1 -> { "$baseAttrValue + $extraAttrValue"}
                -1 -> {"$baseAttrValue - $extraAttrValue"}
                else -> {"$baseAttrValue" }
            }
*/
            val attrLabel = TableModel.myLabel("$attrName : $attrExp")
            attrTable.add(attrLabel)
            attrTable.row()
            return attrTable
        }



    }
}
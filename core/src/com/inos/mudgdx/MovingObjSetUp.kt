package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Image
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.TimeUtils

class MovingObjSetUp {

    companion object {

        fun setUpMovingObj(movingIList:MutableList<ObjImage>, stage:Stage, uiStage: Stage){

            for ( objImage in movingIList){
                objImage.addListener(object : ClickListener(){
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        objImage.clickFlag = true
                        objImage.recentClickTime = TimeUtils.millis()
                        objImageClickResponse(objImage, uiStage)
                    }
                })
                stage.addActor(objImage)
            }
        }


        fun objImageClickResponse(objImage:ObjImage, uiStage: Stage) {

            when (objImage.name) {

                "elder" -> {

                    WalkingElder. clickElder(objImage, uiStage)
                }

                else -> {

                    if (objImage.collideWithPlayerFlag) {

                        ShowInfo.closeContactInfo(objImage, uiStage)

                    } else {

                        ShowInfo.farAwayInfo(objImage, uiStage)
                    }

                }
            }
        }



    }
}
package com.inos.mudgdx


import mudCookingSetting.recipe.CookingRecipe
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener


class DisplayRecipeInfo {

    companion object {

        private val ppLearntCookingRecipeMap = PlayerProfile.playerProfile.learntCookingRecipe
        private val ppCookingRecipeMap = PlayerProfile.playerProfile.cookingRecipeMap


        fun displayInfo(thisRecipe: CookingRecipe, itemButton: TextButton, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage) {

            bagUiBtn.touchable = Touchable.disabled

            val recipeSectionWindow = TableModel.myItemSectionWindow(thisRecipe.name)
            recipeSectionWindow.x = bagItemWindow.x + bagItemWindow.width * 1.02f

            val descriptionBtn = TableModel.myTextButton("Description")
            val materialBtn = TableModel.myTextButton("Material")
            val foodEffectBtn = TableModel.myTextButton("Food Effect")
            val learntStatusBtn = displayLearntStatus(thisRecipe, recipeSectionWindow, bagUiBtn, bagItemWindow, uiStage)
            val actorsList = mutableListOf<Actor>(descriptionBtn, materialBtn, foodEffectBtn, learntStatusBtn)

            val itemTable = TableBtnAndWindowAssembly.sectionListTable(actorsList, 100f)

            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(recipeSectionWindow, itemTable, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    bagUiBtn.touchable = Touchable.enabled
                    itemButton.touchable = Touchable.enabled
                    recipeSectionWindow.addAction(Actions.removeActor(recipeSectionWindow))
                }
            })

            descriptionBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    descriptionBtn.touchable = Touchable.disabled
                    displayRecipeDescription(thisRecipe, descriptionBtn, uiStage)
                }
            })

            materialBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    materialBtn.touchable = Touchable.disabled
                    displayMaterialContent(thisRecipe, materialBtn, uiStage)
                }
            })

            foodEffectBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    foodEffectBtn.touchable = Touchable.disabled
                    displayFoodEffectContent(thisRecipe, foodEffectBtn, uiStage)
                }
            })

        }


        private fun displayLearntStatus(thisRecipe: CookingRecipe, recipeSectionWindow: Window, bagBtn: TextButton, bagItemWindow: Window, uiStage: Stage): TextButton {

            if (ppLearntCookingRecipeMap.contains(thisRecipe.UUID)) {

                val learntBtn = TableModel.myfuncButton("Learnt")
                learntBtn.touchable = Touchable.disabled
                uiStage.addActor(learntBtn)
                return learntBtn

            } else {

                val learntBtn = TableModel.myNewButton("New")
                uiStage.addActor(learntBtn)

                learntBtn.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {

                        learntBtn.touchable = Touchable.disabled

                        val learnRecipeWindow = TableModel.myWindow(" Learn it ? ")
                        val noBtn = TableModel.myfuncButton("No")
                        val yesBtn = TableModel.myfuncButton("Yes")

                        TableBtnAndWindowAssembly.addYesNoBtnToWindow(learnRecipeWindow, yesBtn, noBtn, uiStage)

                        noBtn.addListener(object : ClickListener() {
                            override fun clicked(event: InputEvent?, x: Float, y: Float) {
                                learntBtn.touchable = Touchable.enabled
                                learnRecipeWindow.addAction(Actions.removeActor(learnRecipeWindow))
                            }
                        })

                        yesBtn.addListener(object : ClickListener() {
                            override fun clicked(event: InputEvent?, x: Float, y: Float) {

                                learnRecipeWindow.addAction(Actions.removeActor(learnRecipeWindow))
                                ppLearntCookingRecipeMap.add(thisRecipe.UUID)

                                ppCookingRecipeMap[thisRecipe.UUID] = (ppCookingRecipeMap[thisRecipe.UUID] ?: throw
                                NullPointerException("${thisRecipe.UUID} not found in ppCookingRecipeMap")) - 1

                                recipeSectionWindow.addAction(Actions.removeActor(recipeSectionWindow))
                                bagItemWindow.addAction(Actions.removeActor(bagItemWindow))
                                bagBtn.touchable = Touchable.enabled

                                val recipeInfoWindowStringArray = mutableListOf("recipeDescriptionWindow", "recipeMaterialWindow", "foodEffectWindow")

                                for (wString in recipeInfoWindowStringArray) {
                                    val w = uiStage.root.findActor<Window>(wString)
                                    if (w != null) {
                                        if (w.isVisible) {
                                            uiStage.addAction(Actions.removeActor(uiStage.root.findActor<Window>(wString)))
                                        }
                                    }
                                }

                                val label = TableModel.myGLetter("   Learnt   ${thisRecipe.name}   !!  ")
                                label.setPosition(DimCon.vpWidth * 0.5f - label.width * 0.5f, DimCon.vpHeight * 0.85f)

                                uiStage.addActor(label)
                                label.addAction(Actions.sequence(
                                        Actions.fadeIn(1f),
                                        Actions.delay(1f),
                                        Actions.fadeOut(1f),
                                        Actions.removeActor(label)
                                ))
                            }
                        })
                    }
                })
                return learntBtn
            }
        }


        private fun displayRecipeDescription(thisRecipe: CookingRecipe, descriptionBtn: TextButton, uiStage: Stage) {

            val nameLabel = TableModel.myTopLeftLabel(" RecipeName")
            val nameValue = TableModel.myTopLeftLabel("        ${thisRecipe.name}")

            val categoryLabel = TableModel.myTopLeftLabel(" Category")
            val categoryValue = TableModel.myTopLeftLabel("        ${thisRecipe.category}")

            val rarityLabel = TableModel.myTopLeftLabel(" Rarity")
            val rarityValue = TableModel.myTopLeftLabel("        ${thisRecipe.rarity}")

            val typeLabel = TableModel.myTopLeftLabel(" Type")
            val typeValue = TableModel.myTopLeftLabel("        ${thisRecipe.type}")

            val inventorLabel = TableModel.myTopLeftLabel(" Inventor")
            val inventorValue = TableModel.myTopLeftLabel("        ${thisRecipe.inventor}")

            val descriptionLabel = TableModel.myTopLeftLabel(" Description")
            val descriptionValue = TableModel.myTopLeftLabel("        ${thisRecipe.description}")
            descriptionValue.setWrap(true)
            descriptionValue.width = 230f
            descriptionValue.pack()

            val actorsList = mutableListOf<Actor>(nameLabel, nameValue, categoryLabel, categoryValue,
                    rarityLabel, rarityValue, typeLabel, typeValue, inventorLabel, inventorValue, descriptionLabel, descriptionValue)

            val itemTable = TableBtnAndWindowAssembly.sectionListTable(actorsList, 230f)

            val descriptionWindow = TableModel.myItemQtyWindow("Description")

            descriptionWindow.name = "recipeDescriptionWindow"
            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(descriptionWindow, itemTable, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    descriptionBtn.touchable = Touchable.enabled
                    descriptionWindow.addAction(Actions.removeActor(descriptionWindow))
                }
            })

        }


        private fun displayMaterialContent(thisRecipe: CookingRecipe, materialBtn: TextButton, uiStage: Stage) {

            val itemTable = TableModel.twoColumnTable(150f, 50f)

            val ingredientMap = thisRecipe.ingredientMap
            Gdx.app.log("ingredientMap","$ingredientMap")
            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Ingredient  ", ingredientMap, itemTable)

            val condimentMap = thisRecipe.condimentMap
            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Condiment  ", condimentMap, itemTable)

            val recipeMaterialWindow = TableModel.myItemQtyWindow(" Recipe Material ")
            recipeMaterialWindow.name = "recipeMaterialWindow"

            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(recipeMaterialWindow, itemTable, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    materialBtn.touchable = Touchable.enabled
                    recipeMaterialWindow.addAction(Actions.removeActor(recipeMaterialWindow))
                }
            })
        }


        private fun displayFoodEffectContent (thisRecipe: CookingRecipe, foodEffectBtn: TextButton, uiStage: Stage) {

            val itemTable = TableModel.twoColumnTable(150f, 50f)

            val mainEffectMap = thisRecipe.mainEffectMap
            TableBtnAndWindowAssembly.addMapItemToTable("Main Effect  ", mainEffectMap, itemTable)

            val specialEffectMap = thisRecipe.specialEffectMap
            TableBtnAndWindowAssembly.addMapItemToTable("Special Effect  ", specialEffectMap, itemTable)

            val foodEffectWindow = TableModel.myItemQtyWindow(" Food Effect ")
            foodEffectWindow.name = "foodEffectWindow"

            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(foodEffectWindow, itemTable, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    foodEffectBtn.touchable = Touchable.enabled
                    foodEffectWindow.addAction(Actions.removeActor(foodEffectWindow))
                }
            })

        }




    }
}


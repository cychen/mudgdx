package com.inos.mudgdx

import mudCookingSetting.food.Food
import mudCookingSetting.recipe.CookingRecipe
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import java.util.*


class CookingWithCreate {

    companion object {

        private val ppCookingIngredientMap = PlayerProfile.playerProfile.cookingIngredientMap
        private val ppCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap
        private val ppCookingTypeList = NPCookingMaps.NPCookingTypeList()
        private val ppCookingRecipeMap = PlayerProfile.playerProfile.cookingRecipeMap
        private val ppNoNameFoodMap = PlayerProfile.playerProfile.noNameFoodMap
        private val ppRecipeFoodMap = PlayerProfile.playerProfile.recipeFoodMap
        private val ppFailedFoodMap = PlayerProfile.playerProfile.failedFoodMap
        private val ppMainAttrList = playerMaps.PlayerAttributeMap.baseMainAttributeNameList()
        private val ppSpecialAttrList = playerMaps.PlayerAttributeMap.baseSpecialAttributeNameList()

        private val nPCookingIngredientMap = NPCookingMaps.NPCookingIngredientMap()
        private val nPCookingCondimentMap = NPCookingMaps.NPCookingCondimentMap()
        private val wokCookingIngredientMap = DNDMapItem.wokCookingIngredientMap()
        private val wokCookingCondimentMap = DNDMapCondimentItem.wokCookingCondimentMap()
        private val nPCookingRecipeMap = NPCookingMaps.NPCookingRecipeMap()
        private val nPObjectUUIDToNameMap = NPUUIDMaps.NPObjectUUIDToNameMap()
        private val nPNoNamObjectUUIDMap = NPUUIDMaps.NPNoNameObjectUUIDMap()
        private val nPFailedFoodMap = NPCookingMaps.NPFailedFoodMap()
        private val nPFailedObjectUUIDMap = NPUUIDMaps.NPFailedObjectUUIDMap()


        private val bagItemMapList = mutableListOf(
                ppCookingCondimentMap,
                ppCookingIngredientMap)

        fun displayCookingType(skillUiBtn:TextButton, uiStage: Stage) {

           // skillBtn.touchable = Touchable.disabled

            val cookingTypeListWindow = TableModel.myItemQtyWindow("Cooking Types")

            val closeBtn = TableModel.myCloseTextButton("X")

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    cookingTypeListWindow.addAction(Actions.removeActor(cookingTypeListWindow))
                    skillUiBtn.touchable = Touchable.enabled
                }
            })

            val cookingTypeActorList = mutableListOf<Actor>()

            for (cookingType in ppCookingTypeList) {

                val cookingTypeBtn = TableModel.myfuncButton(cookingType)

                cookingTypeBtn.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        cookingTypeListWindow.addAction(Actions.removeActor(cookingTypeListWindow))
                        selectCookingType(cookingType, skillUiBtn, uiStage)
                    }
                })
                cookingTypeActorList.add(cookingTypeBtn)
            }

            val itemTable = TableBtnAndWindowAssembly.sectionListTable(cookingTypeActorList, 100f)
            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(cookingTypeListWindow, itemTable, closeBtn, uiStage)

        }


        private fun selectCookingType(cookingType:String, skillUiBtn: TextButton, uiStage: Stage){

            when (cookingType){
                "Simple" -> startPreparingSimpleCooking(skillUiBtn, uiStage)
                "Advance" -> return
                "House" -> return
                else -> return
            }
        }


        fun startPreparingSimpleCooking(skillUiBtn: TextButton, uiStage: Stage){

           val bagAndWokIngredientWindowList = setUpDNDCookingIngredient(uiStage)
           val bagAndWokCondimentWindowList = setUpDNDCookingCondiment(uiStage)
            requireQtyInfoWindow(bagAndWokIngredientWindowList, bagAndWokCondimentWindowList, skillUiBtn, uiStage)
        }


        private fun setUpDNDCookingIngredient(uiStage: Stage): MutableList<Window> {
            return SetUpDNDBagVsWok.dNDCookingIngredient(uiStage)
        }


        private fun setUpDNDCookingCondiment(uiStage: Stage): MutableList<Window> {
            return SetUpDNDBagVsWok.dNDCookingCondiment(uiStage)
        }


        private fun requireQtyInfoWindow(bagAndWokIngredientWindowList:MutableList<Window>, bagAndWokCondimentWindowList:MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage):Window{
            val requireQtyInfoWindow = TableModel.myCreateCookingWindow("Create Simple Cooking")
            val requireIngredientQtyLabel = TableModel.myLabel("Require Qty: ")
            val requireCondimentQtyLabel = TableModel.myLabel("2 Ingredients, 2 Condiments")
            requireIngredientQtyLabel.setWrap(true)
            requireCondimentQtyLabel.setWrap(true)
            val requireQtyInfoTable = TableModel.oneColumnTable(250f)
            requireQtyInfoTable.add(requireIngredientQtyLabel)
            requireQtyInfoTable.row()
            requireQtyInfoTable.add(requireCondimentQtyLabel)

            val cookBtn = TableModel.myfuncButton("Cook")
            val cancelBtn = TableModel.myfuncButton("Cancel")

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(requireQtyInfoWindow, requireQtyInfoTable, cookBtn, cancelBtn, uiStage)
            requireQtyInfoWindow.x = requireQtyInfoWindow.x * 1.2f
            requireQtyInfoWindow.y = requireQtyInfoWindow.y * 1.3f

            val cookMaterialAndQtyWindowList = mutableListOf<Window>()
            for (w in bagAndWokIngredientWindowList){
                cookMaterialAndQtyWindowList.add(w)
            }
            for (w in bagAndWokCondimentWindowList){
                cookMaterialAndQtyWindowList.add(w)
            }
            cookMaterialAndQtyWindowList.add(requireQtyInfoWindow)

            cookBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    checkEnoughMaterialForSimpleCook(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
                }
            })

            cancelBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    for (w in cookMaterialAndQtyWindowList){
                        w.addAction(Actions.removeActor(w))
                    }

                    for (ingredient in wokCookingIngredientMap){
                        ppCookingIngredientMap[ingredient.key] =
                                (ppCookingIngredientMap[ingredient.key]?:0) + ingredient.value
                    }

                    for (condiment in wokCookingCondimentMap){
                        ppCookingCondimentMap[condiment.key] =
                                (ppCookingCondimentMap[condiment.key]?:0) + condiment.value
                    }

                    wokCookingCondimentMap.clear()
                    wokCookingIngredientMap.clear()

                    skillUiBtn.touchable = Touchable.enabled
                    //put wok qty back to bag item qty
                }
            })
            return requireQtyInfoWindow
        }


        private fun checkEnoughMaterialForSimpleCook(cookMaterialAndQtyWindowList:MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            if (!checkEnoughCookingComponent("Simple", wokCookingIngredientMap)) {

                displayNotEnoughMaterialMessage(cookMaterialAndQtyWindowList, "Cooking Ingredient", uiStage)

            } else if (!checkEnoughCookingComponent("Simple", wokCookingCondimentMap)){

                displayNotEnoughMaterialMessage(cookMaterialAndQtyWindowList, "Cooking Condiment", uiStage)

            } else {

                startCreatingSimpleCook(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
            }
        }

        private fun displayNotEnoughMaterialMessage(materialAndQtyWindowList:MutableList<Window>, material:String, uiStage: Stage){
            val infoText = "$material Qty not correct"
            uiStage.addAction(Actions.sequence(
                    Actions.run { for (w in materialAndQtyWindowList){ w.isVisible =false } },

                    Actions.run { ShowInfo.infoLabel(infoText, uiStage) },
                    Actions.delay(1.5f),
                    Actions.run { for (w in materialAndQtyWindowList) {
                        w.isVisible = true
                        w.clearListeners()
                    }
                    }
            ))
        }

        private fun startCreatingSimpleCook(cookMaterialAndQtyWindowList: MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            if (isCookingSuccessful()) {
                creatingSimpleCookSuccess(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
            } else {
                creatingSimpleCookFail(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
            }
        }


        private fun creatingSimpleCookFail(cookMaterialAndQtyWindowList: MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            uiStage.addAction(Actions.sequence(
                    Actions.run {
                        for (w in cookMaterialAndQtyWindowList){
                        w.isVisible = false
                        }
                    },
                    Actions.run {
                        ShowInfo.infoLabel("Fail in create Cooking !!!", uiStage)
                    },
                    Actions.delay(1.5f),
                    Actions.run {
                        addFailedFoodToMap()
                        wokCookingIngredientMap.clear()
                        wokCookingCondimentMap.clear()
                    },
                    Actions.run {
                        startPreparingSimpleCooking(skillUiBtn, uiStage)
                    }
            ))
        }



        fun addFailedFoodToMap(){
            val foodRarity = foodRarity()
            val newFailedFood = generateNewFoodWithBasicInformation("Failed", "Simple", foodRarity)
            generateFailedFoodAttrMap(newFailedFood)
            newFailedFood.description = "Mushy! Mushy!"
            PlayerProfile.playerProfile.failedFoodMap[newFailedFood.UUID] = 1
            NPCookingMaps.updateNPFailedFoodMap(newFailedFood)
        }


        private fun generateNewFoodWithBasicInformation(NoNameOrFail:String, type:String, foodRarity: String): Food {

            val objectUUIDMap = when (NoNameOrFail) {
                "Failed" -> nPFailedObjectUUIDMap
                "NoName" -> nPNoNamObjectUUIDMap
                else -> throw NullPointerException("not correct for NoNameOrFail string")
            }

            val newFood = Food()
            do { newFood.UUID = UUID.randomUUID().toString() } while (objectUUIDMap.contains(newFood.UUID))

            var i = 0
            val charName = PlayerProfile.playerProfile.playerName
            var newFoodName: String

            do {
                i += 1
                newFoodName = """$charName#${i}${NoNameOrFail}Food"""
            } while (objectUUIDMap.containsValue(newFoodName))

            newFood.name = newFoodName
            newFood.inventor = charName
            newFood.category = "Create"
            newFood.rarity = foodRarity
            newFood.type = type

            return newFood
        }


        private fun generateFailedFoodAttrMap(thisFood: Food){

            for (ingredient in wokCookingIngredientMap){
                thisFood.ingredientMap[ingredient.key] = ingredient.value
            }

            for (condiment in wokCookingCondimentMap){
                thisFood.condimentMap[condiment.key] = condiment.value
            }
            for (mainAttr in ppMainAttrList){
                    val mainAttrUUID = FindUUID.findUUID(mainAttr)
                    thisFood.mainEffectMap[mainAttrUUID] = -100
                }

            for (specialAttr in ppSpecialAttrList){
                val specialAttrUUID = FindUUID.findUUID(specialAttr)
                thisFood.specialEffectMap[specialAttrUUID] = 10
            }

        }


        fun creatingSimpleCookSuccess(cookMaterialAndQtyWindowList: MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            val chosenIngredientList = chosenIngredientList()
            val chosenIngredientBaseAttributeMap = chosenIngredientAttributeMap("main", chosenIngredientList)
            val chosenIngredientSpecialAttributeMap = chosenIngredientAttributeMap("special", chosenIngredientList)
            val chosenCondimentList = chosenCondimentList()
            val chosenCondimentGainAttributeMap = chosenCondimentAttributeMap(chosenCondimentList)

            val mainGFactorUUID = FindUUID.findUUID("mainGFactor")
            val specialGFactorUUID = FindUUID.findUUID("specialGFactor")
            val mainGf = chosenCondimentGainAttributeMap[mainGFactorUUID]?: throw NullPointerException(
                    "mainGFactorUUID not found in chosenCondimentGainAttributeMap"
            )
            val specialGf = chosenCondimentGainAttributeMap[specialGFactorUUID]?: throw NullPointerException(
                    "specialGFactorUUID not found in chosenCondimentGainAttributeMap"
            )
            val foodRarity = foodRarity()

            val foodRarityG = when (foodRarity){
                "Unique" -> DimCon.foodUniqueG
                "Special" -> DimCon.foodSpecialG
                "Common" -> DimCon.foodCommonG
                else -> throw NullPointerException("$foodRarity not U,S,or C")
            }

            val newNoNameFood = generateNewFoodWithBasicInformation("NoName", "Simple", foodRarity)
            newNoNameFood.ingredientMap = wokCookingIngredientMap
            newNoNameFood.condimentMap = wokCookingCondimentMap

            for (attribute in chosenIngredientBaseAttributeMap) {
                newNoNameFood.mainEffectMap[attribute.key] = attribute.value * mainGf * foodRarityG
            }

            for (attribute in chosenIngredientSpecialAttributeMap){
                newNoNameFood.specialEffectMap[attribute.key] = attribute.value * specialGf * foodRarityG
            }

            ppNoNameFoodMap[newNoNameFood.UUID] = 1
            NPCookingMaps.updateNPNoNameFoodMap(newNoNameFood)

            displayCookingProgressBar(cookMaterialAndQtyWindowList, newNoNameFood, skillUiBtn, uiStage)

        }



        private fun displayCookingProgressBar(cookMaterialAndQtyWindowList: MutableList<Window>, newNoNameFood: Food, skillUiBtn: TextButton, uiStage: Stage) {

            for (w in cookMaterialAndQtyWindowList){
                w.isVisible = false
            }
            val progressB = ProgressBar(0f, 100f, 10f, false, DimCon.UISkin)

            uiStage.addActor(progressB)
            progressB.setAnimateDuration(2f)
            progressB.setPosition(250f, 400f)

            progressB.addAction(Actions.sequence(
                    Actions.run { progressB.value += 100f },
                    Actions.delay(2f),

                    Actions.run { progressB.isVisible = false },
                    //in 2(animationDuration) seconds Gdx will animate moving progressB to its final
                    // value, which is set for 100f here  .

                    Actions.run { displayNewNoNameFoodInfo(newNoNameFood, skillUiBtn, uiStage) },
                    Actions.removeActor(progressB)
            ))
        }


        private fun displayNewNoNameFoodInfo(newNoNameFood: Food, skillUiBtn: TextButton, uiStage: Stage){

            ShowInfo.infoLabel("You have created ${newNoNameFood.name} food !!", uiStage)

            val displayNewNoNameFoodWindow = TableModel.myItemQtyWindow("Save as new Recipe?")
            val itemTable = TableModel.twoColumnTable(130f, 90f)

            val rarityLabel = TableModel.myTopLeftLabel("Rarity")
            val rarityValue = TableModel.myTopLeftLabel("${newNoNameFood.rarity}")

            TableBtnAndWindowAssembly.addTwoLabelsToTable(rarityLabel,rarityValue,itemTable)

            val mainEffectMap = newNoNameFood.mainEffectMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Main Effect  ", mainEffectMap, itemTable)

            val specialEffectMap = newNoNameFood.specialEffectMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Special Effect  ", specialEffectMap, itemTable)

            val yesBtn = TableModel.myfuncButton("Yes")
            val noBtn = TableModel.myfuncButton("No")

            yesBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    displayNewNoNameFoodWindow.addAction(Actions.removeActor(displayNewNoNameFoodWindow))
                    saveAsNewRecipe(newNoNameFood, skillUiBtn, uiStage)
                }
            })

            noBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiStage.addAction(Actions.sequence(
                            Actions.removeActor(displayNewNoNameFoodWindow),
                            Actions.run {
                                wokCookingIngredientMap.clear()
                                wokCookingCondimentMap.clear()
                            },
                            Actions.run {
                                startPreparingSimpleCooking(skillUiBtn, uiStage)
                            }
                    ))
                }
            })

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(displayNewNoNameFoodWindow, itemTable, yesBtn, noBtn, uiStage)
        }


        private fun saveAsNewRecipe(newNoNameFood: Food, skillUiBtn: TextButton, uiStage: Stage) {

            val saveAsNewRecipeWindow = TableModel.myItemQtyWindow(" Name your new recipe ")
            val nameTFTable = Table()
            val defaultRecipeName = newNoNameFood.name
            var newRecipeName = defaultRecipeName
            val nameRecipeTF = TextField(defaultRecipeName, DimCon.UISkin)

            val okBtn = TableModel.myfuncButton("OK")

            val cancelBtn = TableModel.myfuncButton("Cancel")

            nameTFTable.add(nameRecipeTF)
            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(saveAsNewRecipeWindow, nameTFTable, okBtn, cancelBtn, uiStage)

            nameRecipeTF.setTextFieldFilter { _, c ->

                c.toString().matches(Regex("^[a-zA-Z0-9#]"))
            }

            nameRecipeTF.setTextFieldListener { textField, _ ->
                if (textField == null) {
                    ShowInfo.infoLabel("recipe name can not be null", uiStage)
                    nameRecipeTF.text = defaultRecipeName

                } else if (textField.text == "") {
                    ShowInfo.infoLabel("recipe Name can not be empty", uiStage)
                    nameRecipeTF.text = defaultRecipeName
                }
                newRecipeName = textField.text.toString()
            }

            okBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    if (nPObjectUUIDToNameMap.containsValue(newRecipeName)) {
                        ShowInfo.infoLabel("recipe name exists", uiStage)
                        saveAsNewRecipeWindow.addAction(Actions.removeActor(saveAsNewRecipeWindow))
                        saveAsNewRecipe(newNoNameFood, skillUiBtn, uiStage)
                    } else {
                        saveAsNewRecipeWindow.addAction(Actions.removeActor(saveAsNewRecipeWindow))
                        val newRecipe = CookingRecipe()
                        do {
                            newRecipe.UUID = UUID.randomUUID().toString()
                        } while (nPObjectUUIDToNameMap.contains(newRecipe.UUID))
                        newRecipe.name = newRecipeName
                        newRecipe.type = newNoNameFood.type
                        newRecipe.price = newNoNameFood.price
                        newRecipe.rarity = newNoNameFood.rarity
                        newRecipe.category = "Create"
                        newRecipe.inventor = PlayerProfile.playerProfile.playerName
                        newRecipe.description = newNoNameFood.description

                        for (ingredient in newNoNameFood.ingredientMap) {
                            newRecipe.ingredientMap[ingredient.key] = ingredient.value
                        }
                        for (condiment in newNoNameFood.condimentMap) {
                            newRecipe.condimentMap[condiment.key] = condiment.value
                        }

                        for (mainEffect in newNoNameFood.mainEffectMap) {
                            newRecipe.mainEffectMap[mainEffect.key] = mainEffect.value
                        }

                        for (specialEffect in newNoNameFood.specialEffectMap) {
                            newRecipe.specialEffectMap[specialEffect.key] = specialEffect.value
                        }

                        //*** update CreateCookingRecipeMap and RecipeFoodMap ***
                        NPCookingMaps.updateNPCreatedCookingRecipeMap(newRecipe)

                        val pplearntRecipeMap = PlayerProfile.playerProfile.learntCookingRecipe

                        if (pplearntRecipeMap.contains(newRecipe.UUID)) throw NullPointerException(

                                "pplearntRecipeMap contains newRecipeUUID"

                        ) else {
                            pplearntRecipeMap.add(newRecipe.UUID)
                        }

                        giveRecipeDescription(newRecipe, skillUiBtn, uiStage)
                    }
                }
            })
        }


        fun giveRecipeDescription(newRecipe:CookingRecipe, skillUiBtn: TextButton, uiStage: Stage){

            val descriptionWindow = TableModel.myItemQtyWindow("food description")
            val defaultDescription = "How special the food is!"
            var newDescription = defaultDescription
            val descriptionTF = TextField(defaultDescription, DimCon.UISkin)

            val okBtn = TableModel.myfuncButton("OK")
            val cancelBtn = TableModel.myfuncButton("Cancel")

            val descriptionTable = Table()
            descriptionTable.add(descriptionTF)

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(descriptionWindow,descriptionTable, okBtn, cancelBtn, uiStage)

            descriptionTF.setTextFieldFilter{_,c ->
                c.toString().matches(Regex("^[a-zA-Z0-9#]"))
            }

            descriptionTF.setTextFieldListener { textField, _ ->
                if (textField == null) {
                    ShowInfo.infoLabel("description can not be null", uiStage)
                    descriptionTF.text = defaultDescription
                } else if (textField.text == "") {
                    ShowInfo.infoLabel("description can not be empty", uiStage)
                    descriptionTF.text = defaultDescription
                }
                newDescription = textField.text.toString()
            }

            okBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {

                    newRecipe.description = newDescription

                    descriptionWindow.addAction(Actions.removeActor(descriptionWindow))

                    val label = TableModel.myGLetter("New Cooking Recipe ${newRecipe.name} has been" +
                            "created  !!  ")
                    label.setPosition(DimCon.vpWidth * 0.5f - label.width * 0.5f, DimCon.vpHeight * 0.85f)

                    uiStage.addActor(label)

                    saveNewRecipeLabelAction("Simple", label, wokCookingIngredientMap, wokCookingCondimentMap, skillUiBtn, uiStage)
                }
            })
        }


        private fun saveNewRecipeLabelAction(CookingType: String, label: Label, wokCIMap:MutableMap<String, Int>, wokCDMap:MutableMap<String, Int>, skillUiBtn: TextButton, uiStage: Stage){
            label.addAction(Actions.sequence(
                    Actions.delay(1f),
                    Actions.fadeOut(1f),
                    Actions.run {
                        wokCIMap.clear()
                        wokCDMap.clear()
                        if (CookingType == "Simple") {
                            startPreparingSimpleCooking(skillUiBtn, uiStage)
                        }
                    },
                    Actions.removeActor(label)
            ))
        }


        fun chosenIngredientAttributeMap(attributeType:String, chosenList:MutableList<String>):MutableMap<String,Int>{

            val validAttributeMap = mutableMapOf<String,Int>()
            var cookingIngredientAttributeMap = mutableMapOf<String, Int>()

            for (element in chosenList) {

                if (element != "") {

                val cookingIngredient = nPCookingIngredientMap[element]?: throw NullPointerException ("$element not" +
                        "found in NPCookingIngredientMap")

                when (attributeType) {
                    "main" -> cookingIngredientAttributeMap = cookingIngredient.mainAttributeMap
                    "special" -> cookingIngredientAttributeMap = cookingIngredient.specialAttributeMap
                }

                for (entry in cookingIngredientAttributeMap) {
                    val validIBAN = entry.key
                    val ibav = (validAttributeMap[validIBAN] ?: 0) + entry.value
                    validAttributeMap[validIBAN] = ibav
                }

                }
            }

            return validAttributeMap
        }



        private fun chosenCondimentAttributeMap(chosenCondimentList:MutableList<String>):MutableMap<String,Int>{

            val validGainAttributeMap = mutableMapOf<String,Int>()

            for (element in chosenCondimentList) {

                if (element != ""){
                    val ci = nPCookingCondimentMap[element]?: throw NullPointerException(
                            "$element not found in NPCookingCondimentMap"
                    )
                    val ciAttributeMap = ci.gainAttributeMap

                    for (entry in ciAttributeMap){
                        val validIBAN = entry.key
                        val ibav = (validGainAttributeMap[validIBAN]?:0) + entry.value
                        validGainAttributeMap[validIBAN]= ibav
                    }
                }
            }
            return validGainAttributeMap
        }


        private fun foodRarity(): String {
            // for rarity,assume
            // 1/1000 = unique , 1/100 = special, 1-1/100-1/1000 = 989/1000 common
            //implement in 1-1000 prob range, random n:
            // 500 : unique
            // 1-10 - special
            // else - common

            val probRange = 1000
            val chosenN = (1..probRange).random().toInt()

            return when (chosenN){
                500 -> "Unique"
                in 1..10 -> "Special"
                else -> "Common"
            }

        }

        private fun isCookingSuccessful():Boolean {

            //simple cook
            // 1/100 prob = fail
            // implement
            // chosenN = 50 fail

            val probRange = 100
            val chosenN = (1..probRange).random().toInt()
            return (chosenN != 50)
          //  return (chosenN == 50)
        }


        private fun chosenIngredientList():MutableList<String> {

            //simple cook
            // 1/10 prob = two valid items
            // 1-1/10 = 9/10 one valid item

            //implement, in 1-10000 prob range, random n :
            // 1 - 4500 : itemA valid item
            // 4501 - 5500 : two valid items
            // 5501 - 10000 : itemB valid

            val probRange = 10000
            val chosenN = (1..probRange).random().toInt()

            val wokIngredientPortionList = mutableListOf<String>()

            for ((itemUUID, itemQty) in wokCookingIngredientMap) {
                for (i in 1..itemQty) {
                    wokIngredientPortionList.add(itemUUID)
                }
            }

            return when(chosenN) {
                in 1..4500 -> { mutableListOf(wokIngredientPortionList[0]) }
                in 4501..5500 -> {

                    val twoValidItemList = mutableListOf<String>()
                    for (element in wokIngredientPortionList) {
                        twoValidItemList.add(element)
                    }
                    return twoValidItemList
                }
                in 5501..10000 -> { mutableListOf(wokIngredientPortionList[1]) }
                else -> throw NullPointerException("chosenN: $chosenN out of $probRange range")
            }
        }


        private fun chosenCondimentList():MutableList<String> {

            //simple cook
            // 1/10 prob = two valid items
            // 1-1/10 = 9/10 one valid item

            //implement, in 1-10000 prob range, random n :
            // 1 - 4500 : itemA valid item
            // 4501 - 5500 : two valid items
            // 5501 - 10000 : itemB valid

            val probRange = 10000
            val chosenN = (1..probRange).random().toInt()

            val wokCondimentPortionList = mutableListOf<String>()

            for ((itemUUID, itemQty) in wokCookingCondimentMap) {
                for (i in 1..itemQty) {
                    wokCondimentPortionList.add(itemUUID)
                }
            }

            return when(chosenN) {
                in 1..4500 -> { mutableListOf(wokCondimentPortionList[0]) }
               // in 4901..5000 -> {  mutableListOf("") }
                in 4501..5500 -> {

                    val twoValidItemList = mutableListOf<String>()
                    for (element in wokCondimentPortionList) {
                        twoValidItemList.add(element)
                    }
                    return twoValidItemList
                }
                in 5501..10000 -> { mutableListOf(wokCondimentPortionList[1]) }
                else -> throw NullPointerException("chosenN: $chosenN out of $probRange range")
            }
        }



        private fun checkEnoughCookingComponent(CookingType:String, wokCookingMaterialMap:MutableMap<String, Int>): Boolean {

            var ciQty = 0
            for (entry in wokCookingMaterialMap) { ciQty += entry.value }
                return when (CookingType) {
                    "Simple" -> ciQty == DimCon.simpleCIQty
                    "Advance" -> ciQty == DimCon.advanceCIQty
                    "House" -> ciQty == DimCon.houseCIQty
                    else -> throw NullPointerException("$CookingType not found")
                }
        }



        private fun ClosedRange<Int>.random() = Math.random() * ((endInclusive + 1) - start) + start







    }
}


package com.inos.mudgdx


import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener


class Bag {

    companion object {

        val moneyTable = TableModel.moneyTable()
//if moneyTable is created inside moneyUi(), actors will increase with each call on moneyUi
        fun bagUi(uiStage: Stage) {

            val bagUiBtn = TableModel.myfuncButton("Bag")

            bagUiBtn.setPosition(DimCon.vpWidth * 0.85f - bagUiBtn.width * 0.5f, 0f)
            bagUiBtn.setSize(32f, 32f)

            bagUiBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    BagItem.listBagItem(bagUiBtn,uiStage)
                    bagUiBtn.touchable = Touchable.disabled
                }
            })

            bagUiBtn.name = "BagUiBtn"
            uiStage.addActor(bagUiBtn)

        }



        fun moneyUi(uiStage: Stage) {
            moneyTable.clear()

            uiStage.addActor(moneyTable)

            val pprofile = PlayerProfile.playerProfile
            val money = pprofile.money

            val aGSC = MoneyConversion.toGSC(money)

            val golds = TableModel.myGLabel("${aGSC[0]}")
            val silvers = TableModel.myGLabel("${aGSC[1]}")
            val coppers = TableModel.myGLabel("${aGSC[2]}")

            val gL = TableModel.myGLetter("G")
            val sL = TableModel.mySLetter("S")
            val cL = TableModel.myCLetter("C")

            moneyTable.add(golds)
            moneyTable.add(gL)
            moneyTable.add(silvers)
            moneyTable.add(sL)
            moneyTable.add(coppers)
            moneyTable.add(cL)

            moneyTable.x = DimCon.vpWidth * 0.8f
            moneyTable.y = DimCon.vpHeight - DimCon.tileWidthInPx * 0.4f

        }




    }


}
package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.InputListener
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Image

class PlayerMove {

    val moveStep = 0.11f * DimCon.tileWidthInPx

    fun move(playerMoveListener: PlayerMoveListener, player: Image, mapXmax: Float, mapYmax: Float){

        val pml = playerMoveListener
        when  {
            pml.moveL -> moveLR(player,"L", mapXmax)
            pml.moveR -> moveLR(player, "R", mapXmax)
            pml.moveU -> moveUD(player, "U", mapYmax)
            pml.moveD -> moveUD(player, "D", mapYmax)
        }
    }


    fun moveLR(player: Image, dir:String, mapXmax: Float){

        var newMoveStep = moveStep
        val oldposX = player.x

        if (dir == "L") { newMoveStep = -moveStep }

        player.x += newMoveStep

        if (player.x in 1..(mapXmax.toInt()-1) ){
            Collision.collisionDetection(dir, player)

        } else {
            player.x = oldposX
        }
    }


    fun moveUD(player: Image, dir:String, mapYmax: Float){

        var newMoveStep = moveStep
        val oldposY = player.y

        if (dir == "D") { newMoveStep = -moveStep }

        player.y += newMoveStep

        if (player.y in 1..(mapYmax.toInt()-1) ){
            Collision.collisionDetection(dir, player)

        } else {
            player.y = oldposY
        }
    }


}
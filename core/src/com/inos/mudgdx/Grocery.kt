package com.inos.mudgdx


import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage


class Grocery {

    companion object {

        fun grocer( grocerObj: ObjImage, uiStage: Stage) {

            if (grocerObj.available) {

                if (grocerObj.clickFlag) {

                    if (grocerObj.collideWithPlayerFlag) {

                        TradeAct.startTradeAct( grocerObj,uiStage)
                        grocerObj.collideWithPlayerFlag = false

                    } else {

                        ShowInfo.farAwayInfo( grocerObj, uiStage)

                    }
                }
            }
        }



    }
}
package com.inos.mudgdx

import mudCookingSetting.recipe.CreatedCookingRecipe
import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonWriter

class SaveMapFiles {

    companion object {

        fun prepareJson(): Json {
            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)
            return json
        }

        fun saveMapFiles(){
            saveCreatedCookingRecipeMap()
            saveObjectUUIDToNameMap()
            saveNoNameObjectUUIDMap()
            saveRecipeFoodMap()
            saveNoNameFoodMap()
            saveFailedFoodMap()
            saveFailedObjectUUIDMap()
        }


        fun saveCreatedCookingRecipeMap(){
            val json = prepareJson()
            val fileHandle = NPFileHandle.createdCookingRecipeMapFH()
            fileHandle.writeString(json.toJson(CreatedCookingRecipe.createdCookingRecipeMap), false)

        }

        fun saveObjectUUIDToNameMap(){
            val json = prepareJson()
            val uuidFileHandle = NPFileHandle.objectUUIDToNameMapFH()
            uuidFileHandle.writeString(json.toJson(mudObjectUUIDSetting.ObjectUUIDToName.objectUUIDToNameMap), false)
        }

        fun saveRecipeFoodMap(){
            val json = prepareJson()
            val fileHandle = NPFileHandle.recipeFoodMapFH()
            fileHandle.writeString(json.toJson(mudCookingSetting.food.RecipeFood.recipeFoodMap), false)
        }

        fun saveNoNameFoodMap() {
            val json = prepareJson()
            val fileHandle = NPFileHandle.noNameFoodMapFH()
            fileHandle.writeString(json.toJson(mudCookingSetting.food.NoNameFood.noNameFoodMap), false)
        }

        fun saveFailedFoodMap() {
            val json = prepareJson()
            val fileHandle = NPFileHandle.failedFoodMapFH()
            fileHandle.writeString(json.toJson(mudCookingSetting.food.FailedFood.failedFoodMap), false)
        }

        fun saveNoNameObjectUUIDMap(){
            val json = prepareJson()
            val uuidFileHandle = NPFileHandle.noNameObjectUUIDMapFH()
            uuidFileHandle.writeString(json.toJson(mudObjectUUIDSetting.NoNameObjectUUID.noNameObjectUUIDMap), false)
        }

        fun saveFailedObjectUUIDMap(){
            val json = prepareJson()
            val uuidFileHandle = NPFileHandle.failedObjectUUIDMapFH()
            uuidFileHandle.writeString(json.toJson(mudObjectUUIDSetting.FailedObjectUUID.failedObjectUUIDMap), false)
        }


    }
}
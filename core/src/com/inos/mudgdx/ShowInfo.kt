package com.inos.mudgdx

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Label

class ShowInfo {

    companion object {


        fun closeContactInfo(objImage: ObjImage, uiStage: Stage) {
            val mText = "${objImage.actionType} ${objImage.name}"
            infoLabel(mText, uiStage)

        }


        fun farAwayInfo(objImage: ObjImage, uiStage: Stage) {
            val mText = "too far ${objImage.name}"
            infoLabel(mText, uiStage)

        }


        fun infoLabel(mText: String, uiStage: Stage): Label {
            val label = TableModel.infoLabel(mText)
            uiStage.addActor(label)
            label.addAction(Actions.sequence(
                    Actions.delay(1f),
                    Actions.removeActor(label)
            ))
            return label
        }

    }
}
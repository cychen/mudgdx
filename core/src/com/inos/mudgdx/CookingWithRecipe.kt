package com.inos.mudgdx

import mudCookingSetting.recipe.CookingRecipe
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener


class CookingWithRecipe {

    companion object {

        private val nPCookingRecipeMap = NPCookingMaps.NPCookingRecipeMap()
        private val ppCookingIngredientMap = PlayerProfile.playerProfile.cookingIngredientMap
        private val ppCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap
        private val objectUUIDToNameMap = NPUUIDMaps.NPObjectUUIDToNameMap()

        fun displayLearntRecipe(skillUiBtn: TextButton, uiStage: Stage) {

            val learntRecipeListWindow = TableModel.myItemQtyWindow("Learnt Recipes")

            val closeBtn = TableModel.myCloseTextButton("X")

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    learntRecipeListWindow.addAction(Actions.removeActor(learntRecipeListWindow))
                    skillUiBtn.touchable = Touchable.enabled
                }
            })

            val recipeActorList = mutableListOf<Actor>()

            val learntRecipeList = PlayerProfile.playerProfile.learntCookingRecipe

            for (recipeUUID in learntRecipeList) {

                val thisRecipe = nPCookingRecipeMap[recipeUUID] ?: throw
                NullPointerException("$recipeUUID not found in NPCookingMaps")

                val recipeName = thisRecipe.name
                val recipeBtn = TableModel.myfuncButton(recipeName)

                recipeBtn.addListener(cookingWithRecipe(thisRecipe, uiStage))
                recipeActorList.add(recipeBtn)
            }

            val itemTable = TableBtnAndWindowAssembly.sectionListTable(recipeActorList, 100f)
            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(learntRecipeListWindow, itemTable, closeBtn, uiStage)
        }


        private fun cookingWithRecipe(thisRecipe: CookingRecipe, uiStage: Stage): ClickListener {

            return object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {

                    val cookRecipeFoodWindow = TableModel.myItemQtyWindow(" Cook ${thisRecipe.name} ")
                    val closeBtn = TableModel.myCloseTextButton("X")
                    val itemTable = displayMaterialAndFoodEffectContent(thisRecipe)

                    val yesCookingBtn = TableModel.myfuncButton("Cook")
                    val noCookingBtn = TableModel.myfuncButton("No")

                    TableBtnAndWindowAssembly.addTableYesNoAndCloseBtnToWindow(
                            cookRecipeFoodWindow, itemTable, yesCookingBtn, noCookingBtn, closeBtn, uiStage)

                    closeBtn.addListener(object : ClickListener() {
                        override fun clicked(event: InputEvent?, x: Float, y: Float) {
                            cookRecipeFoodWindow.addAction(Actions.removeActor(cookRecipeFoodWindow))
                        }
                    })

                    yesCookingBtn.addListener(startCookingAct(thisRecipe, uiStage))
                    noCookingBtn.addListener(object : ClickListener() {
                        override fun clicked(event: InputEvent?, x: Float, y: Float) {
                            cookRecipeFoodWindow.addAction(Actions.removeActor(cookRecipeFoodWindow))
                        }
                    })
                }
            }
        }


        private fun startCookingAct(thisRecipe: CookingRecipe, uiStage: Stage): ClickListener {

            return object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {

                    val recipeIngredientMap = thisRecipe.ingredientMap
                    val recipeCondimentMap = thisRecipe.condimentMap

                    val yesIngredient = TableBtnAndWindowAssembly.hasEnoughMaterial(recipeIngredientMap, ppCookingIngredientMap)
                    val yesCondiment = TableBtnAndWindowAssembly.hasEnoughMaterial(recipeCondimentMap, ppCookingCondimentMap)

                    if (yesIngredient && yesCondiment) {
                        cookFood(thisRecipe, uiStage)
                    } else {
                        ShowInfo.infoLabel("Not enough Cooking material", uiStage)
                    }
                }
            }
        }


        private fun cookFood(thisRecipe: CookingRecipe, uiStage: Stage) {
            val progressB = ProgressBar(0f, 100f, 10f, false, DimCon.UISkin)
            // progressB.style.knobBefore = progressB.style.knob

            uiStage.addActor(progressB)
            progressB.setAnimateDuration(2f)
            progressB.setPosition(250f, 400f)

            progressB.addAction(Actions.sequence(
                    Actions.run { progressB.value += 100f },
                    Actions.delay(2f),

                    Actions.run { progressB.isVisible = false },
                    //in 2(animationDuration) seconds Gdx will animate moving progressB to its final
                    // value, which is set for 100f here  .

                    Actions.run { ShowInfo.infoLabel(" You get one fresh cooked Food${thisRecipe.name} !", uiStage) },
                    Actions.run {
                        val cookedFoodName = "Food${thisRecipe.name}"
                        val cookedFoodUUID = FindUUID.findUUID(cookedFoodName)

                        val oldqty = PlayerProfile.playerProfile.recipeFoodMap[cookedFoodUUID]?:0

                        PlayerProfile.playerProfile.recipeFoodMap[cookedFoodUUID] = oldqty + 1

                        updateBagItem(thisRecipe)
                    },
                    Actions.removeActor(progressB)
            ))
        }


        private fun updateBagItem(thisRecipe: CookingRecipe) {
            val recipeIngredientMap = thisRecipe.ingredientMap
            val recipeCondimentMap = thisRecipe.condimentMap

            for ((recipeItemUUID, recipeItemQty) in recipeIngredientMap) {

                val oldQty = ppCookingIngredientMap[recipeItemUUID] ?: throw NullPointerException(
                        "$recipeItemUUID not found in ppCookingIngredientMap")
                ppCookingIngredientMap[recipeItemUUID] = oldQty - recipeItemQty
            }

            for ((recipeItemUUID, recipeItemQty) in recipeCondimentMap) {

                val oldQty = ppCookingCondimentMap[recipeItemUUID] ?: throw NullPointerException(
                        "$recipeItemUUID not found in ppCookingCondimentMap")
                ppCookingCondimentMap[recipeItemUUID] = oldQty - recipeItemQty
            }
        }


        private fun displayMaterialAndFoodEffectContent (thisRecipe: CookingRecipe): Table {

            val itemTable = TableModel.twoColumnTable(130f, 90f)

            val rarityLabel = TableModel.myTopLeftLabel("Rarity")
            val rarityValue = TableModel.myTopLeftLabel("${thisRecipe.rarity}")

            val typeLabel = TableModel.myTopLeftLabel("Type")
            val typeValue = TableModel.myTopLeftLabel("${thisRecipe.type}")

            val inventorLabel = TableModel.myTopLeftLabel("Inventor")
            val inventorValue = TableModel.myTopLeftLabel("${thisRecipe.inventor}")

            TableBtnAndWindowAssembly.addTwoLabelsToTable(rarityLabel,rarityValue,itemTable)
            TableBtnAndWindowAssembly.addTwoLabelsToTable(typeLabel,typeValue,itemTable)
            TableBtnAndWindowAssembly.addTwoLabelsToTable(inventorLabel,inventorValue,itemTable)

            val ingredientMap = thisRecipe.ingredientMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Ingredient  ", ingredientMap, itemTable)

            val condimentMap = thisRecipe.condimentMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Condiment  ", condimentMap, itemTable)

            val mainEffectMap = thisRecipe.mainEffectMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Main Effect  ", mainEffectMap, itemTable)

            val specialEffectMap = thisRecipe.specialEffectMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Special Effect  ", specialEffectMap, itemTable)

            return itemTable

        }

    }
}


package com.inos.mudgdx


import com.badlogic.gdx.Gdx
import mudCookingSetting.food.Food
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.utils.TimeUtils


class DisplayFoodInfo {

    companion object {

        val ppFoodMainAttrMap = PlayerProfile.playerProfile.foodMainAttributeMap
        val ppFoodSpecialAttrMap = PlayerProfile.playerProfile.foodSpecialAttributeMap
        val ppRecipeFoodMap = PlayerProfile.playerProfile.recipeFoodMap
        val ppNoNameFoodMap = PlayerProfile.playerProfile.noNameFoodMap
        val ppFailedFoodMap = PlayerProfile.playerProfile.failedFoodMap

        fun displayRecipeFoodInfo(thisFood:Food, itemButton:TextButton, bagItemWindow:Window, bagUiBtn: TextButton, uiStage: Stage) {

            bagUiBtn.touchable = Touchable.disabled

            val foodWindow = TableModel.myItemQtyWindow(" ${thisFood.name} Food ")

            val closeBtn = TableModel.myCloseTextButton("X")
            val useBtn = TableModel.myfuncButton("Use")
            val disposeBtn = TableModel.myfuncButton("Dispose")
            val itemTable = TableModel.twoColumnTable(150f, 50f)

            val mainEffectMap = thisFood.mainEffectMap
            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Main Effect  ", mainEffectMap, itemTable)

            val specialEffectMap = thisFood.specialEffectMap
            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Special Effect  ", specialEffectMap, itemTable)

            TableBtnAndWindowAssembly.addTableYesNoAndCloseBtnToWindow(foodWindow, itemTable, useBtn, disposeBtn, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    itemButton.touchable = Touchable.enabled
                    foodWindow.addAction(Actions.removeActor(foodWindow))
                }
            })

            useBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    itemButton.touchable = Touchable.disabled
                    disableUseAndDisposeBtn(useBtn, disposeBtn)
                    checkToReplaceExistingFoodEffect(thisFood, itemButton, ppRecipeFoodMap, foodWindow, bagItemWindow, bagUiBtn, uiStage)
                }
            })

            disposeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    disableUseAndDisposeBtn(useBtn, disposeBtn)
                    updatePPFoodMap(thisFood, ppRecipeFoodMap)
                    actionsToRemoveFoodWindow(foodWindow, bagItemWindow, bagUiBtn, uiStage)
                }
            })
        }


        private fun disableUseAndDisposeBtn(useBtn: TextButton, disposeBtn: TextButton){
            useBtn.touchable = Touchable.disabled
            disposeBtn.touchable = Touchable.disabled
        }


        private fun checkToReplaceExistingFoodEffect(thisFood: Food, itemButton: TextButton, ppFoodMap: MutableMap<String, Int>, foodWindow: Window, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage){

            val now = TimeUtils.millis()
            val ppFoodEffectStopTime = PlayerProfile.playerProfile.foodEffectStopTime

            if (now < ppFoodEffectStopTime){

                confirmToReplaceExistingEffect(thisFood, itemButton, ppFoodMap, foodWindow, bagItemWindow, bagUiBtn, uiStage)

            } else {

                useFoodProgressBar(thisFood, ppRecipeFoodMap, foodWindow, bagItemWindow, bagUiBtn, uiStage)
            }
        }



        private fun confirmToReplaceExistingEffect(thisFood: Food, itemButton: TextButton, ppFoodMap: MutableMap<String, Int>, foodWindow: Window, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage){

            val warningWindow = TableModel.myItemQtyWindow(" Warning !! ")
            val noBtn = TableModel.myfuncButton("No")
            val yesBtn = TableModel.myfuncButton("Yes")

            val itemTable = TableModel.oneColumnTable(200f)
            val message = TableModel.myLabel("Old FoodEffect will be replaced !!")
            message.setWrap(true)
            itemTable.add(message)
            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(warningWindow, itemTable, yesBtn, noBtn, uiStage)

            noBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    disableUseAndDisposeBtn(yesBtn, noBtn)
                    itemButton.touchable = Touchable.enabled
                    warningWindow.addAction(Actions.removeActor(warningWindow))
                }
            })

            yesBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    disableUseAndDisposeBtn(yesBtn, noBtn)

                    for ( attr in ppFoodMainAttrMap ){
                        ppFoodMainAttrMap[attr.key] = 0
                    }

                    for (attr in ppFoodSpecialAttrMap) {
                        ppFoodSpecialAttrMap[attr.key] = 0
                    }
                    warningWindow.addAction(Actions.removeActor(warningWindow))
                    useFoodProgressBar(thisFood, ppFoodMap,foodWindow, bagItemWindow, bagUiBtn, uiStage )
                }
            })

        }



        fun useFoodProgressBar(thisFood: Food, ppFoodMap: MutableMap<String, Int>, foodWindow:Window, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage) {

            val progressB = ProgressBar(0f, 100f, 10f, false, DimCon.UISkin)

            uiStage.addActor(progressB)
            progressB.setAnimateDuration(2f)
            progressB.setPosition(250f, 400f)

            progressB.addAction(Actions.sequence(
                    Actions.run { progressB.value += 100f },

                    Actions.delay(2f),
                    Actions.run { progressB.isVisible = false },
                    Actions.run { ShowInfo.infoLabel("Eating ${thisFood.name}", uiStage) },

                    Actions.delay(0.25f),
                    Actions.run {
                        updatePPFoodMap(thisFood, ppFoodMap)
                        addFoodAttrAndEffectLasting(thisFood)
                    },
                    Actions.run { actionsToRemoveFoodWindow(foodWindow, bagItemWindow, bagUiBtn, uiStage) },

                    Actions.removeActor(progressB)
            ))
        }


        fun actionsToRemoveFoodWindow(foodWindow: Window, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage){
            uiStage.addAction(Actions.sequence(
                    Actions.delay(0.5f),
            Actions.removeActor(foodWindow),

            Actions.delay(0.5f),
            Actions.removeActor(bagItemWindow),

            Actions.delay(0.25f),
            Actions.run{ BagItem.listBagItem(bagUiBtn, uiStage)}
            ))
        }


        fun updatePPFoodMap(thisFood: Food, ppFoodMap: MutableMap<String, Int>){

            val oldFoodQty = ppFoodMap[thisFood.UUID]?: throw NullPointerException(
                    "${thisFood.name} not found in ppFoodMap")

            ppFoodMap[thisFood.UUID] = oldFoodQty - 1

        }


        fun addFoodAttrAndEffectLasting(thisFood: Food){

            for ((foodMainAttrUUID, foodMainAttrValue) in thisFood.mainEffectMap){
                val ppFoodMainAttrValue = ppFoodMainAttrMap[foodMainAttrUUID]?: throw NullPointerException(
                        "$foodMainAttrUUID not found in ppFoodMainAttrMap")
                ppFoodMainAttrMap[foodMainAttrUUID] = ppFoodMainAttrValue + foodMainAttrValue
            }

            val effectLastUUID = FindUUID.findUUID("effectLasting")
            val foodEffectLastTime = thisFood.specialEffectMap[effectLastUUID]?: throw NullPointerException(
                    "$effectLastUUID not found in thisFood.SpecialEffectMap")

            ppFoodSpecialAttrMap[effectLastUUID] = foodEffectLastTime
            val lifeRecoveryUUID = FindUUID.findUUID("lifeRecovery")
            val foodLifeRecoveryValue = thisFood.specialEffectMap[lifeRecoveryUUID]?: throw NullPointerException(
                    "$lifeRecoveryUUID not found in thisFood.SpecialEffectMap")
            ppFoodSpecialAttrMap[lifeRecoveryUUID] = foodLifeRecoveryValue

            val now = TimeUtils.millis()

            PlayerProfile.playerProfile.foodEffectStopTime = now + foodEffectLastTime * 60 * 1000L

        }



        fun displayNoNameFoodInfo(thisFood: Food, itemButton:TextButton, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage) {

            bagUiBtn.touchable = Touchable.disabled

            val itemTable = TableModel.twoColumnTable(150f, 50f)

            val mainEffectMap = thisFood.mainEffectMap
            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Main Effect  ", mainEffectMap, itemTable)

            val specialEffectMap = thisFood.specialEffectMap
            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Special Effect  ", specialEffectMap, itemTable)

            val foodWindow = TableModel.myItemQtyWindow(" ${thisFood.name} Food ")

            val closeBtn = TableModel.myCloseTextButton("X")
            val useBtn = TableModel.myfuncButton("Use")
            val disposeBtn = TableModel.myfuncButton("Dispose")

            TableBtnAndWindowAssembly.addTableYesNoAndCloseBtnToWindow(foodWindow, itemTable, useBtn, disposeBtn, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    itemButton.touchable = Touchable.enabled
                    foodWindow.addAction(Actions.removeActor(foodWindow))
                }
            })

            useBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    disableUseAndDisposeBtn(useBtn, disposeBtn)
                    checkToReplaceExistingFoodEffect(thisFood, itemButton, ppNoNameFoodMap, foodWindow, bagItemWindow, bagUiBtn, uiStage)
                }
            })

            disposeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    disableUseAndDisposeBtn(useBtn, disposeBtn)
                    updatePPFoodMap(thisFood, ppNoNameFoodMap)
                    actionsToRemoveFoodWindow(foodWindow, bagItemWindow, bagUiBtn, uiStage)
                }
            })

        }


        fun displayFailedFoodInfo(thisFood: Food, itemButton:TextButton, bagItemWindow: Window, bagUiBtn: TextButton, uiStage: Stage) {

            bagUiBtn.touchable = Touchable.disabled

            val foodWindow = TableModel.myItemQtyWindow("${thisFood.name}")

            val itemTable = TableModel.twoColumnTable(130f, 90f)

            val nameLabel = TableModel.myTopLeftLabel(" FoodName")
            val nameValue = TableModel.myTopLeftLabel("        ${thisFood.name}")

            val categoryLabel = TableModel.myTopLeftLabel(" Category")
            val categoryValue = TableModel.myTopLeftLabel("        ${thisFood.category}")

            val rarityLabel = TableModel.myTopLeftLabel(" Rarity")
            val rarityValue = TableModel.myTopLeftLabel("        ${thisFood.rarity}")

            val typeLabel = TableModel.myTopLeftLabel(" Type")
            val typeValue = TableModel.myTopLeftLabel("        ${thisFood.type}")

            val inventorLabel = TableModel.myTopLeftLabel(" Inventor")
            val inventorValue = TableModel.myTopLeftLabel("        ${thisFood.inventor}")

            val descriptionLabel = TableModel.myTopLeftLabel(" Description")
            val descriptionValue1 = TableModel.myTopLeftLabel("")
            val descriptionValue2 = TableModel.myTopLeftLabel("${thisFood.description}")

            descriptionValue2.setWrap(true)
            descriptionValue2.width = 200f
          //  descriptionValue2.pack()

            TableBtnAndWindowAssembly.addTwoLabelsToTable(nameLabel, nameValue,itemTable)
            TableBtnAndWindowAssembly.addTwoLabelsToTable(categoryLabel, categoryValue,itemTable)
            TableBtnAndWindowAssembly.addTwoLabelsToTable(rarityLabel,rarityValue,itemTable)
            TableBtnAndWindowAssembly.addTwoLabelsToTable(typeLabel,typeValue,itemTable)
            TableBtnAndWindowAssembly.addTwoLabelsToTable(inventorLabel,inventorValue,itemTable)
            TableBtnAndWindowAssembly.addTwoLabelsToTable(descriptionLabel,descriptionValue1,itemTable)
            itemTable.add(descriptionValue2).colspan(2)
            itemTable.row()
            val ingredientMap = thisFood.ingredientMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Ingredient  ", ingredientMap, itemTable)

            val condimentMap = thisFood.condimentMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Condiment  ", condimentMap, itemTable)

            val mainEffectMap = thisFood.mainEffectMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Main Effect  ", mainEffectMap, itemTable)

            val specialEffectMap = thisFood.specialEffectMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Special Effect  ", specialEffectMap, itemTable)


           // val actorsList = mutableListOf<Actor>(nameLabel, nameValue, categoryLabel, categoryValue,
           //         rarityLabel, rarityValue, typeLabel, typeValue, inventorLabel, inventorValue, descriptionLabel, descriptionValue)

          //  val itemTable = TableBtnAndWindowAssembly.sectionListTable(actorsList, 230f)


            val useBtn = TableModel.myfuncButton("Use")
            val disposeBtn = TableModel.myfuncButton("Dispose")

            val closeBtn = TableModel.myCloseTextButton("X")

            TableBtnAndWindowAssembly.addTableYesNoAndCloseBtnToWindow(foodWindow, itemTable, useBtn, disposeBtn, closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    itemButton.touchable = Touchable.enabled
                    foodWindow.addAction(Actions.removeActor(foodWindow))
                }
            })

            useBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    disableUseAndDisposeBtn(useBtn, disposeBtn)
                    checkToReplaceExistingFoodEffect(thisFood, itemButton, ppFailedFoodMap, foodWindow, bagItemWindow, bagUiBtn, uiStage)
                }
            })

            disposeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    disableUseAndDisposeBtn(useBtn, disposeBtn)
                    updatePPFoodMap(thisFood, ppFailedFoodMap)
                    actionsToRemoveFoodWindow(foodWindow, bagItemWindow, bagUiBtn, uiStage)
                }
            })
        }


        private fun displayFailedFoodMessage(thisFood: Food, uiStage: Stage){
            val messageWindow = TableModel.myConversationWindow(" ${thisFood.name}")
            val messageTable = TableModel.oneColumnTable(200f)
            val message = TableModel.myLabel("The tasting of ${thisFood.name} is awful !!")
            messageTable.add(message)
            message.setWrap(true)

            val closeBtn = TableModel.myfuncButton("X")
            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(messageWindow,messageTable,closeBtn, uiStage)

            closeBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    messageWindow.addAction(Actions.removeActor(messageWindow))

                }
            })
        }

    }
}


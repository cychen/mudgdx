package mudMedicineSetting


import com.badlogic.gdx.utils.Json
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.JsonWriter
import com.inos.mudgdx.DimCon
import com.inos.mudgdx.FindUUID
import com.inos.mudgdx.NPFileHandle


class Medicinal {

    var UUID: String = "UUID"
    var name: String = "Flower"
    var rarity: String = "Common"
    var price: Int = 1
    var category: String = "medicinal"
    var type: String = "Primary"
    var mainAttributeMap: MutableMap<String, Int> = mutableMapOf()
    var specialAttributeMap: MutableMap<String, Int> = mutableMapOf()


    companion object {

        val medicinalMap = mutableMapOf<String, Medicinal>()

        private val mainAttributeList = MedicinalCategory.mainAttribute()
        private val specialAttributeList = MedicinalCategory.specialAttribute()

        private val primaryList = MedicinalCategory.defaultPrimaryMedicinal()
        private val primaryMainMax = DimCon.primaryMainMax
        private val primarySpecialMax = DimCon.primarySpecialMax


        fun medicinalMap() {

            if (!NPFileHandle.medicinalMapFH().exists()) {
                generateMedicinalMap()
            }
            extractMedicinalMap()
        }


        private fun generateMedicinalMap() {

            for (element in primaryList) {
                val thisMedicinal = Medicinal()
                thisMedicinal.name = element
                thisMedicinal.UUID = FindUUID.findUUID(element)
                thisMedicinal.category = "medicinal"
                thisMedicinal.rarity = "Common"
                thisMedicinal.type = "Primary"
                thisMedicinal.price = 1

                for ( mainAttributeName in mainAttributeList) {
                    val mainAttributeUUID = FindUUID.findUUID(mainAttributeName)
                    thisMedicinal.mainAttributeMap[mainAttributeUUID] = ((0..primaryMainMax).random()).toInt()
                }

                for (specialAttributeName in specialAttributeList){
                    val specialAttributeUUID = FindUUID.findUUID(specialAttributeName)
                    thisMedicinal.specialAttributeMap[specialAttributeUUID] = ((0..primarySpecialMax).random()).toInt()
                }
                medicinalMap[thisMedicinal.UUID] = thisMedicinal
            }

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.medicinalMapFH()
            fileHandle.writeString(json.toJson(medicinalMap), false)

        }



        private fun extractMedicinalMap() {

            val json = Json()
            json.setOutputType(JsonWriter.OutputType.json)
            json.setUsePrototypes(false)

            val fileHandle = NPFileHandle.medicinalMapFH()

            val jsonReader = JsonReader()
            val jsonValueMap = jsonReader.parse(fileHandle)

            for ( element in jsonValueMap) {

                val thisMedicinal = Medicinal()
                thisMedicinal.UUID = element.getString("UUID")
                thisMedicinal.category = element.getString("category")
                thisMedicinal.name = element.getString("name")
                thisMedicinal.rarity = element.getString("rarity")
                thisMedicinal.type = element.getString("type")
                thisMedicinal.price = element.getInt("price")
                val mainAttributeMapJV = element.get("mainAttributeMap")
                val specialAttributeMapJV = element.get("specialAttributeMap")

                for (attribute in mainAttributeMapJV) {

                    if (attribute.name != "class") {
                        val attributeUUID = attribute.name
                        val attributeValue = mainAttributeMapJV.getInt(attribute.name)
                        thisMedicinal.mainAttributeMap[attributeUUID] = attributeValue
                    }
                }

                for (attribute in specialAttributeMapJV) {

                    if (attribute.name != "class") {

                        val attributeUUID = attribute.name
                        val attributeValue = specialAttributeMapJV.getInt(attribute.name)
                        thisMedicinal.specialAttributeMap[attributeUUID] = attributeValue
                    }
                }
                medicinalMap[thisMedicinal.UUID] = thisMedicinal
            }
        }


        private fun ClosedRange<Int>.random() = Math.random() * ((endInclusive + 1) - start) + start


    }

}

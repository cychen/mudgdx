package mudCookingSkill.createCooking


import com.inos.mudgdx.PlayerProfile
import com.inos.mudgdx.NPCookingMaps
import com.inos.mudgdx.NPUUIDMaps
import com.inos.mudgdx.DNDMapCondimentItem
import com.inos.mudgdx.DNDMapItem

import com.inos.mudgdx.FindUUID
import com.inos.mudgdx.TableModel
import com.inos.mudgdx.TableBtnAndWindowAssembly

import com.inos.mudgdx.ShowInfo
import com.inos.mudgdx.DimCon

import mudCookingSetting.food.Food
import mudCookingSetting.recipe.CookingRecipe
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.InputEvent
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.Touchable
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop
import java.util.*


class CreateCooking {

    companion object {

        private val ppCookingIngredientMap = PlayerProfile.playerProfile.cookingIngredientMap
        private val ppCookingCondimentMap = PlayerProfile.playerProfile.cookingCondimentMap
        private val ppCookingTypeList = NPCookingMaps.NPCookingTypeList()
        private val ppCookingRecipeMap = PlayerProfile.playerProfile.cookingRecipeMap
        private val ppNoNameFoodMap = PlayerProfile.playerProfile.noNameFoodMap
        private val ppRecipeFoodMap = PlayerProfile.playerProfile.recipeFoodMap

        private val nPCookingIngredientMap = NPCookingMaps.NPCookingIngredientMap()
        private val nPCookingCondimentMap = NPCookingMaps.NPCookingCondimentMap()
        private val wokCookingIngredientMap = DNDMapItem.wokCookingIngredientMap()
        private val wokCookingCondimentMap = DNDMapCondimentItem.wokCookingCondimentMap()
        private val nPCookingRecipeMap = NPCookingMaps.NPCookingRecipeMap()
        private val nPObjectUUIDToNameMap = NPUUIDMaps.NPObjectUUIDToNameMap()
        private val nPNoNamObjectUUIDMap = NPUUIDMaps.NPNoNameObjectUUIDMap()
        private val nPFailedFoodMap = NPCookingMaps.NPFailedFoodMap()
        private val nPFailedObjectUUIDMap = NPUUIDMaps.NPFailedObjectUUIDMap()


        private val bagItemMapList = mutableListOf(
                ppCookingCondimentMap,
                ppCookingIngredientMap)

        fun displayCookingType(skillUiBtn:TextButton, uiStage: Stage) {

           // skillBtn.touchable = Touchable.disabled

            val cookingTypeListWindow = TableModel.myItemQtyWindow("Cooking Types")

            val closeBtn = TableModel.myCloseTextButton("X")

            closeBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    cookingTypeListWindow.addAction(Actions.removeActor(cookingTypeListWindow))
                    skillUiBtn.touchable = Touchable.enabled
                }
            })

            val cookingTypeActorList = mutableListOf<Actor>()

            for (cookingType in ppCookingTypeList) {

                val cookingTypeBtn = TableModel.myfuncButton(cookingType)

                cookingTypeBtn.addListener(object : ClickListener() {
                    override fun clicked(event: InputEvent?, x: Float, y: Float) {
                        cookingTypeListWindow.addAction(Actions.removeActor(cookingTypeListWindow))
                        selectCookingType(cookingType, skillUiBtn, uiStage)
                    }
                })
                cookingTypeActorList.add(cookingTypeBtn)
            }

            val itemTable = TableBtnAndWindowAssembly.sectionListTable(cookingTypeActorList, 100f)
            TableBtnAndWindowAssembly.addTableAndCloseBtnToWindow(cookingTypeListWindow, itemTable, closeBtn, uiStage)

        }


        private fun selectCookingType(cookingType:String, skillUiBtn: TextButton, uiStage: Stage){

            when (cookingType){
                "Simple" -> startPreparingSimpleCooking(skillUiBtn, uiStage)
                "Advance" -> return
                "House" -> return
                else -> return
            }
        }


        private fun startPreparingSimpleCooking(skillUiBtn: TextButton, uiStage: Stage){

           val bagAndWokIngredientWindowList = addCookingIngredientToWok(uiStage)
           val bagAndWokCondimentWindowList = addCookingCondimentToWok(uiStage)
            requireQtyInfoWindow(bagAndWokIngredientWindowList, bagAndWokCondimentWindowList, skillUiBtn, uiStage)
        }


        private fun addCookingIngredientToWok(uiStage: Stage): MutableList<Window> {
           return AddBagItemToWok.addCookingIngredientToWok(uiStage)
           /*
            val dnd = DragAndDrop()

            val bagTable = Table()
            bagTable.columnDefaults(0).width(130f).height(30f)
            bagTable.debug()

            val wokTable = Table()
            wokTable.columnDefaults(0).width(130f).height(30f)
            wokTable.debug()

            val bagAndWokIngredientWindowList = mutableListOf<Window>()
            val bagWindow = TableModel.myItemQtyWindow("Bag Ingredients")
            val wokWindow = TableModel.myItemQtyWindow("Wok Ingredients")
            bagAndWokIngredientWindowList.add(bagWindow)
            bagAndWokIngredientWindowList.add(wokWindow)

            bagWindow.x = bagWindow.x * 0.5f
            bagWindow.y = bagWindow.y * 0.75f

            TableBtnAndWindowAssembly.addTableToWindow(bagWindow, bagTable, uiStage)

            val x = bagWindow.x + bagWindow.width * 1.02f
            val y = bagWindow.y
            wokWindow.setPosition(x,y)

            TableBtnAndWindowAssembly.addTableToWindow(wokWindow, wokTable, uiStage)
            DNDMapItem.dndBag(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)

            return bagAndWokIngredientWindowList
          */
        }


        private fun requireQtyInfoWindow(bagAndWokIngredientWindowList:MutableList<Window>, bagAndWokCondimentWindowList:MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage):Window{
            val requireQtyInfoWindow = TableModel.myCreateCookingWindow("Create Simple Cooking")
            val requireIngredientQtyLabel = TableModel.myLabel("Require Qty: ")
            val requireCondimentQtyLabel = TableModel.myLabel("2 Ingredients, 2 Condiments")
            requireIngredientQtyLabel.setWrap(true)
            requireCondimentQtyLabel.setWrap(true)
            val requireQtyInfoTable = TableModel.oneColumnTable(250f)
            requireQtyInfoTable.add(requireIngredientQtyLabel)
            requireQtyInfoTable.row()
            requireQtyInfoTable.add(requireCondimentQtyLabel)

            val cookBtn = TableModel.myfuncButton("Cook")
            val cancelBtn = TableModel.myfuncButton("Cancel")

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(requireQtyInfoWindow, requireQtyInfoTable, cookBtn, cancelBtn, uiStage)
            requireQtyInfoWindow.x = requireQtyInfoWindow.x * 1.2f
            requireQtyInfoWindow.y = requireQtyInfoWindow.y * 1.3f

            val cookMaterialAndQtyWindowList = mutableListOf<Window>()
            for (w in bagAndWokIngredientWindowList){
                cookMaterialAndQtyWindowList.add(w)
            }
            for (w in bagAndWokCondimentWindowList){
                cookMaterialAndQtyWindowList.add(w)
            }
            cookMaterialAndQtyWindowList.add(requireQtyInfoWindow)

            cookBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    simpleCook(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
                }
            })

            cancelBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    for (w in cookMaterialAndQtyWindowList){
                        w.addAction(Actions.removeActor(w))
                    }

                    for (ingredient in wokCookingIngredientMap){
                        ppCookingIngredientMap[ingredient.key] =
                                (ppCookingIngredientMap[ingredient.key]?:0) + ingredient.value
                    }

                    for (condiment in wokCookingCondimentMap){
                        ppCookingCondimentMap[condiment.key] =
                                (ppCookingCondimentMap[condiment.key]?:0) + condiment.value
                    }

                    wokCookingCondimentMap.clear()
                    wokCookingIngredientMap.clear()

                    skillUiBtn.touchable = Touchable.enabled
                    //put wok qty back to bag item qty
                }
            })
            return requireQtyInfoWindow
        }


        private fun simpleCook(cookMaterialAndQtyWindowList:MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            if (!checkEnoughCI()){

                uiStage.addAction(Actions.sequence(
                        Actions.run {
                            for (w in cookMaterialAndQtyWindowList) {
                                w.isVisible = false
                            }
                        },

                    Actions.run {
                        ShowInfo.infoLabel("Cooking Ingredient Qty not correct", uiStage)
                    },
                    Actions.delay(1.5f),

                    Actions.run {
                        for (w in cookMaterialAndQtyWindowList) {
                            w.isVisible = true
                            w.clearListeners()
                        }
                    }
                ))

            } else if (!checkEnoughCD()){

                uiStage.addAction(Actions.sequence(
                        Actions.run {
                            for (w in cookMaterialAndQtyWindowList){
                                w.isVisible =false
                            }
                        },
                        Actions.run {

                            ShowInfo.infoLabel("Cooking Condiment Qty not correct", uiStage)
                        },
                        Actions.delay(1.5f),
                        Actions.run {
                            for (w in cookMaterialAndQtyWindowList) {
                                w.isVisible = true
                                w.clearListeners()
                            }
                        }
                ))

            } else {
                startCreateSimpleCook(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
            }
        }


        private fun startCreateSimpleCook(cookMaterialAndQtyWindowList: MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            if (isCookingSuccessful()) {
                simpleCookSuccess(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
            } else {
                simpleCookFail(cookMaterialAndQtyWindowList, skillUiBtn, uiStage)
            }
        }


        private fun simpleCookFail(cookMaterialAndQtyWindowList: MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            uiStage.addAction(Actions.sequence(
                    Actions.run {
                        for (w in cookMaterialAndQtyWindowList){
                        w.isVisible = false
                        }
                    },
                    Actions.run {
                        ShowInfo.infoLabel("Fail in create Cooking !!!", uiStage)
                    },
                    Actions.delay(1.5f),
                    Actions.run {
                        wokCookingIngredientMap.clear()
                        wokCookingCondimentMap.clear()
                        addFailedFoodToMap()
                    },
                    Actions.run {
                        startPreparingSimpleCooking(skillUiBtn, uiStage)
                    }
            ))
        }


        fun addFailedFoodToMap(){
            val newFailedFood = Food()

            do {
                newFailedFood.UUID = UUID.randomUUID().toString()
            } while (nPFailedObjectUUIDMap.contains(newFailedFood.UUID))

            var i = 0
            val charName = PlayerProfile.playerProfile.playerName
            var foodName: String

            do {
                i += 1
                foodName = """$charName#${i}FailedFood"""
            } while (nPFailedObjectUUIDMap.containsValue(foodName))

            newFailedFood.name = foodName
            newFailedFood.inventor = charName
            newFailedFood.category = "Create"
            newFailedFood.rarity = foodRarity()
            newFailedFood.type = "Simple"
            newFailedFood.description = "Mushy! Mushy!"
            PlayerProfile.playerProfile.failedFoodMap[newFailedFood.UUID] = 1
            NPCookingMaps.updateNPFailedFoodMap(newFailedFood)
        }

        fun simpleCookSuccess(cookMaterialAndQtyWindowList: MutableList<Window>, skillUiBtn: TextButton, uiStage: Stage) {

            val chosenIngredientList = chosenIngredientList()
            val chosenIngredientBaseAttributeMap = chosenIngredientAttributeMap("main", chosenIngredientList)
            val chosenIngredientSpecialAttributeMap = chosenIngredientAttributeMap("special", chosenIngredientList)
            val chosenCondimentList = chosenCondimentList()
            val chosenCondimentGainAttributeMap = chosenCondimentAttributeMap(chosenCondimentList)

            val mainGFactorUUID = FindUUID.findUUID("mainGFactor")
            val specialGFactorUUID = FindUUID.findUUID("specialGFactor")
            val mainGf = chosenCondimentGainAttributeMap[mainGFactorUUID]?: throw NullPointerException(
                    "mainGFactorUUID not found in chosenCondimentGainAttributeMap"
            )
            val specialGf = chosenCondimentGainAttributeMap[specialGFactorUUID]?: throw NullPointerException(
                    "specialGFactorUUID not found in chosenCondimentGainAttributeMap"
            )
            val foodRarity = foodRarity()

            val foodRarityG = when (foodRarity){
                "Unique" -> DimCon.foodUniqueG
                "Special" -> DimCon.foodSpecialG
                "Common" -> DimCon.foodCommonG
                else -> throw NullPointerException("$foodRarity not U,S,or C")
            }

            val newNoNameFood = Food()

            do {
                newNoNameFood.UUID = UUID.randomUUID().toString()
            } while (nPNoNamObjectUUIDMap.contains(newNoNameFood.UUID))

            var i = 0
            val charName = PlayerProfile.playerProfile.playerName
            var foodName: String

            do {
                i += 1
                foodName = """$charName#${i}NoNameFood"""
            } while (nPNoNamObjectUUIDMap.containsValue(foodName))

            newNoNameFood.name = foodName
            newNoNameFood.inventor = charName
            newNoNameFood.category = "Create"
            newNoNameFood.rarity = foodRarity
            newNoNameFood.type = "Simple"

            newNoNameFood.ingredientMap = wokCookingIngredientMap
            newNoNameFood.condimentMap = wokCookingCondimentMap

            for (attribute in chosenIngredientBaseAttributeMap) {
                newNoNameFood.mainEffectMap[attribute.key] = attribute.value * mainGf * foodRarityG
            }

            for (attribute in chosenIngredientSpecialAttributeMap){
                newNoNameFood.specialEffectMap[attribute.key] = attribute.value * specialGf * foodRarityG
            }

            ppNoNameFoodMap[newNoNameFood.UUID] = 1
            NPCookingMaps.updateNPNoNameFoodMap(newNoNameFood)

            cookFood(cookMaterialAndQtyWindowList, newNoNameFood, skillUiBtn, uiStage)

        }



        private fun cookFood(cookMaterialAndQtyWindowList: MutableList<Window>, newNoNameFood: Food, skillUiBtn: TextButton, uiStage: Stage) {

            for (w in cookMaterialAndQtyWindowList){
                w.isVisible = false
            }
            val progressB = ProgressBar(0f, 100f, 10f, false, DimCon.UISkin)

            uiStage.addActor(progressB)
            progressB.setAnimateDuration(2f)
            progressB.setPosition(250f, 400f)

            progressB.addAction(Actions.sequence(
                    Actions.run { progressB.value += 100f },
                    Actions.delay(2f),

                    Actions.run { progressB.isVisible = false },
                    //in 2(animationDuration) seconds Gdx will animate moving progressB to its final
                    // value, which is set for 100f here  .

                    Actions.run {
                       displayNewNoNameFoodInfo(newNoNameFood, skillUiBtn, uiStage)
                    },
                    Actions.removeActor(progressB)
            ))

        }


        private fun displayNewNoNameFoodInfo(newNoNameFood: Food, skillUiBtn: TextButton, uiStage: Stage){

            ShowInfo.infoLabel("You have created ${newNoNameFood.name} food !!", uiStage)

            val displayNewNoNameFoodWindow = TableModel.myItemQtyWindow("Save as new Recipe?")
            val itemTable = TableModel.twoColumnTable(130f, 90f)

            val rarityLabel = TableModel.myTopLeftLabel("Rarity")
            val rarityValue = TableModel.myTopLeftLabel("${newNoNameFood.rarity}")

            TableBtnAndWindowAssembly.addTwoLabelsToTable(rarityLabel,rarityValue,itemTable)

            val mainEffectMap = newNoNameFood.mainEffectMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Main Effect  ", mainEffectMap, itemTable)

            val specialEffectMap = newNoNameFood.specialEffectMap

            TableBtnAndWindowAssembly.addUUIDMapItemToTable("Special Effect  ", specialEffectMap, itemTable)

            val yesBtn = TableModel.myfuncButton("Yes")
            val noBtn = TableModel.myfuncButton("No")

            yesBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    displayNewNoNameFoodWindow.addAction(Actions.removeActor(displayNewNoNameFoodWindow))
                    saveAsNewRecipe(newNoNameFood, skillUiBtn, uiStage)
                }
            })

            noBtn.addListener(object : ClickListener(){
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    uiStage.addAction(Actions.sequence(
                            Actions.removeActor(displayNewNoNameFoodWindow),
                            Actions.run {
                                wokCookingIngredientMap.clear()
                                wokCookingCondimentMap.clear()
                            },
                            Actions.run {
                                startPreparingSimpleCooking(skillUiBtn, uiStage)
                            }
                    ))
                }
            })

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(displayNewNoNameFoodWindow, itemTable, yesBtn, noBtn, uiStage)
        }


        private fun saveAsNewRecipe(newNoNameFood: Food, skillUiBtn: TextButton, uiStage: Stage) {

            val saveAsNewRecipeWindow = TableModel.myItemQtyWindow(" Name your new recipe ")
            val nameTFTable = Table()
            val defaultRecipeName = newNoNameFood.name
            var newRecipeName = defaultRecipeName
            val nameRecipeTF = TextField(defaultRecipeName, DimCon.UISkin)

            val okBtn = TableModel.myfuncButton("OK")

            val cancelBtn = TableModel.myfuncButton("Cancel")

            nameTFTable.add(nameRecipeTF)
            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(saveAsNewRecipeWindow, nameTFTable, okBtn, cancelBtn, uiStage)

            nameRecipeTF.setTextFieldFilter { _, c ->

                c.toString().matches(Regex("^[a-zA-Z0-9#]"))
            }

            nameRecipeTF.setTextFieldListener { textField, _ ->
                if (textField == null) {
                    ShowInfo.infoLabel("recipe name can not be null", uiStage)
                    nameRecipeTF.text = defaultRecipeName

                } else if (textField.text == "") {
                    ShowInfo.infoLabel("recipe Name can not be empty", uiStage)
                    nameRecipeTF.text = defaultRecipeName
                }
                newRecipeName = textField.text.toString()
            }

            okBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {
                    if (NPUUIDMaps.NPObjectUUIDToNameMap().containsValue(newRecipeName)) {
                        ShowInfo.infoLabel("recipe name exists", uiStage)
                        saveAsNewRecipeWindow.addAction(Actions.removeActor(saveAsNewRecipeWindow))
                        saveAsNewRecipe(newNoNameFood, skillUiBtn, uiStage)
                    } else {
                        saveAsNewRecipeWindow.addAction(Actions.removeActor(saveAsNewRecipeWindow))
                        val newRecipe = CookingRecipe()
                        do {
                            newRecipe.UUID = UUID.randomUUID().toString()
                        } while (nPObjectUUIDToNameMap.contains(newRecipe.UUID))
                        newRecipe.name = newRecipeName
                        newRecipe.type = newNoNameFood.type
                        newRecipe.price = newNoNameFood.price
                        newRecipe.rarity = newNoNameFood.rarity
                        newRecipe.category = "Create"
                        newRecipe.inventor = PlayerProfile.playerProfile.playerName
                        newRecipe.description = newNoNameFood.description

                        for (ingredient in newNoNameFood.ingredientMap) {
                            newRecipe.ingredientMap[ingredient.key] = ingredient.value
                        }
                        for (condiment in newNoNameFood.condimentMap) {
                            newRecipe.condimentMap[condiment.key] = condiment.value
                        }

                        for (mainEffect in newNoNameFood.mainEffectMap) {
                            newRecipe.mainEffectMap[mainEffect.key] = mainEffect.value
                        }

                        for (specialEffect in newNoNameFood.specialEffectMap) {
                            newRecipe.specialEffectMap[specialEffect.key] = specialEffect.value
                        }

                        //*** update CreateCookingRecipeMap and RecipeFoodMap ***
                        NPCookingMaps.updateNPCreatedCookingRecipeMap(newRecipe)

                        val pplearntRecipeMap = PlayerProfile.playerProfile.learntCookingRecipe

                        if (pplearntRecipeMap.contains(newRecipe.UUID)) throw NullPointerException(

                                "pplearntRecipeMap contains newRecipeUUID"

                        ) else {
                            pplearntRecipeMap.add(newRecipe.UUID)
                        }

                        giveRecipeDescription(newRecipe, skillUiBtn, uiStage)
                    }
                }
            })
        }



        fun giveRecipeDescription(newRecipe:CookingRecipe, skillUiBtn: TextButton, uiStage: Stage){

            val descriptionWindow = TableModel.myItemQtyWindow("food description")
            val defaultDescription = "How special the food is!"
            var newDescription = defaultDescription
            val descriptionTF = TextField(defaultDescription, DimCon.UISkin)

            val okBtn = TableModel.myfuncButton("OK")
            val cancelBtn = TableModel.myfuncButton("Cancel")

            val descriptionTable = Table()
            descriptionTable.add(descriptionTF)

            TableBtnAndWindowAssembly.addTableYesNoBtnToWindow(descriptionWindow,descriptionTable, okBtn, cancelBtn, uiStage)

            descriptionTF.setTextFieldFilter{_,c ->
                c.toString().matches(Regex("^[a-zA-Z0-9#]"))
            }

            descriptionTF.setTextFieldListener { textField, _ ->
                if (textField == null) {
                    ShowInfo.infoLabel("description can not be null", uiStage)
                    descriptionTF.text = defaultDescription
                } else if (textField.text == "") {
                    ShowInfo.infoLabel("description can not be empty", uiStage)
                    descriptionTF.text = defaultDescription
                }
                newDescription = textField.text.toString()
            }

            okBtn.addListener(object : ClickListener() {
                override fun clicked(event: InputEvent?, x: Float, y: Float) {

                    newRecipe.description = newDescription

                    descriptionWindow.addAction(Actions.removeActor(descriptionWindow))

                    val label = TableModel.myGLetter("New Cooking Recipe ${newRecipe.name} has been" +
                            "created  !!  ")
                    label.setPosition(DimCon.vpWidth * 0.5f - label.width * 0.5f, DimCon.vpHeight * 0.85f)

                    uiStage.addActor(label)

                    label.addAction(Actions.sequence(
                            Actions.delay(1f),
                            Actions.fadeOut(1f),
                            Actions.run {
                                wokCookingIngredientMap.clear()
                                wokCookingCondimentMap.clear()
                                startPreparingSimpleCooking(skillUiBtn, uiStage)
                            },
                            Actions.removeActor(label)
                    ))


                }
            })
        }



        fun chosenIngredientAttributeMap(attributeType:String, chosenList:MutableList<String>):MutableMap<String,Int>{

            val validAttributeMap = mutableMapOf<String,Int>()
            var ciAttributeMap = mutableMapOf<String, Int>()

            for (element in chosenList) {

                if (element != "") {

                val ci = nPCookingIngredientMap[element]?: throw NullPointerException ("$element not" +
                        "found in NPCookingIngredientMap")

                when (attributeType) {
                    "main" -> ciAttributeMap = ci.mainAttributeMap
                    "special" -> ciAttributeMap = ci.specialAttributeMap
                }

                for (entry in ciAttributeMap) {
                    val validIBAN = entry.key
                    val ibav = (validAttributeMap[validIBAN] ?: 0) + entry.value
                    validAttributeMap[validIBAN] = ibav
                }

                }
            }

            return validAttributeMap
        }



        private fun chosenCondimentAttributeMap(chosenCondimentList:MutableList<String>):MutableMap<String,Int>{

            val validGainAttributeMap = mutableMapOf<String,Int>()

            for (element in chosenCondimentList) {

                if (element != ""){
                    val ci = nPCookingCondimentMap[element]?: throw NullPointerException(
                            "$element not found in NPCookingCondimentMap"
                    )
                    val ciAttributeMap = ci.gainAttributeMap

                    for (entry in ciAttributeMap){
                        val validIBAN = entry.key
                        val ibav = (validGainAttributeMap[validIBAN]?:0) + entry.value
                        validGainAttributeMap[validIBAN]= ibav
                    }
                }
            }
            return validGainAttributeMap
        }


        private fun foodRarity(): String {
            // for rarity,assume
            // 1/1000 = unique , 1/100 = special, 1-1/100-1/1000 = 989/1000 common
            //implement in 1-1000 prob range, random n:
            // 500 : unique
            // 1-10 - special
            // else - common

            val probRange = 1000
            val chosenN = (1..probRange).random().toInt()

            return when (chosenN){
                500 -> "Unique"
                in 1..10 -> "Special"
                else -> "Common"
            }

        }

        private fun isCookingSuccessful():Boolean {

            //simple cook
            // 1/100 prob = fail
            // implement
            // chosenN = 50 fail

            val probRange = 100
            val chosenN = (1..probRange).random().toInt()
            return (chosenN != 50)
          //  return (chosenN == 50)
        }


        private fun chosenIngredientList():MutableList<String> {

            //simple cook
            // 1/10 prob = two valid items
            // 1-1/10 = 9/10 one valid item

            //implement, in 1-10000 prob range, random n :
            // 1 - 4500 : itemA valid item
            // 4501 - 5500 : two valid items
            // 5501 - 10000 : itemB valid

            val probRange = 10000
            val chosenN = (1..probRange).random().toInt()

            val wokIngredientPortionList = mutableListOf<String>()

            for ((itemUUID, itemQty) in wokCookingIngredientMap) {
                for (i in 1..itemQty) {
                    wokIngredientPortionList.add(itemUUID)
                }
            }

            return when(chosenN) {
                in 1..4500 -> { mutableListOf(wokIngredientPortionList[0]) }
                in 4501..5500 -> {

                    val twoValidItemList = mutableListOf<String>()
                    for (element in wokIngredientPortionList) {
                        twoValidItemList.add(element)
                    }
                    return twoValidItemList
                }
                in 5501..10000 -> { mutableListOf(wokIngredientPortionList[1]) }
                else -> throw NullPointerException("chosenN: $chosenN out of $probRange range")
            }
        }


        private fun chosenCondimentList():MutableList<String> {

            //simple cook
            // 1/10 prob = two valid items
            // 1-1/10 = 9/10 one valid item

            //implement, in 1-10000 prob range, random n :
            // 1 - 4500 : itemA valid item
            // 4501 - 5500 : two valid items
            // 5501 - 10000 : itemB valid

            val probRange = 10000
            val chosenN = (1..probRange).random().toInt()

            val wokCondimentPortionList = mutableListOf<String>()

            for ((itemUUID, itemQty) in wokCookingCondimentMap) {
                for (i in 1..itemQty) {
                    wokCondimentPortionList.add(itemUUID)
                }
            }

            return when(chosenN) {
                in 1..4500 -> { mutableListOf(wokCondimentPortionList[0]) }
               // in 4901..5000 -> {  mutableListOf("") }
                in 4501..5500 -> {

                    val twoValidItemList = mutableListOf<String>()
                    for (element in wokCondimentPortionList) {
                        twoValidItemList.add(element)
                    }
                    return twoValidItemList
                }
                in 5501..10000 -> { mutableListOf(wokCondimentPortionList[1]) }
                else -> throw NullPointerException("chosenN: $chosenN out of $probRange range")
            }
        }





        private fun addCookingCondimentToWok(uiStage: Stage): MutableList<Window> {
          return AddBagItemToWok.addCookingCondimentToWok(uiStage)
           /*
            val dnd = DragAndDrop()

            val bagTable = Table()
            bagTable.columnDefaults(0).width(130f).height(30f)
            bagTable.debug()

            val wokTable = Table()
            wokTable.columnDefaults(0).width(130f).height(30f)
            wokTable.debug()

            val bagWindow = TableModel.myItemQtyWindow("Bag Condiments")
            val wokWindow = TableModel.myItemQtyWindow("Wok Condiments")

            val bagAndWokCondimentWindowList = mutableListOf<Window>()
            bagAndWokCondimentWindowList.add(bagWindow)
            bagAndWokCondimentWindowList.add(wokWindow)

            bagWindow.x = bagWindow.x * 0.5f
            bagWindow.y = bagWindow.y - bagWindow.height * 1.5f
            TableBtnAndWindowAssembly.addTableToWindow(bagWindow, bagTable, uiStage)

            wokWindow.x = bagWindow.x + bagWindow.width * 1.02f
            wokWindow.y = bagWindow.y
            TableBtnAndWindowAssembly.addTableToWindow(wokWindow, wokTable, uiStage)

            DNDMapCondimentItem.dndBag(dnd, bagWindow, wokWindow, bagTable, wokTable, uiStage)
            return bagAndWokCondimentWindowList
            */
        }


        fun checkEnoughCI(): Boolean {
            var ciQty = 0
            for (entry in wokCookingIngredientMap){ ciQty += entry.value }
            return (ciQty == 2)
        }


        fun checkEnoughCD(): Boolean {
            var cdQty = 0
            for (entry in wokCookingCondimentMap){ cdQty += entry.value }
            return (cdQty == 2)
        }


        private fun ClosedRange<Int>.random() = Math.random() * ((endInclusive + 1) - start) + start







    }
}





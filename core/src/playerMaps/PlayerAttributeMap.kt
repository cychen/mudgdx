package playerMaps

import com.inos.mudgdx.DimCon

class PlayerAttributeMap() {


    companion object {


        fun baseMainAttributeNameList(): MutableList<String> {
            return mutableListOf("Str", "Agi", "Foc")
        }

        fun foodSpecialAttributeNameList(): MutableList<String>{
            return mutableListOf("effectLasting", "lifeRecovery")
        }

        fun foodMainAttributeNameList(): MutableList<String>{
            return mutableListOf("Str", "Agi", "Foc")
        }

        fun baseSpecialAttributeNameList():MutableList<String>{
            return mutableListOf("effectLasting", "lifeRecovery")
        }

        fun baseLifeAttributeNameList(): MutableList<String>{
            return mutableListOf("totalLife", "remainLife")
        }

        fun baseLifeAttributeNameToValueMap(): MutableMap<String, Int>{
            val baseLifeAttrNameToValueMap = mutableMapOf<String, Int>()
            baseLifeAttrNameToValueMap["totalLife"] = DimCon.baseLife
            baseLifeAttrNameToValueMap["remainLife"] = DimCon.baseLife
            return baseLifeAttrNameToValueMap
        }

        fun baseMainAttributeNameToValueMap(): MutableMap<String, Int> {
            val baseMainAttrNameToValueMap = mutableMapOf<String, Int>()
            baseMainAttrNameToValueMap["Str"] = DimCon.baseStrength
            baseMainAttrNameToValueMap["Agi"] = DimCon.baseAgility
            baseMainAttrNameToValueMap["Foc"] = DimCon.baseFocus
            return baseMainAttrNameToValueMap
        }


        fun baseSpecialAttributeNameToValueMap(): MutableMap<String, Int> {
            val baseSpecialAttributeNameToValueMap = mutableMapOf<String, Int>()
            baseSpecialAttributeNameToValueMap["effectLasting"] = DimCon.baseEffectLasting
            baseSpecialAttributeNameToValueMap["lifeRecovery"] = DimCon.baseLifeRecovery
            return baseSpecialAttributeNameToValueMap
        }




    }
}